package com.liststing.android.models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatMessage {
    private boolean isIncoming;
    private String message;
    private String author;
    private String timestamp;


    public ChatMessage(String message, String author) {
        this(true, message, author, generateTimeStamp());
    }

    public ChatMessage(boolean isIncoming, String message, String author) {
        this(isIncoming, message, author, generateTimeStamp());
    }

    public ChatMessage(boolean isIncoming, String message, String author, String timestamp) {
        this.isIncoming = isIncoming;
        this.message = message;
        this.author = author;
        this.timestamp = timestamp;
    }

    private static String generateTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm a");
        return sdf.format(new Date());
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isIncoming() {
        return isIncoming;
    }

    public void setIsIncoming(boolean isIncoming) {
        this.isIncoming = isIncoming;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
