package com.liststing.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.liststing.android.util.RecordedLog;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Element;

public class Service implements Parcelable {
    private long id;
    private String name;
    private Category category;

    public Service(Long id, String name, Category category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public Service(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.category = in.readParcelable(Category.class.getClassLoader());
    }

    public static Service fromJson(JSONObject json) {
        long id = json.optLong("id");
        String name = json.optString("name");
        Category category = null;
        try {
            category = Category.fromJson(json.getJSONObject("category"));
        } catch (JSONException e) {
            RecordedLog.e(RecordedLog.Tag.API, "Service > failed to get category jsonObject \n" + e);
        }

        return new Service(id, name, category);
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString()
    {
        return getName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(this.id);
        parcel.writeString(this.name);
        parcel.writeParcelable(this.category, flags);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    public String toXml() {
        StringBuilder builder = new StringBuilder();
        builder.append("<service>");
        builder.append("<id>" + getId() + "</id>");
        builder.append("<name>" + getName() + "</name>");
        builder.append(getCategory().toXml());
        builder.append("</service>");
        return builder.toString();
    }

    public static Service fromXml(Element serviceNode) {
        if(serviceNode.hasChildNodes()) {
            long id = Long.parseLong(serviceNode.getElementsByTagName("id").item(0).getFirstChild().getTextContent());
            String name = serviceNode.getElementsByTagName("name").item(0).getFirstChild().getTextContent();
            Category category = Category.fromXml((Element) serviceNode.getElementsByTagName("category").item(0));

            return new Service(id, name, category);
        }
        return null;
    }
}
