package com.liststing.android.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Patterns;

import com.liststing.android.datasets.BusinessMediaCollection;
import com.liststing.android.datasets.BusinessTable;
import com.liststing.android.datasets.ServiceCollection;
import com.liststing.android.util.NetworkUtils;

import org.json.JSONObject;

public class Business implements Parcelable {

    private long id;
    private long industryId;
    private String businessName;
    private String description;
    private String phoneNumber;
    private String email;
    private String website;
    private String avatarUrl;
    private boolean showLocation;
    private String addressData;
    private Account admin;

    private BusinessMediaCollection photos;
    private ServiceCollection services;

    public Business() {
        reset();
    }

    public Business(long id, Account admin, long industryId, String businessName, String description, String phoneNumber,
                    String email, String website, ServiceCollection services, boolean showLocation, String addressData, String avatarUrl,
                    BusinessMediaCollection photos) {
        this.id = id;
        this.industryId = industryId;
        this.businessName = businessName;
        this.description = description;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.website = website;
        this.avatarUrl = avatarUrl;
        this.services = services;
        this.showLocation = showLocation;
        this.addressData = addressData;
        this.admin = admin;
        this.photos = photos;
    }

    public Business(Parcel in) {
        this.id = in.readLong();
        this.industryId = in.readLong();
        this.admin = in.readParcelable(Business.class.getClassLoader());
        this.businessName = in.readString();
        this.description = in.readString();
        this.phoneNumber = in.readString();
        this.email =  in.readString();
        this.website = in.readString();
        this.avatarUrl = in.readString();
        this.services = new ServiceCollection();
        in.readList(this.services, getClass().getClassLoader());
        this.showLocation = in.readInt() > 0;
        this.addressData = in.readString();
        this.photos = new BusinessMediaCollection();
        in.readList(this.photos, getClass().getClassLoader());
    }

    public static Business fromJson(JSONObject json) {
        if(json == null) {
            return null;
        }
        long id = json.optLong("id");
        long industryId = json.optLong("industry");
        Account admin = new Account();
        admin.updateFromJsonObject(json.optJSONObject("admin"));
        String displayName = NetworkUtils.optJsonString(json, "business_name");
        String description = NetworkUtils.optJsonString(json, "description");
        String phoneNumber = NetworkUtils.optJsonString(json, "phone_number");
        String email = NetworkUtils.optJsonString(json, "email");
        String website = NetworkUtils.optJsonString(json, "website");
        String avatarUrl = NetworkUtils.optJsonString(json, "avatar");
        String addressData = NetworkUtils.optJsonString(json, "address_data");
        ServiceCollection services = ServiceCollection.fromJson(json);
        BusinessMediaCollection photos = BusinessMediaCollection.fromJson(json);
        boolean showLocation = json.optBoolean("show_location");

        return new Business(id, admin, industryId, displayName, description, phoneNumber, email, website, services, showLocation, addressData,
                avatarUrl, photos);
    }

    public void reset() {
        this.id = 0;
        this.industryId = 0;
        this.admin = null;
        this.businessName = "";
        this.description = "";
        this.phoneNumber = "";
        this.email = "";
        this.website = "";
        this.addressData = "";
        this.avatarUrl = "";
        this.services = new ServiceCollection();
        this.showLocation = false;
        this.photos = new BusinessMediaCollection();
    }

    public void signout() {
        reset();
        save();
    }

    public void save() {
        BusinessTable.saveAdmin(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIndustryId() {
        return industryId;
    }

    public void setIndustryId(long industryId) {
        this.industryId = industryId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return convertWebsite(website);
    }

    public void setWebsite(String website) {
        this.website =  convertWebsite(website);
    }

    private String convertWebsite(String website) {
        if(website == null || website.isEmpty() || !Patterns.WEB_URL.matcher(website).matches()) {
            return "";
        }
        if(!website.startsWith("www.") && !website.startsWith("http://")){
            website = "www."+website;
        }
        if (!website.startsWith("https://") && !website.startsWith("http://")){
            website = "http://"+website;
        }
        return website;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }


    public ServiceCollection getServices() {
        return services;
    }

    public void setServices(ServiceCollection services) {
        this.services = services;
    }

    public boolean canShowLocation() {
        return showLocation;
    }

    public void setShowLocation(boolean showLocation) {
        this.showLocation = showLocation;
    }

    public String getAddressData() {
        return addressData;
    }

    public void setAddressData(String addressData) {
        this.addressData = addressData;
    }

    public Account getAdmin() {
        return admin;
    }

    public void setAdmin(Account admin) {
        this.admin = admin;
    }

    public BusinessMediaCollection getPhotos() {
        return photos;
    }

    public void setPhotos(BusinessMediaCollection photos) {
        this.photos = photos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeLong(this.industryId);
        parcel.writeParcelable(this.admin, i);
        parcel.writeString(this.businessName);
        parcel.writeString(this.description);
        parcel.writeString(this.phoneNumber);
        parcel.writeString(this.email);
        parcel.writeString(this.website);
        parcel.writeString(this.avatarUrl);
        parcel.writeList(this.services);
        parcel.writeInt(showLocation ? 1 : 0);
        parcel.writeString(this.addressData);
        parcel.writeList(this.photos);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Business createFromParcel(Parcel in) {
            return new Business(in);
        }

        public Business[] newArray(int size) {
            return new Business[size];
        }
    };

}
