package com.liststing.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class Industry implements Parcelable {
    private long id;
    private String name;
    private String image;

    public Industry(Long id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public Industry(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.image = in.readString();
    }

    public static Industry fromJson(JSONObject json) {
        long id = json.optLong("id");
        String name = json.optString("name");
        String image = json.optString("image");

        return new Industry(id, name, image);
    }

    public boolean isSameIndustry(Industry industry) {
        return industry != null
                && industry.id == this.id
                && industry.name.equals(this.name)
                && industry.image.equals(this.image);
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return getName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.name);
        parcel.writeString(this.image);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Industry createFromParcel(Parcel in) {
            return new Industry(in);
        }

        public Industry[] newArray(int size) {
            return new Industry[size];
        }
    };
}
