package com.liststing.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.i18n.addressinput.common.AddressData;

/**
 * This class mirrors AddressData instance variables in order to ensure state is saved
 */
public class QCAddress implements Parcelable {
    private final String postalCountry;
    private final String addressLine1;
    private final String addressLine2;
    private final String administrativeArea;
    private final String locality;
    private final String dependentLocality;
    private final String postalCode;
    private final String sortingCode;
    private final String organization;
    private final String recipient;
    private final String languageCode;

    public QCAddress(AddressData data) {
        this.postalCountry = data.getPostalCountry();
        this.addressLine1 = data.getAddressLine1();
        this.addressLine2 = data.getAddressLine2();
        this.administrativeArea = data.getAdministrativeArea();
        this.locality = data.getLocality();
        this.dependentLocality = data.getDependentLocality();
        this.postalCode = data.getPostalCode();
        this.sortingCode = data.getSortingCode();
        this.organization = data.getOrganization();
        this.recipient = data.getRecipient();
        this.languageCode = data.getLanguageCode();
    }

    public QCAddress(Parcel in) {
        this.postalCountry = in.readString();
        this.addressLine1 = in.readString();
        this.addressLine2 = in.readString();
        this.administrativeArea = in.readString();
        this.locality = in.readString();
        this.dependentLocality = in.readString();
        this.postalCode = in.readString();
        this.sortingCode = in.readString();
        this.organization = in.readString();
        this.recipient = in.readString();
        this.languageCode = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.postalCountry);
        parcel.writeString(this.addressLine1);
        parcel.writeString(this.addressLine2);
        parcel.writeString(this.administrativeArea);
        parcel.writeString(this.locality);
        parcel.writeString(this.dependentLocality);
        parcel.writeString(this.postalCode);
        parcel.writeString(this.sortingCode);
        parcel.writeString(this.organization);
        parcel.writeString(this.recipient);
        parcel.writeString(this.languageCode);
    }

    public String getPostalCountry() {
        return postalCountry;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getAdministrativeArea() {
        return administrativeArea;
    }

    public String getLocality() {
        return locality;
    }

    public String getDependentLocality() {
        return dependentLocality;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getSortingCode() {
        return sortingCode;
    }

    public String getOrganization() {
        return organization;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getLanguageCode() {
        return languageCode;
    }
}
