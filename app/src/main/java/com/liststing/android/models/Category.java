package com.liststing.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.liststing.android.datasets.ServiceCollection;

import org.json.JSONObject;
import org.w3c.dom.Element;

public class Category implements Parcelable {
    private long id;
    private String name;
    private ServiceCollection services;

    public Category(Long id, String name) {
        this.id = id;
        this.name = name;
        this.services = new ServiceCollection();
    }

    public Category(Long id, String name, ServiceCollection services) {
        this.id = id;
        this.name = name;
        this.services = services;
    }

    public Category(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.services = new ServiceCollection();
        in.readList(this.services, getClass().getClassLoader());
    }

    public static Category fromJson(JSONObject json) {
        long id = json.optLong("id");
        String name = json.optString("name");
        ServiceCollection services = ServiceCollection.fromJson(json);

        return new Category(id, name, services);
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public ServiceCollection getServices() {
        return services;
    }
    public void setServices(ServiceCollection services) {
        this.services = services;
    }

    @Override
    public String toString()
    {
        return getName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.name);
        parcel.writeList(this.services);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (id != category.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public String toXml() {
        StringBuilder builder = new StringBuilder();
        builder.append("<category>");
        builder.append("<id>" + getId() + "</id>");
        builder.append("<name>" + getName() + "</name>");
        builder.append("<services>");
        for(Service service : getServices()) {
            builder.append(service);
        }
        builder.append("</services>");
        builder.append("</category>");
        return builder.toString();
    }

    public static Category fromXml(Element categoryNode) {
        Long id = Long.parseLong(categoryNode.getElementsByTagName("id").item(0).getFirstChild().getTextContent());
        String name = categoryNode.getElementsByTagName("name").item(0).getFirstChild().getTextContent();
        ServiceCollection services = ServiceCollection.fromXml(categoryNode.getElementsByTagName("services"));

        return new Category(id, name, services);
    }
}