package com.liststing.android.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.datasets.AccountTable;
import com.liststing.android.datasets.BusinessCollection;
import com.liststing.android.networking.RestClient;
import com.liststing.android.networking.ServerInterfaces;
import com.liststing.android.networking.events.AccountEvents;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;

import de.greenrobot.event.EventBus;

public class Account implements Parcelable {

    private static final String CURRENT_USER_ENDPOINT = "/user/current";
    private long id;
    private String email;
    private String firstName;
    private String lastName;
    private String accessToken;
    private String avatarUrl;
    private String jabberId;

    private Business business;

    private static Account sAccount;
    private BusinessCollection favorites;

    public static Account getDefaultAccount() {
        if (sAccount == null) {
            sAccount = AccountTable.getDefaultAccount();
            if (sAccount == null) {
                sAccount = new Account();
            }
        }
        return sAccount;
    }

    public Account() {
        reset();
    }

    public Account(Parcel in) {
        this.id = in.readLong();
        this.email =  in.readString();
        this.firstName =  in.readString();
        this.lastName =  in.readString();
        this.accessToken =  in.readString();
        this.avatarUrl =  in.readString();
        this.jabberId = in.readString();

        favorites = new BusinessCollection();
        in.readList(favorites, getClass().getClassLoader());
    }

    public void updateFromJsonObject(JSONObject json) {
        id = json.optLong("id");
        email = json.optString("email");
        firstName = json.optString("first_name");
        lastName = json.optString("last_name");
        avatarUrl = json.optString("avatar");
        jabberId = json.optString("jabber_id");

        favorites = BusinessCollection.fromJson(json.optJSONArray("favorites"));
        business = Business.fromJson(json.optJSONObject("business"));
    }

    public void fetchAccountDetails() {
        RestClient.Listener listener = new RestClient.Listener() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    updateFromJsonObject(response);
                    save();
                    EventBus.getDefault().post(new AccountEvents.FetchingAccountEnded(ServerInterfaces.ServerResult.HAS_NEW));
                }
            }
        };
        RestClient.ErrorListener errorListener = new RestClient.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RecordedLog.e(RecordedLog.Tag.API, "Account > " +  error);
                EventBus.getDefault().post(new AccountEvents.FetchingAccountEnded(ServerInterfaces.ServerResult.FAILED));
            }
        };

        ListSting.getRestClient().get(CURRENT_USER_ENDPOINT, listener, errorListener);
    }

    public void reset() {
        id = 0;
        email = "";
        firstName = "";
        lastName = "";
        avatarUrl = "";
        accessToken = "";
        jabberId = "";
    }

    public void signout() {
        if(hasBusiness()) {
            business.signout();
        }
        reset();
        save();
    }

    public void save() {
        AccountTable.save(this);
        if(hasBusiness()) {
            business.save();
        }
    }

    public static boolean isBusinessProfileSignedIn() {
        return Account.isSignedIn() && Account.getDefaultAccount().hasBusiness();
    }
    public static boolean isCustomerProfileSignedIn() {
        return Account.isSignedIn() && !Account.getDefaultAccount().hasBusiness();
    }
    public static boolean isShowingPersonalBusiness(Business business) {
        return isBusinessProfileSignedIn() && getDefaultAccount().getBusiness().getId() == business.getId();
    }
    public static boolean isShowingOthersBusiness(Business business) {
        return isBusinessProfileSignedIn() && getDefaultAccount().getBusiness().getId() != business.getId();
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String mEmail) {
        this.email = mEmail;
    }

    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String mAccessToken) {
        this.accessToken = mAccessToken;
    }

    public boolean hasAccessToken() {
        return !TextUtils.isEmpty(getAccessToken());
    }
    public boolean hasBusiness() {
        return business != null;
    }

    public static boolean isSignedIn() {
        return getDefaultAccount().hasAccessToken();
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getAvatarUrl() {
        return avatarUrl;
    }
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getJabberId() {
        return jabberId;
    }

    public void setJabberId(String jabberId) {
        this.jabberId = jabberId;
    }


    public void setBusiness(Business business) {
        this.business = business;
    }

    public Business getBusiness() {
        return business;
    }

    public BusinessCollection getFavorites() {
        return favorites;
    }
    public void setFavorites(BusinessCollection favorites) {
        this.favorites = favorites;
    }

    public void addFavorite(Business mBusiness) {
        favorites.add(mBusiness);
    }
    public void removeFavorite(Business mBusiness) {
        for(Business favorite : favorites) {
            if(favorite.getId() == mBusiness.getId()) {
                favorites.remove(favorite);
                return;
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.email);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.accessToken);
        parcel.writeString(this.avatarUrl);
        parcel.writeString(this.jabberId);
        parcel.writeList(this.favorites);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    public String getUsername() {
        return String.format("%s %s", firstName, lastName);
    }
}
