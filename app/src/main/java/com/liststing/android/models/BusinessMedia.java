package com.liststing.android.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.liststing.android.util.NetworkUtils;

import org.json.JSONObject;

public class BusinessMedia implements Parcelable {
    private String image;
    private long id;

    public BusinessMedia(long id, String image) {
        this.id = id;
        this.image = image;
    }

    public BusinessMedia(Parcel in) {
        this.id = in.readLong();
        this.image = in.readString();
    }

    public static BusinessMedia fromJson(JSONObject json) {
        if(json == null) {
            return null;
        }
        long id = json.optLong("id");
        String image = NetworkUtils.optJsonString(json, "image");

        return new BusinessMedia(id, image);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.id);
        parcel.writeString(this.image);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public BusinessMedia createFromParcel(Parcel in) {
            return new BusinessMedia(in);
        }

        public BusinessMedia[] newArray(int size) {
            return new BusinessMedia[size];
        }
    };

}
