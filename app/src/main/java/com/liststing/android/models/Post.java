package com.liststing.android.models;

import android.text.TextUtils;

import com.liststing.android.util.DateTimeUtils;

public class Post {
    private long postId;
    private long vendorId;

    private int numEndorsed;
    private int numComments;

    private String vendorName;
    private String vendorAvatar;
    private String vendorMessage;
    private String vendorImage;
    private String vendorVideo;

    /**
     * used for sorting
     */
    public long timestamp;
    private String datePublished;

    public boolean isEndoredByCurrentUser;

    public Post(long postId, long vendorId, String vendorName, String vendorMessage, String vendorAvatar,
                String vendorImage, String vendorVideo, int numEndorsed, int numComments, long timestamp,
                String datePublished, boolean isEndoredByCurrentUser) {
        this.postId = postId;
        this.vendorId = vendorId;
        this.numEndorsed = numEndorsed;
        this.numComments = numComments;
        this.vendorName = vendorName;
        this.vendorAvatar = vendorAvatar;
        this.vendorMessage = vendorMessage;
        this.vendorImage = vendorImage;
        this.vendorVideo = vendorVideo;
        this.timestamp = timestamp;
        this.datePublished = datePublished;
        this.isEndoredByCurrentUser = isEndoredByCurrentUser;
    }

    public boolean hasMessage() {
        return TextUtils.isEmpty(vendorMessage);
    }

    public long getPostId() {
        return postId;
    }
    public void setPostId(long postId) {
        this.postId = postId;
    }

    public long getVendorId() {
        return vendorId;
    }
    public void setVendorId(long vendorId) {
        this.vendorId = vendorId;
    }

    public int getNumEndorsed() {
        return numEndorsed;
    }
    public void setNumEndorsed(int numEndorsed) {
        this.numEndorsed = numEndorsed;
    }

    public int getNumComments() {
        return numComments;
    }
    public void setNumComments(int numComments) {
        this.numComments = numComments;
    }

    public String getVendorName() {
        return vendorName;
    }
    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorAvatar() {
        return vendorAvatar;
    }
    public void setVendorAvatar(String vendorAvatar) {
        this.vendorAvatar = vendorAvatar;
    }

    public String getVendorMessage() {
        return vendorMessage;
    }
    public void setVendorMessage(String vendorMessage) {
        this.vendorMessage = vendorMessage;
    }

    public String getVendorVideo() {
        return vendorVideo;
    }
    public void setVendorVideo(String vendorVideo) {
        this.vendorVideo = vendorVideo;
    }

    public String getVendorImage() {
        return vendorImage;
    }
    public void setVendorImage(String vendorImage) {
        this.vendorImage = vendorImage;
    }

    public boolean isEndoredByCurrentUser() {
        return isEndoredByCurrentUser;
    }
    public void setEndoredByCurrentUser(boolean isEndoredByCurrentUser) {
        this.isEndoredByCurrentUser = isEndoredByCurrentUser;
    }


    public String getDatePublishedText() {
        return datePublished;
    }
    public void setDatePublishedText(String datePublished) {
        this.datePublished = datePublished;
    }

    private transient java.util.Date publishedDate;
    public java.util.Date getDatePublished() {
        if (publishedDate == null) {
            publishedDate = DateTimeUtils.iso8601ToJavaDate(datePublished);
        }
        return publishedDate;
    }
    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }

    public long getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
