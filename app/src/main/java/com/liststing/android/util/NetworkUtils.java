package com.liststing.android.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.liststing.android.ListSting;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NetworkUtils {
    public static final String PARAMS_ENCODING = "UTF-8";

    private NetworkUtils() {}

    /**
     * Retrieves the string value from the JSONObject and prevents returning
     * the string "null"
     * @param json
     * @param key
     * @return
     */
    public static String optJsonString(JSONObject json, String key) {
        String value = json.optString(key);
        return value == "null" ? "" : value;
    }

    public static String appendEndPoint(String url) {
        // already contains endpoint
        if (url.indexOf(ListSting.API_ENDPOINT) == 0) {
            return url;
        }
        // if it has a leading slash, remove it
        if (url.indexOf("/") == 0) {
            url = url.substring(1);
        }
        // prepend the endpoint
        return String.format("%s%s", ListSting.API_ENDPOINT, url);
    }


    public static String appendParams(String url, Map<String, Object> params) {
        if (params != null && params.size() > 0) {
            StringBuilder builder = new StringBuilder(url + '?');
            for (HashMap.Entry<String, Object> entry : params.entrySet()) {
                Object value = entry.getValue();
                if (value != null && value instanceof ArrayList) {
                    ArrayList<String> list = (ArrayList<String>) value;
                    for(String val : list) {
                        builder.append("&").append(entry.getKey());
                        builder.append("=").append(val);
                    }
                }
                if (value != null && value instanceof String) {
                    builder.append("&").append(entry.getKey());
                    builder.append("=").append(value);
                }
            }
            builder.deleteCharAt(url.length() + 1); // remove first '&'
            return builder.toString();
        }
        return url;
    }

    /**
     * returns information on the active network connection
     */
    private static NetworkInfo getActiveNetworkInfo(Context context) {
        if (context == null) {
            return null;
        }
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return null;
        }
        // note that this may return null if no network is currently active
        return cm.getActiveNetworkInfo();
    }

    /**
     * returns true if a network connection is available
     */
    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo info = getActiveNetworkInfo(context);
        return (info != null && info.isConnected());
    }


    /*
     * attempts to return JSON from a volleyError - useful for WP REST API failures, which often
     * contain JSON in the response
     */
    public static JSONObject volleyErrorToJSON(VolleyError volleyError) {
        if (volleyError == null || volleyError.networkResponse == null || volleyError.networkResponse.data == null
                || volleyError.networkResponse.headers == null) {
            return null;
        }

        String contentType = volleyError.networkResponse.headers.get("Content-Type");
        if (contentType == null || !contentType.equals("application/json")) {
            return null;
        }

        try {
            String response = new String(volleyError.networkResponse.data, "UTF-8");
            JSONObject json = new JSONObject(response);
            return json;
        } catch (UnsupportedEncodingException e) {
            return null;
        } catch (JSONException e) {
            return null;
        }
    }

    /*
     * cancel all Volley requests
     */
    public static void cancelAllRequests(RequestQueue requestQueue) {
        if (requestQueue==null)
            return;
        RequestQueue.RequestFilter filter = new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        };
        requestQueue.cancelAll(filter);
    }
}
