package com.liststing.android.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.android.i18n.addressinput.AddressWidget;
import com.google.i18n.addressinput.common.AddressData;
import com.liststing.android.ListSting;
import com.liststing.android.models.Business;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.ParserConfigurationException;


public class LocationUtils implements LocationListener {

    private final Context mContext;
    private final LocationManager mLocationManager;
    private LocationResult mLocationResult;
    private final Timer fallbackTimer;
    private static final int FALLBACK_TIME = 5000; // (10 seconds)

    public LocationUtils(Context context) {
        mContext = context;
        fallbackTimer = new Timer();
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * 1. Checks what provideres are enabled
     * 2. if any of the two providers are enabled, the corresponding location listener is started
     * 3. Return a location if we get an update from the listeners; stop all other updates ( grabs location only one time ).
     * 4. If listeners cannot find a location, we fallback to lastKnownValues
     * @param locationResult
     */
    public void requestLocation(LocationResult locationResult) {
        if(locationResult == null)
            return;

        mLocationResult = locationResult;
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if(isGPSEnabled) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }

        if(isNetworkEnabled) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }

        fallbackTimer.schedule(new GetLastLocation(), FALLBACK_TIME);
    }

    public Location getLastLocation() {
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location location = mLocationManager.getLastKnownLocation(provider);
            if (location == null) {
                continue;
            }
            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = location;
            }
        }
        return bestLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        fallbackTimer.cancel();
        mLocationManager.removeUpdates(this);
        mLocationResult.gotLocation(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    public interface LocationResult {
        public void gotLocation(Location location);
    }

    private class GetLastLocation extends TimerTask {
        @Override
        public void run() {
            mLocationManager.removeUpdates(LocationUtils.this);

            boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if(isGPSEnabled) {
                Location gpsLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                mLocationResult.gotLocation(gpsLocation);
            }

            if(isNetworkEnabled) {
                Location networkLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                mLocationResult.gotLocation(networkLocation);
            }
            // TODO: could extend this class to return the latest location
        }
    }

    public static String buildLocationName(AddressData addressData, boolean withCountry) {
        String location = "";
        List<String> address = AddressWidget.getFullEnvelopeAddress(addressData);
        for(int i = 0; i < address.size(); i++) {
            location += address.get(i);
            if(i < address.size() - 1) {
                location += ", ";
            }
        }
        if(withCountry) {
            return location + ", " + getLocalCountryName(addressData.getPostalCountry());
        }
        return location;
    }

    public static String buildLocationName(AddressData addressData) {
        return buildLocationName(addressData, true);
    }

    public static String getLocalCountryName(String regionCode) {
        return (new Locale("", regionCode)).getDisplayCountry(Locale.getDefault());
    }

    public static Address getAddress(String location, Context context) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(location, 1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            RecordedLog.e(RecordedLog.Tag.LIST_BUSINESS, "Location I/O > \n" + ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            RecordedLog.e(RecordedLog.Tag.LIST_BUSINESS, "Location invalid > \n" + illegalArgumentException);
        }

        if(addresses != null && addresses.size() == 1) {
            return addresses.get(0);
        }
        return null;
    }

    public static Address getAddress(Double latitude, Double longitude, Context context) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(
                    latitude,
                    longitude,
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            RecordedLog.e(RecordedLog.Tag.LIST_BUSINESS, "Location I/O > \n" + ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            RecordedLog.e(RecordedLog.Tag.LIST_BUSINESS, "Location invalid > \n" + illegalArgumentException);
        }

        if(addresses != null && addresses.size() == 1) {
            return addresses.get(0);
        }
        return null;
    }

    public static AddressData parseAddressData(Business business) {
        String address = business.getAddressData();
        try {
            Document addressXML = com.liststing.android.util.TextUtils.stringToDom(address);

            String country = getElement(addressXML, "POSTAL_COUNTRY");
            String languageCode = getElement(addressXML, "LANGUAGE");
            String adminArea = getElement(addressXML, "ADMIN_AREA");
            String locality = getElement(addressXML, "LOCALITY");
            String dependentLocality = getElement(addressXML, "DEPENDENT_LOCALITY");
            String postalCode = getElement(addressXML, "POSTAL_CODE").toUpperCase();
            String sortingCode = getElement(addressXML, "SORTING_CODE");
            String organization = getElement(addressXML, "ORGANIZATION");
            String recipient = getElement(addressXML, "RECIPIENT");

            ArrayList<String> streets = new ArrayList<>();
            NodeList streetList = addressXML.getElementsByTagName("STREETS");
            for(int i = 0 ; i < streetList.getLength(); i ++) {
                Element street = (Element) streetList.item(i);
                streets.add(street.getFirstChild().getTextContent());
            }

            return AddressData.builder()
                    .setCountry(country)
                    .setLanguageCode(languageCode)
                    .setAdminArea(adminArea)
                    .setLocality(locality)
                    .setDependentLocality(dependentLocality)
                    .setPostalCode(postalCode)
                    .setSortingCode(sortingCode)
                    .setOrganization(organization)
                    .setRecipient(recipient)
                    .setAddressLines(streets)
                    .build();

        } catch (SAXException e) {
            RecordedLog.e(RecordedLog.Tag.BUSINESS, e);
        } catch (ParserConfigurationException e) {
            RecordedLog.e(RecordedLog.Tag.BUSINESS, e);
        } catch (IOException e) {
            RecordedLog.e(RecordedLog.Tag.BUSINESS, e);
        }
        return null;
    }

    // todo: move to an xml util class
    private static String getElement(Document doc, String tagName) {
        String value = doc.getElementsByTagName(tagName).item(0).getFirstChild().getTextContent();
        if(value.equals("null"))
            return "";
        return value;
    }

    /**
     * Returns the approximate distance in meters between Location A and Location B.
     * @param locA
     * @param locB
     * @return
     */
    public static float distanceBetween(Location locA, Location locB) {
        return locA.distanceTo(locB);
    }

    /**
     * Returns the approximate distance between the current location and the business location
     * @param addressData
     * @return
     */
    public static String calculateBusinessDistance(AddressData addressData, Context context) {
        String businessLocation = LocationUtils.buildLocationName(addressData);
        Address address = LocationUtils.getAddress(businessLocation, context);
        LocationUtils locationUtils = new LocationUtils(context);
        Location location = locationUtils.getLastLocation();
        if(address == null || location == null) {
            return "";
        }
        Location addressLocation = new Location("");
        addressLocation.setLatitude(address.getLatitude());
        addressLocation.setLongitude(address.getLongitude());
        return distance(distanceBetween(location, addressLocation));
    }

    private static String distance(float meters) {
        boolean inKM = true;
        if(inKM && meters > 1000) {
            return String.format("%s km",new DecimalFormat("#.#").format(meters / 1000));
        }
        return String.format("%.0f m", meters);
    }
}