package com.liststing.android.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

public class SqlUtils {

    private SqlUtils() {}

    public static long boolToSql(boolean value) {
        return value ? 1 : 0;
    }

    public static boolean sqlToBool(int value) {
        return value != 0;
    }

    public static void closeStatement(SQLiteStatement stmt) {
        if (stmt != null) {
            stmt.close();
        }
    }
    
    public static void closeCursor(Cursor c) {
        if (c != null && !c.isClosed()) {
            c.close();
        }
    }

    /*
     * Android's CursorWindow has a max size of 2MB per row which can be exceeded
     * with a very large text column, causing an IllegalStateException when the
     * row is read - prevent this by limiting the amount of text that's stored in
     * the text column - note that this situation very rarely occurs
     * https://github.com/android/platform_frameworks_base/blob/master/core/res/res/values/config.xml#L946
     * https://github.com/android/platform_frameworks_base/blob/3bdbf644d61f46b531838558fabbd5b990fc4913/core/java/android/database/CursorWindow.java#L103
     */
    private static final int MAX_TEXT_LEN = (1024 * 1024) / 2;
    public static String truncateMessage(final String message) {
        if (message.length() <= MAX_TEXT_LEN) {
            return message;
        }
        RecordedLog.w(RecordedLog.Tag.FEED, "table > max text exceeded, storing truncated text");
        return message.substring(0, MAX_TEXT_LEN);
    }
}
