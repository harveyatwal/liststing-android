package com.liststing.android.util;


import android.content.Context;
import android.util.Patterns;
import android.widget.EditText;

import com.liststing.android.ListSting;
import com.liststing.android.R;

public class ValidationUtils {
    private ValidationUtils() {}

    public static boolean requiredField(Context context, EditText textBox) {
        final String text = TextUtils.getText(textBox).trim();
        boolean isValid = true;

        if (text.equals("")) {
            textBox.setError(context.getString(R.string.required_field));
            textBox.requestFocus();
            isValid = false;
        }
        return isValid;
    }

    public static boolean isValidEmailNoInputRequired(EditText input) {
        String email = TextUtils.getText(input).trim();
        if(email.length() == 0) {
            return true; // Valid if no input
        }
        return isValidEmailInputRequired(input);
    }

    public static boolean isValidEmailInputRequired(EditText input) {
        final String email = TextUtils.getText(input).trim();
        //!Patterns.EMAIL_ADDRESS.matcher(email).matches() || email.length() > 255
        return Patterns.EMAIL_ADDRESS.matcher(email).matches() && email.length() <= ListSting.getContext().getResources().getInteger(R.integer.max_email);
    }

    public static boolean isDigitOnly(EditText input) {
        String digits = TextUtils.getText(input).trim();
        return digits.matches("[0-9]+");
    }

    public static boolean isValidPhoneNumber(EditText mCountryCode, EditText mPhoneNumber) {
        final String phoneNumber = TextUtils.getText(mPhoneNumber).trim();
        final String countryCode = TextUtils.getText(mCountryCode).trim();
        int phoneLength = countryCode.length() + phoneNumber.length();

        return (phoneLength >= ListSting.getContext().getResources().getInteger(R.integer.phone_number_start_range) &&
                phoneLength <= ListSting.getContext().getResources().getInteger(R.integer.phone_number_end_range));
    }
}
