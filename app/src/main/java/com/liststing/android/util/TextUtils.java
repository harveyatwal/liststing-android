package com.liststing.android.util;

import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class TextUtils {

    private TextUtils() {}

    /**
     * returns text string from passed TextView
     */
    public static String getText(TextView textView) {
        if (textView.getText() == null) {
            return "";
        }
        return textView.getText().toString();
    }

    /**
     * returns text string from passed EditText
     */
    public static String getText(EditText edit) {
        if (edit.getText() == null) {
            return "";
        }
        return edit.getText().toString();
    }

    /**
     * Converts a String to an XML document
     * @param xmlSource
     * @return
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public static Document stringToDom(String xmlSource)
            throws SAXException, ParserConfigurationException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xmlSource)));
    }
}
