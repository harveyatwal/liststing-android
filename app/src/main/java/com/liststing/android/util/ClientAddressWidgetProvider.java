package com.liststing.android.util;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.i18n.addressinput.AddressWidgetUiComponentProvider;
import com.liststing.android.R;
import com.google.i18n.addressinput.common.AddressField;

public class ClientAddressWidgetProvider extends AddressWidgetUiComponentProvider {

    public ClientAddressWidgetProvider(Context context) {
        super(context);
    }

    protected TextView createUiLabel(CharSequence label, AddressField.WidthType widthType) {
        TextView textView = (TextView) inflater.inflate(R.layout.address_textview, null, false);
        textView.setText(label);
        return textView;
    }

    protected EditText createUiTextField(AddressField.WidthType widthType) {
        return (EditText) inflater.inflate(R.layout.address_edittext, null, false);
    }

    protected Spinner createUiPickerSpinner(AddressField.WidthType widthType) {
        return (Spinner) inflater.inflate(R.layout.address_spinner, null, false);
    }

    protected ArrayAdapter<String> createUiPickerAdapter(AddressField.WidthType widthType) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.address_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

}
