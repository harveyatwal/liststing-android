package com.liststing.android.util;

import android.text.format.DateUtils;

import com.liststing.android.ListSting;
import com.liststing.android.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtils {

    private DateTimeUtils() {}

    /**
     * instantiating new DateFormat instances is very inefficient. To increase effeciency,
     * the DateFormat is stored as a class variable.
     * http://www.javacodegeeks.com/2010/07/java-best-practices-dateformat-in.html
     */
    private static final ThreadLocal<DateFormat> ISO8601Format = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
        }
    };

    /*
     * converts an ISO8601 date to a Java date
     */
    public static Date iso8601ToJavaDate(final String strDate) {
        try {
            DateFormat formatter = ISO8601Format.get();
            return formatter.parse(strDate);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Used to conver a date into a time span ("5m", "1d", etc)
     */
    public static String dateToTimeSpan(final Date date) {
        if (date == null)
            return "";

        long passedTime = date.getTime();
        long currentTime = System.currentTimeMillis();

        // less than a minute (ex: now)
        long secondsSince = (currentTime - passedTime) / 1000;
        if (secondsSince < 60)
            return ListSting.getContext().getString(R.string.feed_timespan_now);

        // less than an hour (ex: 12m)
        long minutesSince = secondsSince / 60;
        if (minutesSince < 60)
            return Long.toString(minutesSince) + "m";

        // less than a day (ex: 17h)
        long hoursSince = minutesSince / 60;
        if (hoursSince < 24)
            return Long.toString(hoursSince) + "h";

        // less than a week (ex: 5d)
        long daysSince = hoursSince / 24;
        if (daysSince < 7)
            return Long.toString(daysSince) + "d";

        // return day/month without year (ex: Jan 30)
        if (daysSince < 365)
            return DateUtils.formatDateTime(ListSting.getContext(), passedTime, DateUtils.FORMAT_NO_YEAR | DateUtils.FORMAT_ABBREV_ALL);

        // date is older, so include year (ex: Jan 30, 2013)
        return DateUtils.formatDateTime(ListSting.getContext(), passedTime, DateUtils.FORMAT_ABBREV_ALL);
    }

}
