package com.liststing.android.util;

import android.util.Log;

import com.liststing.android.ListSting;

/**
 * wrapper for android log calls
 */
public class RecordedLog {

    public static final String TAG = ListSting.NAME;
    public enum Tag {MAIN, FEED, EXPLORE, PROFILE, API, APPLICATION, LIST_BUSINESS, CATEGORY, BUSINESS, BUSINESS_LIST, SERVICE_COLLECTION, SEARCH, CHAT, MESSENGER, VOLLEY}

    private RecordedLog() {}

    public static void v(Tag tag, String message) {
        Log.v(TAG + "-" + tag.toString(), message);
    }

    public static void d(Tag tag, String message) {
        Log.d(TAG + "-" + tag.toString(), message);
    }

    public static void i(Tag tag, String message) {
        Log.i(TAG + "-" + tag.toString(), message);
    }

    public static void w(Tag tag, String message) {
        Log.w(TAG + "-" + tag.toString(), message);
    }

    public static void e(Tag tag, String message) {
        Log.e(TAG + "-" + tag.toString(), message);
    }

    public static void e(Tag tag, Throwable error) {
        Log.e(TAG + "-" + tag.toString(), error.getMessage(), error);
    }

}
