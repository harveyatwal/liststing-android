package com.liststing.android;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.liststing.android.datasets.AccountTable;
import com.liststing.android.models.Account;
import com.liststing.android.networking.BitmapCache;
import com.liststing.android.networking.OkHttpStack;
import com.liststing.android.networking.RestClient;
import com.liststing.android.networking.RestRequest;
import com.liststing.android.ui.explore.ExploreActivity;
import com.liststing.android.util.NetworkUtils;


import de.greenrobot.event.EventBus;

public class ListSting extends Application {
    public static final String NAME = "ListSting";
    public static final String DOMAIN = "liststing.com";
    public static final String ENDPOINT = "52.89.26.136";
    public static final String API_ENDPOINT = "http://" + ENDPOINT + "/";

    public static final String OAUTH_APP_ID = "QEeuIPI2M2fXcfjtQtm9i1KO46qelCQejwRPDsCu";
    public static final String OAUTH_APP_SECRET = "G5QjIYJS3L3JIyd7x6bXcAnJN0bRqYTm4cXgAjHC4zDHBErdouZq7Mm80TFspHOxCmLzzxyckzCKXmpgn83ym7ASYPy16L4HUXazfWIBly9ODQNj1v5mDpOwJqBp0Q2m";
    public static final String OAUTH_REDIRECT_URI = "http://andorid.liststing.com/";

    private static RequestQueue requestQueue;
    private static ImageLoader imageLoader;

    private static Context mContext;
    private static RestClient mRestClient;
    private static BitmapCache mBitmapCache;


    /**
     * Callback for response that failed authentication
     */
    private static RestRequest.OnAuthFailedListener mOnAuthFailedListener = new RestRequest.OnAuthFailedListener() {
        @Override
        public void onAuthFailed() {
            if (getContext() == null) return;
            // token is no longer valid / server offline
            // safely logout and return to sign in page
            logout();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        initializeEventBus();
        initializeVolley();
    }

    private void initializeVolley() {
        requestQueue = Volley.newRequestQueue(mContext, new OkHttpStack());
        imageLoader = new ImageLoader(requestQueue, getBitmapCache());
        imageLoader.setBatchedResponseDelay(0);     // http://stackoverflow.com/a/17035814

        VolleyLog.setTag(NAME);
    }

    public static BitmapCache getBitmapCache() {
        if (mBitmapCache == null) {
            // Only allocate 1/16 of the application memory to our cache
            int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            int cacheSize = maxMemory / 16;
            mBitmapCache = new BitmapCache(cacheSize);
        }
        return mBitmapCache;
    }

    private void initializeEventBus() {
        EventBus.TAG = "Placeholder-EVENT";
        EventBus.builder()
                .logNoSubscriberMessages(false)
                .sendNoSubscriberEvent(false)
                .throwSubscriberException(true)
                .installDefaultEventBus();
    }

    public static Spanned getTypeface() {
        String list = mContext.getString(R.string.name_list);
        String sting = mContext.getString(R.string.name_sting);
        return Html.fromHtml(String.format("<font color=#ffffff>%s</font><font color=#424242>%s</font>", list, sting));
    }
    public static void setTypeface(TextView view) {
        view.setText(getTypeface());
    }

    public static Context getContext() {
        return mContext;
    }

    public static RestClient getRestClient() {
        if(mRestClient == null) {
            mRestClient = new RestClient(requestQueue, mOnAuthFailedListener);
        }
        return mRestClient;
    }
    public static RequestQueue getRequestQueue() {
        return requestQueue;
    }
    public static ImageLoader getImageLoader() {
        return imageLoader;
    }

    public static void logout() {
        NetworkUtils.cancelAllRequests(requestQueue);
        Account account = Account.getDefaultAccount();
        if(account.isSignedIn()) {
            account.signout();
            AccountTable.dropRows(ListStingDatabase.getWritableDb());

            Intent intent = new Intent(getContext(), ExploreActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            getContext().startActivity(intent);
        }
    }
}