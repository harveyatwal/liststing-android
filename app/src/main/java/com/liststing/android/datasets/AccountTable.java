package com.liststing.android.datasets;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.liststing.android.ListStingDatabase;
import com.liststing.android.models.Account;
import com.liststing.android.util.SqlUtils;

/**
 * tbl_accounts houses the current user information
 */
public class AccountTable {

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE tbl_accounts ("
                + "local_id                INTEGER PRIMARY KEY DEFAULT 0,"
                + "id                      INTEGER,"
                + "jabber_id               TEXT,"
                + "email                   TEXT,"
                + "avatar_url              TEXT,"
                + "first_name              TEXT,"
                + "last_name               TEXT,"
                + "access_token            TEXT)");
    }

    public static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS tbl_accounts");
    }

    public static void dropRows(SQLiteDatabase db) {
        db.delete("tbl_accounts", null, null);
    }

    public static void save(Account account) {
        save(account, ListStingDatabase.getWritableDb());
    }

    private static void save(Account account, SQLiteDatabase database) {
        ContentValues values = new ContentValues();
        values.put("local_id", 0);
        values.put("id", account.getId());
        values.put("jabber_id", account.getJabberId());
        values.put("email", account.getEmail());
        values.put("avatar_url", account.getAvatarUrl());
        values.put("first_name", account.getFirstName());
        values.put("last_name", account.getLastName());
        values.put("access_token", account.getAccessToken());
        database.insertWithOnConflict("tbl_accounts", null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static Account getDefaultAccount() {
        return getAccountByLocalId(0);
    }

    private static Account getAccountByLocalId(long localId) {
        Account account = new Account();

        String[] args = {Long.toString(localId)};
        Cursor c = ListStingDatabase.getReadableDb().rawQuery("SELECT * FROM tbl_accounts WHERE local_id=?", args);

        try {
            if (c.moveToFirst()) {
                account.setId(c.getLong(c.getColumnIndex("id")));
                account.setJabberId(c.getString(c.getColumnIndex("jabber_id")));
                account.setEmail(c.getString(c.getColumnIndex("email")));
                account.setAvatarUrl(c.getString(c.getColumnIndex("avatar_url")));
                account.setFirstName(c.getString(c.getColumnIndex("first_name")));
                account.setLastName(c.getString(c.getColumnIndex("last_name")));
                account.setAccessToken(c.getString(c.getColumnIndex("access_token")));
                account.setBusiness(BusinessTable.getBusiness(account.getId()));
            }
            return account;
        } finally {
            SqlUtils.closeCursor(c);
        }
    }
}
