package com.liststing.android.datasets;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.liststing.android.ListStingDatabase;
import com.liststing.android.models.Account;
import com.liststing.android.models.Business;
import com.liststing.android.util.SqlUtils;

/**
 * tbl_businesses houses the business information
 */
public class BusinessTable {

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE tbl_businesses ("
                + "id                      INTEGER PRIMARY KEY,"
                + "admin_id                INTEGER,"
                + "industry                INTEGER,"
                + "business_name           TEXT,"
                + "description             TEXT,"
                + "phone_number            TEXT,"
                + "email                   TEXT,"
                + "website                 TEXT,"
                + "services                TEXT,"
                + "address_data            TEXT,"
                + "show_location           INTEGER,"
                + "avatar_url              TEXT)");
    }

    public static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS tbl_businesses");
    }

    public static void saveAdmin(Business business) {
        save(business, ListStingDatabase.getWritableDb(), Account.getDefaultAccount().getId());
    }

    private static void save(Business business, SQLiteDatabase database, long adminId) {
        ContentValues values = new ContentValues();
        values.put("id", business.getId());
        values.put("admin_id", adminId);
        values.put("industry", business.getIndustryId());
        values.put("business_name", business.getBusinessName());
        values.put("description", business.getDescription());
        values.put("phone_number", business.getPhoneNumber());
        values.put("email", business.getEmail());
        values.put("website", business.getWebsite());
        values.put("avatar_url", business.getAvatarUrl());
        values.put("services", business.getServices().toXml());
        values.put("address_data", business.getAddressData());
        values.put("show_location", business.canShowLocation() ? 1 : 0);
        database.insertWithOnConflict("tbl_businesses", null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static Business getBusiness(long adminId) {
        Business business = new Business();

        String[] args = {Long.toString(adminId)};
        Cursor c = ListStingDatabase.getReadableDb().rawQuery("SELECT * FROM tbl_businesses WHERE admin_id=?", args);

        try {
            if (c.moveToFirst()) {
                business.setId(c.getLong(c.getColumnIndex("id")));
                business.setIndustryId(c.getLong(c.getColumnIndex("industry")));
                business.setBusinessName(c.getString(c.getColumnIndex("business_name")));
                business.setDescription(c.getString(c.getColumnIndex("description")));
                business.setPhoneNumber(c.getString(c.getColumnIndex("phone_number")));
                business.setEmail(c.getString(c.getColumnIndex("email")));
                business.setWebsite(c.getString(c.getColumnIndex("website")));
                business.setShowLocation(c.getInt(c.getColumnIndex("show_location")) > 0);
                business.setAddressData(c.getString(c.getColumnIndex("address_data")));
                business.setAvatarUrl(c.getString(c.getColumnIndex("avatar_url")));
                business.setServices(ServiceCollection.fromXml(c.getString(c.getColumnIndex("services"))));
                return business;
            }
            return null;
        } finally {
            SqlUtils.closeCursor(c);
        }
    }
}
