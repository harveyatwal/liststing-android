package com.liststing.android.datasets;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.liststing.android.ListStingDatabase;
import com.liststing.android.models.Post;
import com.liststing.android.util.RecordedLog;
import com.liststing.android.util.SqlUtils;

/**
 * tbl_feed_posts contains all feed posts
 */
public class FeedPostTable {
    private static final String COLUMN_NAMES =
              "post_id,"
            + "vendor_id,"
            + "vendor_name,"
            + "vender_message,"
            + "vendor_avatar,"
            + "vendor_image,"
            + "vendor_video,"
            + "num_endorsed,"
            + "num_comments,"
            + "timestamp,"
            + "date_published,"
            + "is_endorsed";

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE tbl_feed_posts ("
                + "post_id          INTEGER DEFAULT 0,"
                + "vendor_id        INTEGER DEFAULT 0,"
                + "vendor_name      TEXT,"
                + "vender_message   TEXT,"
                + "vender_avatar    TEXT,"
                + "vendor_image     TEXT,"
                + "vendor_video     TEXT,"
                + "num_endorsed     INTEGER DEFAULT 0,"
                + "num_comments     INTEGER DEFAULT 0,"
                + "timestamp        INTEGER DEFAULT 0,"
                + "date_published   TEXT,"
                + "is_endorsed      INTEGER DEFAULT 0,"
                + "PRIMARY KEY (post_id, vendor_id)"
                + ")");

        db.execSQL("CREATE INDEX idx_feed_posts_timestamp ON tbl_feed_posts(timestamp)");
    }

    public static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS tbl_feed_posts");
    }

    public static void reset(SQLiteDatabase db) {
        dropTables(db);
        createTable(db);
    }

    public static void addOrUpdatePost(Post post) {
        if (post == null) {
            return;
        }
        PostCollection posts = new PostCollection();
        posts.add(post);
        addOrUpdatePosts(posts);
    }
    public static void addOrUpdatePosts(PostCollection posts) {
        if (posts == null || posts.size() == 0) {
            return;
        }

        SQLiteDatabase db = ListStingDatabase.getWritableDb();
        SQLiteStatement postsStmt = db.compileStatement(
                "INSERT OR REPLACE INTO tbl_feed_posts ("
                        + COLUMN_NAMES
                        + ") VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12)");

        db.beginTransaction();
        try {
            for (Post post: posts) {
                postsStmt.bindLong(1, post.getPostId());
                postsStmt.bindLong(2, post.getVendorId());
                postsStmt.bindString(3, post.getVendorName());
                postsStmt.bindString(4, SqlUtils.truncateMessage(post.getVendorMessage()));
                postsStmt.bindString(5, post.getVendorAvatar());
                postsStmt.bindString(6, post.getVendorImage());
                postsStmt.bindString(7, post.getVendorVideo());
                postsStmt.bindLong(8, post.getNumEndorsed());
                postsStmt.bindLong(9, post.getNumComments());
                postsStmt.bindLong(10, post.getTimestamp());
                postsStmt.bindString(11, post.getDatePublishedText());
                postsStmt.bindLong(12, SqlUtils.boolToSql(post.isEndoredByCurrentUser()));
                postsStmt.execute();
            }

            db.setTransactionSuccessful();

        } finally {
            db.endTransaction();
            SqlUtils.closeStatement(postsStmt);
        }
    }

    public static PostCollection getPosts(int maxPosts) {
        String sql = "SELECT * FROM tbl_feed_posts"
                   + " ORDER BY tbl_feed_posts.timestamp DESC";

        if (maxPosts > 0) {
            sql += " LIMIT " + Integer.toString(maxPosts);
        }

        String[] args = {};
        Cursor cursor = ListStingDatabase.getReadableDb().rawQuery(sql, args);
        try {
            return getPostCollectionFromCursor(cursor);
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }


    private static Post getPostFromCursor(Cursor c) {
        if (c == null) {
            throw new IllegalArgumentException("getPostFromCursor > null cursor");
        }

        Long postId = c.getLong(c.getColumnIndex("post_id"));
        Long vendorId = c.getLong(c.getColumnIndex("vendor_id"));
        String vendorName = c.getString(c.getColumnIndex("vendor_name"));
        String vendorMessage = c.getString(c.getColumnIndex("vender_message"));
        String vendorAvatar = c.getString(c.getColumnIndex("vender_avatar"));
        String vendorImage = c.getString(c.getColumnIndex("vendor_image"));
        String vendorVideo = c.getString(c.getColumnIndex("vendor_video"));
        int numEndorsed = c.getInt(c.getColumnIndex("num_endorsed"));
        int numComments = c.getInt(c.getColumnIndex("num_comments"));
        Long timestamp = c.getLong(c.getColumnIndex("timestamp"));
        String datePublished = c.getString(c.getColumnIndex("date_published"));
        boolean isEndorsed = SqlUtils.sqlToBool(c.getInt(c.getColumnIndex("is_endorsed")));

        return new Post(postId, vendorId, vendorName, vendorMessage, vendorAvatar,
                vendorImage, vendorVideo, numEndorsed, numComments, timestamp, datePublished,
                isEndorsed);
    }

    private static PostCollection getPostCollectionFromCursor(Cursor cursor) {
        PostCollection posts = new PostCollection();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    posts.add(getPostFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (IllegalStateException e) {
            RecordedLog.e(RecordedLog.Tag.FEED, e);
        }
        return posts;

    }
}