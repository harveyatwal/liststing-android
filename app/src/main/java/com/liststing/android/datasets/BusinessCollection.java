package com.liststing.android.datasets;

import com.liststing.android.models.Business;
import com.liststing.android.models.BusinessMedia;

import org.json.JSONArray;

import java.util.ArrayList;

public class BusinessCollection extends ArrayList<Business> {

    public static BusinessCollection fromJson(JSONArray jsonBusinesses) {
        BusinessCollection businesses = new BusinessCollection();
        if(jsonBusinesses != null) {
            for(int i = 0; i < jsonBusinesses.length(); i++) {
                businesses.add(Business.fromJson(jsonBusinesses.optJSONObject(i)));
            }
        }
        return businesses;
    }
}
