package com.liststing.android.datasets;

import com.liststing.android.models.Category;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryCollection extends ArrayList<Category> {

    public static CategoryCollection fromJson(JSONObject json) {
        CategoryCollection categories = new CategoryCollection();
        JSONArray jsonCategories = json.optJSONArray("categories");
        if(jsonCategories != null) {
            for(int i = 0; i < jsonCategories.length(); i++) {
                categories.add(Category.fromJson(jsonCategories.optJSONObject(i)));
            }
        }
        return categories;
    }


}
