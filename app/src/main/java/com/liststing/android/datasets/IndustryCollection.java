package com.liststing.android.datasets;

import com.liststing.android.models.Industry;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class IndustryCollection extends ArrayList<Industry> {

    public static IndustryCollection fromJson(JSONObject json) {
        IndustryCollection industries = new IndustryCollection();
        JSONArray jsonIndustries = json.optJSONArray("industries");
        if(jsonIndustries != null) {
            for(int i = 0; i < jsonIndustries.length(); i++) {
                industries.add(Industry.fromJson(jsonIndustries.optJSONObject(i)));
            }
        }
        return industries;
    }
}
