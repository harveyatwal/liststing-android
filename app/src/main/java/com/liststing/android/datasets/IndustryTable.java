package com.liststing.android.datasets;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.liststing.android.ListStingDatabase;
import com.liststing.android.models.Industry;
import com.liststing.android.networking.ServerInterfaces;
import com.liststing.android.util.RecordedLog;
import com.liststing.android.util.SqlUtils;

/**
 * tbl_industries contains all industries
 */
public class IndustryTable {
    private static final String COLUMN_NAMES =
              "id,"
            + "name,"
            + "image";

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE tbl_industries ("
                + "id          INTEGER DEFAULT 0,"
                + "name        TEXT,"
                + "image       TEXT,"
                + "PRIMARY KEY (id)"
                + ")");
    }

    public static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS tbl_industries");
    }

    public static void updateIndustry(IndustryCollection industries) {
        if (industries == null || industries.size() == 0) {
            return;
        }

        deleteAllIndustries();

        SQLiteDatabase db = ListStingDatabase.getWritableDb();
        SQLiteStatement industryStmt = db.compileStatement(
                "INSERT OR REPLACE INTO tbl_industries ("
                        + COLUMN_NAMES
                        + ") VALUES (?1,?2,?3)");

        db.beginTransaction();
        try {
            for (Industry industry : industries) {
                industryStmt.bindLong(1, industry.getId());
                industryStmt.bindString(2, industry.getName());
                industryStmt.bindString(3, industry.getImage());
                industryStmt.execute();
            }

            db.setTransactionSuccessful();

        } finally {
            db.endTransaction();
            SqlUtils.closeStatement(industryStmt);
        }
    }

    private static void deleteAllIndustries() {
        SQLiteDatabase db = ListStingDatabase.getWritableDb();
        SQLiteStatement industryStmt = db.compileStatement("DELETE FROM tbl_industries");

        db.beginTransaction();
        try {
            industryStmt.execute();
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            SqlUtils.closeStatement(industryStmt);
        }
    }

    public static IndustryCollection getIndustries() {
        String sql = "SELECT * FROM tbl_industries";

        String[] args = {};
        Cursor cursor = ListStingDatabase.getReadableDb().rawQuery(sql, args);
        try {
            return getIndustryCollectionFromCursor(cursor);
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    public static Industry getIndustryById(long industryId) {
        String sql = "SELECT * FROM tbl_industries WHERE id=? LIMIT 1";

        String[] args = {Long.toString(industryId)};
        Cursor cursor = ListStingDatabase.getReadableDb().rawQuery(sql, args);
        try {
            return getIndustryFromCursor(cursor);
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    public static ServerInterfaces.ServerResult compareIndustries(IndustryCollection industries) {
        // removed/added industries
        IndustryCollection cachedIndustries = getIndustries();
        if(industries.size() < cachedIndustries.size()) {
            return ServerInterfaces.ServerResult.CHANGED;
        } else if(industries.size() > cachedIndustries.size()) {
            return ServerInterfaces.ServerResult.HAS_NEW;
        }

        // check for any changes
        int i = 0;
        for (Industry industry : industries) {
            Industry existingIndustry = cachedIndustries.get(i++);

            if (!industry.isSameIndustry(existingIndustry)) {
                return ServerInterfaces.ServerResult.CHANGED;
            }
        }
        return ServerInterfaces.ServerResult.UNCHANGED;
    }

    private static Industry getIndustryFromCursor(Cursor c) {
        if (c == null) {
            throw new IllegalArgumentException("getIndustryFromCursor > null cursor");
        }
        if(c.isBeforeFirst()) {
            c.moveToFirst();
        }

        Long id = c.getLong(c.getColumnIndex("id"));
        String name = c.getString(c.getColumnIndex("name"));
        String image = c.getString(c.getColumnIndex("image"));

        return new Industry(id, name, image);
    }

    private static IndustryCollection getIndustryCollectionFromCursor(Cursor cursor) {
        IndustryCollection industries = new IndustryCollection();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    industries.add(getIndustryFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (IllegalStateException e) {
            RecordedLog.e(RecordedLog.Tag.EXPLORE, e);
        }
        return industries;
    }

}