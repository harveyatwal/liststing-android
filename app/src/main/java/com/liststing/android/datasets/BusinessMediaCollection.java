package com.liststing.android.datasets;

import com.liststing.android.models.BusinessMedia;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class BusinessMediaCollection extends ArrayList<BusinessMedia> {

    public static BusinessMediaCollection fromJson(JSONObject json) {
        BusinessMediaCollection mediaCollection = new BusinessMediaCollection();
        JSONArray jsonPhotos = json.optJSONArray("photos");
        if(jsonPhotos != null) {
            for(int i = 0; i < jsonPhotos.length(); i++) {
                mediaCollection.add(BusinessMedia.fromJson(jsonPhotos.optJSONObject(i)));
            }
        }
        return mediaCollection;
    }
}
