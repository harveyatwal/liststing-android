package com.liststing.android.datasets;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.liststing.android.ListStingDatabase;
import com.liststing.android.util.RecordedLog;
import com.liststing.android.util.SqlUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * tbl_search_suggestions contains recent search suggestions
 */
public class SearchSuggestionTable {

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE tbl_search_suggestions ("
                + "id                      INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "suggestion              TEXT,"
                + "created_on              DATETIME,"
                + "UNIQUE(suggestion)      ON CONFLICT REPLACE)");
    }

    public static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS tbl_search_suggestions");
    }

    public static void insert(String query) {
        if(!query.isEmpty()) {
            ContentValues values = new ContentValues();
            values.put("suggestion", query);
            values.put("created_on", getDateTime());
            ListStingDatabase.getWritableDb().replace("tbl_search_suggestions", null, values);
        }
    }

    public static ArrayList<String> getSuggestions(String text) {
        String[] args = {text + '%'};
        Cursor c = ListStingDatabase.getReadableDb().rawQuery(
                "SELECT * FROM tbl_search_suggestions" +
                        " WHERE suggestion LIKE ?" +
                        " ORDER BY created_on DESC" +
                        " LIMIT 2", args);

        try {
            return getSuggestionsFromCursor(c);
        } finally {
            SqlUtils.closeCursor(c);
        }
    }

    private static ArrayList<String> getSuggestionsFromCursor(Cursor c) {
        ArrayList<String> suggestions = new ArrayList<String>();
        try {
            if (c != null && c.moveToFirst()) {
                do {
                    suggestions.add(getSuggestionFromCursor(c));
                } while (c.moveToNext());
            }
        } catch (IllegalStateException e) {
            RecordedLog.e(RecordedLog.Tag.SEARCH, e);
        }
        return suggestions;
    }

    private static String getSuggestionFromCursor(Cursor c) {
        if (c == null) {
            throw new IllegalArgumentException("getSuggestionFromCursor > null cursor");
        }
        if(c.isBeforeFirst()) {
            c.moveToFirst();
        }
        return c.getString(c.getColumnIndex("suggestion"));
    }


    private static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
