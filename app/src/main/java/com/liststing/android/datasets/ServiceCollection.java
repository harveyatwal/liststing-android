package com.liststing.android.datasets;

import com.liststing.android.models.Service;
import com.liststing.android.util.RecordedLog;
import com.liststing.android.util.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

public class ServiceCollection extends ArrayList<Service> {

    public static ServiceCollection fromJson(JSONObject json) {
        ServiceCollection services = new ServiceCollection();
        JSONArray jsonServices = json.optJSONArray("services");
        if (jsonServices != null) {
            for (int i = 0; i < jsonServices.length(); i++) {
                services.add(Service.fromJson(jsonServices.optJSONObject(i)));
            }
        }
        return services;
    }

    public String toXml() {
        StringBuilder builder = new StringBuilder();
        builder.append("<services>");
        for (Service service : this) {
            builder.append(service.toXml());
        }
        builder.append("</services>");
        return builder.toString();
    }

    public static ServiceCollection fromXml(String xmlString) {
        try {
            Document xml = TextUtils.stringToDom(xmlString);
            return fromXml(xml.getElementsByTagName("services"));
        } catch (SAXException e) {
            RecordedLog.e(RecordedLog.Tag.SERVICE_COLLECTION, e);
        } catch (ParserConfigurationException e) {
            RecordedLog.e(RecordedLog.Tag.SERVICE_COLLECTION, e);
        } catch (IOException e) {
            RecordedLog.e(RecordedLog.Tag.SERVICE_COLLECTION, e);
        }
        return null;
    }

    public static ServiceCollection fromXml(NodeList serviceNodeList) {
        ServiceCollection services = new ServiceCollection();
        for(int i = 0 ; i < serviceNodeList.getLength(); i ++) {
            Element serviceNode = (Element) serviceNodeList.item(i);
            services.add(Service.fromXml(serviceNode));
        }
        return services;
    }
}