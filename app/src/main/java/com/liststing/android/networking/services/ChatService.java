package com.liststing.android.networking.services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.models.ChatMessage;
import com.liststing.android.networking.extensions.ImageExtension;
import com.liststing.android.networking.extensions.ShowBusinessProfileExtension;
import com.liststing.android.networking.extensions.UserExtension;
import com.liststing.android.ui.MainActivity;
import com.liststing.android.ui.chat.ChatActivity;
import com.liststing.android.util.RecordedLog;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.nick.packet.Nick;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


public class ChatService extends Service implements PacketListener, ChatManagerListener {

    private static final long[] VIBRATE_PATTERN = new long[]{200, 400, 200, 400};
    private static final int NOTIFICATION_ID = 100;
    private static final int NOTIFICATION_MESSAGES_lENGTH = 6;

    private XMPPTCPConnection mConnection;

    private ArrayList<ChatMessage> mMessages = new ArrayList();
    private HashSet<String> mConversations = new HashSet<>();
    private HashMap<String, Chat> mChats = new HashMap<>();
    private NotificationManagerCompat mNotificationManager;

    private final IBinder mBinder = new ChatController ();
    private String mParticipantJaggerID;
    private ChatCreatedListener mChatCreatedListener;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY;
        }
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RecordedLog.i(RecordedLog.Tag.MESSENGER, "service > created");
    }

    @Override
    public void onDestroy() {
        RecordedLog.i(RecordedLog.Tag.MESSENGER, "service > destroyed");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
        if (packet instanceof Message ) {
            Message msg = ((Message) packet);
            String body = msg.getBody();
            String jabberdID = msg.getFrom();

            UserExtension userExtension = msg.getExtension(UserExtension.ELEMENT_NAME, UserExtension.NAMESPACE);
            ImageExtension imageExtension = msg.getExtension(ImageExtension.ELEMENT_NAME, ImageExtension.NAMESPACE);
            ShowBusinessProfileExtension showBusinessProfileExtension = msg.getExtension(ShowBusinessProfileExtension.ELEMENT_NAME, ShowBusinessProfileExtension.NAMESPACE);

            Nick nick = msg.getExtension(Nick.ELEMENT_NAME, Nick.NAMESPACE);
            String from = jabberdID;
            if(nick != null) {
                from = nick.getName();
            }

            if(userExtension == null || imageExtension == null || showBusinessProfileExtension == null) {
                // Todo: throw error as the incoming packet is not in correct format
                // Currently, fails silently
                return;
            }


            long userID = userExtension.getUserID();
            String imageURL = imageExtension.getUrl();
            boolean useBusinessProfile = showBusinessProfileExtension.isShowBusinessProfile();

            getImageBitmap(jabberdID, from, body, imageURL, userID, useBusinessProfile);
        }
    }

    private void getImageBitmap(final String jabberdID, final String from, final String body,
                                final String imageURL, final long userID, final boolean useBusinessProfile) {
        if (!isParticipantChatOpen(jabberdID)) {
            if(imageURL.equals("null")) {
                sendNotification(null, jabberdID, userID, from, body, useBusinessProfile);
                return;
            }

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ListSting.getImageLoader().get(ListSting.API_ENDPOINT +imageURL, new ImageLoader.ImageListener() {
                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                            Bitmap bitmap = response.getBitmap();
                            Bitmap thumbnail = ThumbnailUtils.extractThumbnail(bitmap,
                                    (int) getResources().getDimension(R.dimen.notification_large_icon_width), (int)getResources().getDimension(R.dimen.notification_large_icon_height));
                            if (bitmap != null) {
                                sendNotification(thumbnail, jabberdID, userID, from, body, useBusinessProfile);
                            }
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                }
            });
        }
    }

    private void sendNotification(Bitmap thumbnail, String jabberdID, long userID, String from, String body, boolean useBusinessProfile) {
        mMessages.add(0, new ChatMessage(body, jabberdID));
        mConversations.add(jabberdID);
        Intent intent = buildChatIntent(jabberdID, userID, useBusinessProfile);
        buildNotification(thumbnail, intent, from);
    }

    private boolean isParticipantChatOpen(String jabberdID) {
        if(extractJaggerID(jabberdID).equals(mParticipantJaggerID)) {
            return true;
        }
        return false;
    }

    private void buildNotification(Bitmap thumbnail, Intent intent, String user) {
        PendingIntent activities = PendingIntent.getActivity(ChatService.this,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        if(mConversations.size() == 1 && thumbnail != null) {
            largeIcon = thumbnail;
        }

        mNotificationManager = NotificationManagerCompat.from(ChatService.this);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ChatService.this)
                .setSmallIcon(R.drawable.ic_stat_notification)
                .setVibrate(VIBRATE_PATTERN)
                .setStyle(createInbox(user))
                .setContentTitle(buildContentTitle(user))
                .setContentText(buildMessageSummary())
                .setTicker(buildTickerText(user))
                .setLargeIcon(largeIcon)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(activities);

        mNotificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    private CharSequence buildTickerText(String user) {
        return String.format(getString(R.string.message_from), user);
    }

    private String extractJaggerID(String jaggerId) {
        return jaggerId.substring(0, jaggerId.indexOf('@'));
    }

    private Intent buildChatIntent(String jabberdID, long userID, boolean useBusinessProfile) {
        Intent intent;
        if(mConversations.size() == 1) {
            intent = new Intent(getApplicationContext(), ChatActivity.class);
            intent.putExtra(ChatActivity.ARG_PARTICIPANT_IS_BUSINESS, useBusinessProfile);
            intent.putExtra(ChatActivity.ARG_PARTICIPANT_USER_ID, userID);
            intent.putExtra(ChatActivity.ARG_PARTICIPANT_JABBER_ID, jabberdID);
            return intent;
        }
        intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }



    private NotificationCompat.InboxStyle createInbox(String from) {
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        createInboxMessageWaterfall(inboxStyle, from);
        inboxStyle.setSummaryText(buildMessageSummary());
        return inboxStyle;
    }

    private String buildMessageSummary() {
        int messageCount = mMessages.size();
        int conversationCount = mConversations.size();
        String messages = getResources().getQuantityString(R.plurals.notification_new_message_count,
                messageCount, messageCount);

        if(conversationCount > 1) {
            String conversationFormat = getString(R.string.notification_conversation_count);
            String conversations = String.format(conversationFormat, conversationCount);
            return String.format("%s %s", messages, conversations);
        }
        return messages;
    }

    private void createInboxMessageWaterfall(NotificationCompat.InboxStyle inboxStyle, String from) {
        inboxStyle.setBigContentTitle(buildContentTitle(from));

        for(int i = 0; i < mMessages.size(); i++) {
            if( i >= NOTIFICATION_MESSAGES_lENGTH) {
                inboxStyle.addLine(getString(R.string.ellipsis));
                break;
            }
            ChatMessage message = mMessages.get(i);
            if(mConversations.size() <= 1) {
                inboxStyle.addLine(message.getMessage());
            }
            if(mConversations.size() > 1) {
                inboxStyle.addLine(String.format("%s: %s", message.getAuthor(), message.getMessage()));
            }
        }
    }

    private String buildContentTitle(String from) {
        if(mConversations.size() == 1) {
            return from;
        }
        return getString(R.string.app_name);
    }

    @Override
    public void chatCreated(Chat chat, boolean createdLocally) {
        notifyChatUpdate(chat);
    }

    private void notifyChatUpdate(Chat chat) {
        if(mChats.containsKey(chat.getParticipant())) {
            Chat oldChat = mChats.get(chat.getParticipant());
            oldChat.close();
        }
        mChats.put(chat.getParticipant(), chat);
        if(mChatCreatedListener != null) {
            mChatCreatedListener.chatInitialized(chat);
        }
    }

    public class ChatController  extends Binder {

        /**
         * Creates a chat bridge between the user and the participant. This bridge
         * should be closed when a conversation is over.
         * @param jaggerID
         */
        public void openChat(String jaggerID, ChatCreatedListener chatCreatedListener) {
            if (mConnection.isAuthenticated()) {
                ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
                Chat chat = getChatByJaggerID(jaggerID);

                mChatCreatedListener = chatCreatedListener;
                stopNotifications(jaggerID);
                if(chat == null) {
                    chatManager.createChat(jaggerID);
                    return;
                }
                mChatCreatedListener.chatInitialized(chat);
            }
        }

        private Chat getChatByJaggerID(String jaggerID) {
            return mChats.get(jaggerID);
        }

        public void registerNotifications() {
            mParticipantJaggerID = "";
        }
        public void stopNotifications(String jaggerID) {
            mParticipantJaggerID = extractJaggerID(jaggerID);
        }

        public void closeChat(Chat chat) {
            if(chat != null) {
                mParticipantJaggerID = "";
                mChats.remove(chat.getParticipant());
                chat.close();
            }
        }

        public void login(String jabberId, String password) {
            XMPPChatTask chatTask = new XMPPChatTask();
            chatTask.execute(jabberId, password);
        }

        public void clearNotification() {
            if(mNotificationManager != null) {
                mNotificationManager.cancel(NOTIFICATION_ID);
                mMessages.clear();
                mConversations.clear();
            }
        }
    }

    public interface ChatCreatedListener {
        void chatInitialized(Chat chat);
    }

    class XMPPChatTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            if(Account.isSignedIn()) {
                try {
                    XMPPTCPConnectionConfiguration.Builder builder = XMPPTCPConnectionConfiguration.builder();
                    builder.setUsernameAndPassword(String.format("%s", params[0]), params[1]);
                    builder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                    builder.setHost(ListSting.ENDPOINT);
                    builder.setServiceName(ListSting.DOMAIN);
                    builder.setPort(5222);
                    builder.setCompressionEnabled(true);

                    mConnection = new XMPPTCPConnection(builder.build());
                    mConnection.connect();
                    mConnection.login();
                    mConnection.addSyncStanzaListener(ChatService.this, MessageTypeFilter.CHAT);
                    ChatManager chatmanager = ChatManager.getInstanceFor(mConnection);
                    chatmanager.addChatListener(ChatService.this);

                    addExtensionProviders();
                } catch (SmackException e) {
                    RecordedLog.e(RecordedLog.Tag.CHAT, e);
                } catch (IOException e) {
                    RecordedLog.e(RecordedLog.Tag.CHAT, e);
                } catch (XMPPException e) {
                    RecordedLog.e(RecordedLog.Tag.CHAT, e);
                }
            }
            return null;
        }
    }

    private void addExtensionProviders() {
        ProviderManager.addExtensionProvider(UserExtension.ELEMENT_NAME, UserExtension.NAMESPACE, new UserExtension.Provider());
        ProviderManager.addExtensionProvider(ImageExtension.ELEMENT_NAME, ImageExtension.NAMESPACE, new ImageExtension.Provider());
        ProviderManager.addExtensionProvider(ShowBusinessProfileExtension.ELEMENT_NAME, ShowBusinessProfileExtension.NAMESPACE, new ShowBusinessProfileExtension.Provider());
    }
}