package com.liststing.android.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.liststing.android.util.NetworkUtils;
import com.liststing.android.util.RecordedLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RestRequest extends Request<JSONObject> {
    public static final String USER_AGENT_HEADER = "User-Agent";
    public static final String REST_AUTHORIZATION_HEADER = "Authorization";
    public static final String REST_AUTHORIZATION_FORMAT = "Bearer %s";

    private static OnAuthFailedListener mOnAuthFailedListener;

    private final Response.Listener<JSONObject> mListener;
    private final Map<String, String> mHeaders;
    private final Map<String, Object> mParams;

    public interface OnAuthFailedListener {
        public void onAuthFailed();
    }

    public RestRequest(int method, String url, Map<String, Object> params,
                       Response.Listener<JSONObject> listener,
                       Response.ErrorListener errorListener) {
        super(method, NetworkUtils.appendEndPoint(url), errorListener);

        mHeaders = new HashMap<String, String>(2);
        mParams = params;
        mListener = listener;
    }

    public void setUserAgent(String userAgent) {
        mHeaders.put(USER_AGENT_HEADER, userAgent);
    }

    public void setAccessToken(String token) {
        mHeaders.put(REST_AUTHORIZATION_HEADER, String.format(REST_AUTHORIZATION_FORMAT, token));
    }

    public void setOnAuthFailedListener(OnAuthFailedListener onAuthFailedListener) {
        mOnAuthFailedListener = onAuthFailedListener;
    }

    @Override
    public Map<String, String> getHeaders() {
        return mHeaders;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        // Allow duplicate param keys
        if (mParams != null && mParams.size() > 0) {
            StringBuilder builder = new StringBuilder("");
            for (HashMap.Entry<String, Object> entry : mParams.entrySet()) {
                Object value = entry.getValue();
                if (value != null && value instanceof ArrayList) {
                    ArrayList<String> list = (ArrayList<String>) value;
                    for(String val : list) {
                        builder.append("&").append(entry.getKey());
                        builder.append("=").append(val);
                    }
                }
                if (value != null && value instanceof String) {
                    builder.append("&").append(entry.getKey());
                    builder.append("=").append(value);
                }
            }
            builder.deleteCharAt(0); // remove first '&'
            return builder.toString().getBytes();
        }
        return super.getBody();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        if (mListener != null) {
            mListener.onResponse(response);
        }
    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);

        // Server failed to authenticate
        if (error.networkResponse != null && error.networkResponse.statusCode >= 400 && mOnAuthFailedListener != null) {
            String responseString;
            try {
                responseString = new String(error.networkResponse.data, HttpHeaderParser.parseCharset(error.networkResponse.headers));
            } catch (UnsupportedEncodingException e) {
                responseString = "";
            }

            JSONObject responseObject;
            try {
                responseObject = new JSONObject(responseString);
            } catch (JSONException e) {
                responseObject = new JSONObject();
            }

            // token is invalid
            String restError = responseObject.optString("detail", "");
            if (restError.startsWith("Authentication credentials were not provided.")) {
                mOnAuthFailedListener.onAuthFailed();
                return;
            }

            // Server is offline
            if(responseString.contains("<title>") && responseString.contains("</title>")) {
                String htmlTitle = responseString.substring(responseString.indexOf("<title>"), responseString.indexOf("</title>"));
                if (htmlTitle.contains("Bad Gateway")) {
                    mOnAuthFailedListener.onAuthFailed();
                    return;
                }
            }

            RecordedLog.e(RecordedLog.Tag.VOLLEY, "RestRequest > " + responseString);
        }
    }
}
