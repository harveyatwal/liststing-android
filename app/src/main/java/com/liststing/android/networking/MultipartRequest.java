package com.liststing.android.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyLog;
import com.liststing.android.models.Account;
import com.liststing.android.util.RecordedLog;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.util.CharsetUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

public class MultipartRequest extends RestRequest {

    public static final int REST_TIMEOUT_MS = 30000;
    public static final int REST_MAX_RETRIES_POST = 0;
    public static final float REST_BACKOFF_MULT = 2f;

    private HttpEntity httpEntity;
    private MultipartEntityBuilder builder = MultipartEntityBuilder.create();

    public MultipartRequest(String url, Map<String, String> params, Map<String, File> files,
                            RestClient.Listener listener,
                            RestClient.ErrorListener errorListener) {
        super(Request.Method.POST, url, null, listener, errorListener);

        constructBody(params, files);
        RetryPolicy retryPolicy = new DefaultRetryPolicy(REST_TIMEOUT_MS, REST_MAX_RETRIES_POST, REST_BACKOFF_MULT);
        setRetryPolicy(retryPolicy);
        setAccessToken(Account.getDefaultAccount().getAccessToken());
    }

    private void constructBody(Map<String, String> params, Map<String, File> files) {
        try {
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.setCharset(CharsetUtils.get("UTF-8"));
            addParams(params);
            addFiles(files);
            httpEntity = builder.build();
        } catch (UnsupportedEncodingException e) {
            RecordedLog.e(RecordedLog.Tag.API, e);
        }
    }

    private void addParams(Map<String, String> params) throws UnsupportedEncodingException {
        if(params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                builder.addPart(key, new StringBody(value));
            }
        }
    }

    private void addFiles(Map<String, File> files) {
        if(files != null) {
            for (Map.Entry<String, File> entry : files.entrySet()) {
                String fileName = entry.getKey();
                File file = entry.getValue();
                builder.addPart(fileName, new FileBody(file));
            }
        }
    }

    @Override
    public String getBodyContentType() {
        return httpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
            RecordedLog.e(RecordedLog.Tag.VOLLEY, "Multipart > " + e.toString());
        }
        return bos.toByteArray();
    }
}