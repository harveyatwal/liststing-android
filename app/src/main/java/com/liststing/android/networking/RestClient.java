package com.liststing.android.networking;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.liststing.android.models.Account;
import com.liststing.android.util.NetworkUtils;

import org.json.JSONObject;

import java.util.Map;

public class RestClient {

    private final RequestQueue mQueue;

    private RestRequest.OnAuthFailedListener mOnAuthFailedListener;

    private static final String USER_AGENT = "ListSting Android";
    public static final int REST_TIMEOUT_MS = 30000;
    public static final int REST_MAX_RETRIES_GET = 3;
    public static final int REST_MAX_RETRIES_POST = 0;
    public static final float REST_BACKOFF_MULT = 2f;

    // Shorter version of Volley Listeners
    public interface Listener extends Response.Listener<JSONObject> {}
    public interface ErrorListener extends Response.ErrorListener {}

    public RestClient(RequestQueue queue, RestRequest.OnAuthFailedListener onAuthFailedListener) {
        mQueue = queue;
        // load an existing access token from prefs if we have one
        if (onAuthFailedListener != null) {
            setOnAuthFailedListener(onAuthFailedListener);
        }
    }

    public void setOnAuthFailedListener(RestRequest.OnAuthFailedListener onAuthFailedListener) {
        mOnAuthFailedListener = onAuthFailedListener;
    }


    public void get(String url, Listener listener, ErrorListener errorListener) {
        get(url, null, null, listener, errorListener);
    }
    public void get(String url, Map<String, Object> params, RetryPolicy retryPolicy,
                    Listener listener,
                    ErrorListener errorListener) {
        RestRequest request = makeRequest(Request.Method.GET, NetworkUtils.appendParams(url, params), params, listener, errorListener);

        if (retryPolicy == null) {
            retryPolicy = new DefaultRetryPolicy(REST_TIMEOUT_MS, REST_MAX_RETRIES_GET, REST_BACKOFF_MULT);
        }
        request.setRetryPolicy(retryPolicy);
        mQueue.add(request);
    }


    public void post(String url, Listener listener, ErrorListener errorListener) {
        post(url, null, null, listener, errorListener);
    }
    public void post(final String url, Map<String, Object> params, RetryPolicy retryPolicy,
                     Listener listener,
                     ErrorListener errorListener) {
        final RestRequest request = makeRequest(Request.Method.POST, url, params, listener, errorListener);

        //Do not retry on failure
        if (retryPolicy == null) {
            retryPolicy = new DefaultRetryPolicy(REST_TIMEOUT_MS, REST_MAX_RETRIES_POST, REST_BACKOFF_MULT);
        }
        request.setRetryPolicy(retryPolicy);
        mQueue.add(request);
    }


    private RestRequest makeRequest(int method, String url, Map<String, Object> params,
                                    Listener listener,
                                    ErrorListener errorListener) {
        RestRequest request = new RestRequest(method, url, params, listener, errorListener);
        if (mOnAuthFailedListener != null) {
            request.setOnAuthFailedListener(mOnAuthFailedListener);
        }
        String token = Account.getDefaultAccount().getAccessToken();
        request.setAccessToken(token);
        request.setUserAgent(USER_AGENT);
        return request;
    }
}