package com.liststing.android.networking.events;

import com.liststing.android.networking.ServerInterfaces;

/**
 * account-related events passed into EventBus
 */
public class AccountEvents {

    public static class FetchingAccountEnded {
        private final ServerInterfaces.ServerResult mResult;
        public FetchingAccountEnded(ServerInterfaces.ServerResult result) {
            mResult = result;
        }
        public ServerInterfaces.ServerResult getResult() {
            return mResult;
        }
    }

    public static class AccountUpdated {
        public AccountUpdated() {

        }
    }
}
