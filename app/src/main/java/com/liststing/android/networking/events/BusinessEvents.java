package com.liststing.android.networking.events;

import com.liststing.android.datasets.BusinessCollection;
import com.liststing.android.networking.ServerInterfaces;

/**
 * Business-related events passed into EventBus
 */
public class BusinessEvents {

    public static class RetrievingBusinessesStarted {
        public RetrievingBusinessesStarted() {
        }
    }

    public static class FavoriteRemoved {
        public FavoriteRemoved() {

        }
    }

    public static class RetrievingBusinessesEnded {
        private final ServerInterfaces.ServerResult mResult;

        public RetrievingBusinessesEnded(ServerInterfaces.ServerResult result) {
            mResult = result;
        }

        public ServerInterfaces.ServerResult getResult() {
            return mResult;
        }
    }

    public static class RetrievingBusinessesHasData {
        private final BusinessCollection mBusinesses;

        public RetrievingBusinessesHasData(BusinessCollection businesses) {
            mBusinesses = businesses;
        }

        public BusinessCollection getBusinesses() {
            return mBusinesses;
        }
    }
}
