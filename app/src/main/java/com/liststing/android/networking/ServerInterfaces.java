package com.liststing.android.networking;

public class ServerInterfaces {

    public enum ServerResult {
        HAS_NEW,    // new posts/comments/etc. have been retrieved
        CHANGED,    // no new posts/comments, but existing ones have changed
        UNCHANGED,  // no new or changed posts/comments
        FAILED;     // request failed
        public boolean isNewOrChanged() {
            return (this == HAS_NEW || this == CHANGED);
        }
        public boolean hasFailed() {
            return (this == FAILED);
        }
    }

    /**
     * used by adapters to notify when data has been loaded
     */
    public static interface DataLoadedListener {
        public void onDataLoaded(boolean isEmpty);
    }

    /*
     * used by adapters to notify when more data should be loaded
     */
    public interface DataRequestedListener {
        public void onRequestData();
    }


    public interface ServerResultListener {
        public void onServerResult(ServerResult result);
    }
}
