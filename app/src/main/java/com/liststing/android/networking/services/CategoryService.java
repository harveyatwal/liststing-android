package com.liststing.android.networking.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.datasets.CategoryCollection;
import com.liststing.android.networking.events.CategoryEvents;
import com.liststing.android.networking.RestClient;
import com.liststing.android.networking.ServerInterfaces;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * retrieves categories
 */
public class CategoryService extends Service {

    private static final String EXTRA_INDUSTRY_ID = "INDUSTRY_ID";
    private long mIndustryId;

    public static void startService(Context context, long id) {
        Intent intent = new Intent(context, CategoryService.class);
        intent.putExtra(EXTRA_INDUSTRY_ID, id);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RecordedLog.i(RecordedLog.Tag.CATEGORY, "service > created");
    }

    @Override
    public void onDestroy() {
        RecordedLog.i(RecordedLog.Tag.CATEGORY, "service > destroyed");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }
        mIndustryId = intent.getLongExtra(EXTRA_INDUSTRY_ID, 0);
        retrieveCategories();

        return START_NOT_STICKY;
    }

    private void retrieveCategories() {
        ServerInterfaces.ServerResultListener listener = new ServerInterfaces.ServerResultListener() {
            @Override
            public void onServerResult(ServerInterfaces.ServerResult result) {
                EventBus.getDefault().post(new CategoryEvents.RetrievingCategoriesEnded(result));
                stopSelf();
            }
        };

        requestCategories(listener);
    }

    private void requestCategories(final ServerInterfaces.ServerResultListener resultListener) {
        EventBus.getDefault().post(new CategoryEvents.RetrievingCategoriesStarted());
        RecordedLog.d(RecordedLog.Tag.CATEGORY, "Retreving categories");
        String endpoint = "industry/" + mIndustryId +"/categories/";

        RestClient.Listener listener = new RestClient.Listener() {
            @Override
            public void onResponse(JSONObject response) {
                hanndleResponse(response, resultListener);
            }
        };

        RestClient.ErrorListener errorListener = new RestClient.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RecordedLog.e(RecordedLog.Tag.CATEGORY, "Volley error \n + " + error);
                resultListener.onServerResult(ServerInterfaces.ServerResult.FAILED);
            }
        };
        ListSting.getRestClient().get(endpoint, null, null, listener, errorListener);
    }

    private void hanndleResponse(final JSONObject response, final ServerInterfaces.ServerResultListener resultListener) {
        if (response == null) {
            resultListener.onServerResult(ServerInterfaces.ServerResult.FAILED);
            return;
        }

        resultListener.onServerResult(ServerInterfaces.ServerResult.HAS_NEW);
        EventBus.getDefault().post(new CategoryEvents.RetrievingCategoriesHasData(CategoryCollection.fromJson(response)));
    }
}
