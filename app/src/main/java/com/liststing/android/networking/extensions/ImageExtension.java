package com.liststing.android.networking.extensions;


import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class ImageExtension implements ExtensionElement {

    public static final String NAMESPACE = "http://liststing.com/protocol/image";
    public static final String ELEMENT_NAME = "image";

    private String url;

    public ImageExtension(String url) {
        this.url = url;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public String getElementName() {
        return ELEMENT_NAME;
    }

    @Override
    public CharSequence toXML() {
        final StringBuilder buf = new StringBuilder();

        buf.append("<").append(ELEMENT_NAME).append(" xmlns=\"").append(
                NAMESPACE).append("\">");
        buf.append(getUrl());
        buf.append("</").append(ELEMENT_NAME).append('>');

        return buf.toString();
    }

    public String getUrl() {
        return url;
    }

    public static class Provider extends ExtensionElementProvider<ImageExtension> {

        @Override
        public ImageExtension parse(XmlPullParser parser, int initialDepth)
                throws XmlPullParserException, IOException {

            parser.next();
            final String url = parser.getText();

            // Advance to end of extension.
            while(parser.getEventType() != XmlPullParser.END_TAG) {
                parser.next();
            }

            return new ImageExtension(url);
        }
    }
}