package com.liststing.android.networking.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.datasets.IndustryCollection;
import com.liststing.android.datasets.IndustryTable;
import com.liststing.android.networking.events.IndustryEvents;
import com.liststing.android.networking.RestClient;
import com.liststing.android.networking.ServerInterfaces;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * retrieves industries
 */
public class IndustryService extends Service {

    public static void startService(Context context) {
        Intent intent = new Intent(context, IndustryService.class);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RecordedLog.i(RecordedLog.Tag.EXPLORE, "service > created");
    }

    @Override
    public void onDestroy() {
        RecordedLog.i(RecordedLog.Tag.EXPLORE, "service > destroyed");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }

        retrieveIndustries();

        return START_NOT_STICKY;
    }

    private void retrieveIndustries() {
        ServerInterfaces.ServerResultListener listener = new ServerInterfaces.ServerResultListener() {

            @Override
            public void onServerResult(ServerInterfaces.ServerResult result) {
                EventBus.getDefault().post(new IndustryEvents.RetrievingIndustriesEnded(result));
                stopSelf();
            }
        };

        EventBus.getDefault().post(new IndustryEvents.RetrievingIndustriesStarted());
        requestIndustries(listener);
    }

    private static void requestIndustries(final ServerInterfaces.ServerResultListener resultListener) {
        String endpoint = "industries/";
        RecordedLog.d(RecordedLog.Tag.EXPLORE, "Retreving industries");

        RestClient.Listener listener = new RestClient.Listener() {
            @Override
            public void onResponse(JSONObject response) {
                hanndleResponse(response, resultListener);
            }
        };

        RestClient.ErrorListener errorListener = new RestClient.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RecordedLog.e(RecordedLog.Tag.EXPLORE, "Volley error \n + " + error);
                resultListener.onServerResult(ServerInterfaces.ServerResult.FAILED);
            }
        };
        ListSting.getRestClient().get(endpoint, null, null, listener, errorListener);
    }

    private static void hanndleResponse(final JSONObject response, final ServerInterfaces.ServerResultListener resultListener) {
        if (response == null) {
            resultListener.onServerResult(ServerInterfaces.ServerResult.FAILED);
            return;
        }

        // spin off a new thread to check new industries with the cached industries
        // if there is new or changed industries we update the cache industries
        // otherwise we continue with the cached industries
        new Thread() {
            @Override
            public void run() {
                IndustryCollection industries = IndustryCollection.fromJson(response);
                ServerInterfaces.ServerResult updateResult = IndustryTable.compareIndustries(industries);
                if (updateResult.isNewOrChanged()) {
                    IndustryTable.updateIndustry(industries);
                }
                resultListener.onServerResult(updateResult);
            }
        }.start();
    }
}
