package com.liststing.android.networking;

import org.json.JSONException;
import org.json.JSONObject;

public class Token {

    private static final String ACCESS_TOKEN_FIELD_NAME = "access_token";
    private static final String TOKEN_TYPE_FIELD_NAME = "token_type";

    private String mTokenType;
    private String mAccessToken;

    public Token(String accessToken, String tokenType) {
        mAccessToken = accessToken;
        mTokenType = tokenType;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public String toString() {
        return getAccessToken();
    }

    public static Token fromJSONObject(JSONObject tokenJSON) throws JSONException {
        return new Token(tokenJSON.getString(ACCESS_TOKEN_FIELD_NAME), tokenJSON.getString(TOKEN_TYPE_FIELD_NAME));
    }
}
