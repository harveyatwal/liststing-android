package com.liststing.android.networking;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

/**
 * Used as an L1 cache that stores the url and bitmap image.
 */
public class BitmapCache extends LruCache<String, Bitmap> implements ImageLoader.ImageCache {

    public BitmapCache(int maxSize) {
        super(maxSize);
    }

    @Override
    public Bitmap getBitmap(String url) {
        return this.get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        this.put(url, bitmap);
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        int bytes = (value.getRowBytes() * value.getHeight());
        return (bytes / 1024); //value.getByteCount() introduced in HONEYCOMB_MR1 or higher.
    }
}
