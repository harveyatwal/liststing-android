package com.liststing.android.networking.extensions;


import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class UserExtension implements ExtensionElement {

    public static final String NAMESPACE = "http://liststing.com/protocol/user";
    public static final String ELEMENT_NAME = "user";

    private long userID;

    public UserExtension(long userID) {
        this.userID = userID;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public String getElementName() {
        return ELEMENT_NAME;
    }

    @Override
    public CharSequence toXML() {
        final StringBuilder buf = new StringBuilder();

        buf.append("<").append(ELEMENT_NAME).append(" xmlns=\"").append(
                NAMESPACE).append("\">");
        buf.append(getUserID());
        buf.append("</").append(ELEMENT_NAME).append('>');

        return buf.toString();
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public static class Provider extends ExtensionElementProvider<UserExtension> {

        @Override
        public UserExtension parse(XmlPullParser parser, int initialDepth)
                throws XmlPullParserException, IOException {

            parser.next();
            final String userID = parser.getText();

            // Advance to end of extension.
            while(parser.getEventType() != XmlPullParser.END_TAG) {
                parser.next();
            }

            return new UserExtension(Long.parseLong(userID));
        }
    }
}