package com.liststing.android.networking;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.liststing.android.ListSting;
import com.liststing.android.util.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class SignUpRequest extends Request<JSONObject> {
    /**
     * Socket timeout in milliseconds for rest requests
     */
    public static final int REST_TIMEOUT_MS = 30000;
    /**
     * Default number of retries for POST rest requests
     */
    public static final int REST_MAX_RETRIES_POST = 0;
    /**
     * Default backoff multiplier for rest requests
     */
    public static final float REST_BACKOFF_MULT = 2f;

    public static final String TOKEN_ENDPOINT = "/users/new/";

    public static final String CLIENT_ID_PARAM_NAME = "client_id";
    public static final String CLIENT_SECRET_PARAM_NAME = "client_secret";
    private static final String EMAIL_PARAM_NAME = "email";
    private static final String PASSWORD_PARAM_NAME = "password";
    private static final String FIRST_NAME_PARAM_NAME = "first_name";
    private static final String LAST_NAME_PARAM_NAME = "last_name";

    private final Response.Listener mListener;
    private Map<String, String> mParams = new HashMap<String, String>();

    public SignUpRequest(String email, String password, String firstName, String lastName,
                         Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.POST, NetworkUtils.appendEndPoint(TOKEN_ENDPOINT), errorListener);
        this.mListener = listener;
        mParams.put(CLIENT_ID_PARAM_NAME, ListSting.OAUTH_APP_ID);
        mParams.put(CLIENT_SECRET_PARAM_NAME, ListSting.OAUTH_APP_SECRET);
        mParams.put(EMAIL_PARAM_NAME, email);
        mParams.put(PASSWORD_PARAM_NAME, password);
        mParams.put(FIRST_NAME_PARAM_NAME, firstName);
        mParams.put(LAST_NAME_PARAM_NAME, lastName);

        // Do not retry on failure
        RetryPolicy retryPolicy = new DefaultRetryPolicy(REST_TIMEOUT_MS, REST_MAX_RETRIES_POST, REST_BACKOFF_MULT);
        setRetryPolicy(retryPolicy);
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            JSONObject tokenData = new JSONObject(jsonString);
            return Response.success(tokenData, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        mListener.onResponse(response);
    }

    @Override
    public Map<String, String> getParams() {
        return mParams;
    }
}
