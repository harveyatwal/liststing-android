package com.liststing.android.networking.events;

import com.liststing.android.datasets.CategoryCollection;
import com.liststing.android.networking.ServerInterfaces;

/**
 * Category-related events passed into EventBus
 */
public class CategoryEvents {

    public static class RetrievingCategoriesStarted {
        public RetrievingCategoriesStarted() {

        }
    }

    public static class RetrievingCategoriesEnded {
        private final ServerInterfaces.ServerResult mResult;

        public RetrievingCategoriesEnded(ServerInterfaces.ServerResult result) {
            mResult = result;
        }

        public ServerInterfaces.ServerResult getResult() {
            return mResult;
        }
    }

    public static class RetrievingCategoriesHasData {
        private final CategoryCollection mCategories;

        public RetrievingCategoriesHasData(CategoryCollection categories) {
            mCategories = categories;
        }

        public CategoryCollection getCategories() {
            return mCategories;
        }
    }
}
