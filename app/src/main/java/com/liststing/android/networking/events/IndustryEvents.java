package com.liststing.android.networking.events;

import com.liststing.android.networking.ServerInterfaces;

/**
 * Industry-related events passed into EventBus
 */
public class IndustryEvents {

    public static class RetrievingIndustriesStarted {
        public RetrievingIndustriesStarted() {

        }
    }

    public static class RetrievingIndustriesEnded {
        private final ServerInterfaces.ServerResult mResult;
        public RetrievingIndustriesEnded(ServerInterfaces.ServerResult result) {
            mResult = result;
        }
        public ServerInterfaces.ServerResult getResult() {
            return mResult;
        }
    }
}
