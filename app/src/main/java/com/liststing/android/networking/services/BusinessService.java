package com.liststing.android.networking.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.datasets.BusinessCollection;
import com.liststing.android.networking.events.BusinessEvents;
import com.liststing.android.networking.RestClient;
import com.liststing.android.networking.ServerInterfaces;
import com.liststing.android.util.LocationUtils;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;

import java.util.HashMap;

import de.greenrobot.event.EventBus;

/**
 * retrieves businesses
 */
public class BusinessService extends Service {

    private static final String EXTRA_INDUSTRY_ID = "INDUSTRY_ID";
    private static final String EXTRA_QUERY = "QUERY";
    private long mIndustryId;
    private String mQuery;

    public static void startService(Context context, long id, String query) {
        Intent intent = new Intent(context, BusinessService.class);
        intent.putExtra(EXTRA_INDUSTRY_ID, id);
        intent.putExtra(EXTRA_QUERY, query);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RecordedLog.i(RecordedLog.Tag.BUSINESS, "service > created");
    }

    @Override
    public void onDestroy() {
        RecordedLog.i(RecordedLog.Tag.BUSINESS, "service > destroyed");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }
        mIndustryId = intent.getLongExtra(EXTRA_INDUSTRY_ID, 0);
        mQuery = intent.getStringExtra(EXTRA_QUERY);

        EventBus.getDefault().post(new BusinessEvents.RetrievingBusinessesStarted());
        getLocation();

        return START_NOT_STICKY;
    }

    private void retrieveBusinesses(Location location) {
        ServerInterfaces.ServerResultListener listener = new ServerInterfaces.ServerResultListener() {

            @Override
            public void onServerResult(ServerInterfaces.ServerResult result) {
                EventBus.getDefault().post(new BusinessEvents.RetrievingBusinessesEnded(result));
                stopSelf();
            }
        };
        requestBusinesses(location, listener);
    }

    private void getLocation() {
        LocationUtils locationUtils = new LocationUtils(this);
        Location location = locationUtils.getLastLocation();
        retrieveBusinesses(location);
    }

    private void requestBusinesses(Location location, final ServerInterfaces.ServerResultListener resultListener) {
        String endpoint = "businesses/industry/" + mIndustryId +"/";
        RecordedLog.d(RecordedLog.Tag.BUSINESS, "Retreving businesses");

        RestClient.Listener listener = new RestClient.Listener() {
            @Override
            public void onResponse(JSONObject response) {
                hanndleResponse(response, resultListener);
            }
        };

        RestClient.ErrorListener errorListener = new RestClient.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RecordedLog.e(RecordedLog.Tag.BUSINESS, "Volley error \n + " + error);
                resultListener.onServerResult(ServerInterfaces.ServerResult.FAILED);
            }
        };
        ListSting.getRestClient().get(endpoint, buildParams(location), null, listener, errorListener);
    }

    private HashMap<String, Object> buildParams(Location location) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("longitude", String.valueOf(location.getLongitude()));
        params.put("latitude", String.valueOf(location.getLatitude()));
        params.put("q", mQuery);
        return params;
    }

    private static void hanndleResponse(final JSONObject response, final ServerInterfaces.ServerResultListener resultListener) {
        if (response == null) {
            resultListener.onServerResult(ServerInterfaces.ServerResult.FAILED);
            return;
        }

        resultListener.onServerResult(ServerInterfaces.ServerResult.HAS_NEW);
        EventBus.getDefault().post(new BusinessEvents.RetrievingBusinessesHasData(BusinessCollection.fromJson(response.optJSONArray("businesses"))));
    }
}
