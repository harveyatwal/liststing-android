package com.liststing.android.networking;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.liststing.android.ListSting;
import com.liststing.android.util.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class SignInRequest extends Request<Token> {
    public static final String TOKEN_ENDPOINT = "/oauth2/token/";

    public static final String CLIENT_ID_PARAM_NAME = "client_id";
    public static final String REDIRECT_URI_PARAM_NAME = "redirect_uri";
    public static final String CLIENT_SECRET_PARAM_NAME = "client_secret";
    private static final String GRANT_TYPE_PARAM_NAME = "grant_type";
    private static final String EMAIL_PARAM_NAME = "username";
    private static final String PASSWORD_PARAM_NAME = "password";

    private static final String PASSWORD_GRANT_TYPE = "password";

    private final Response.Listener mListener;
    private Map<String, String> mParams = new HashMap<String, String>();

    public SignInRequest(String email, String password, Response.Listener<Token> listener, Response.ErrorListener errorListener) {
        super(Method.POST, NetworkUtils.appendEndPoint(TOKEN_ENDPOINT), errorListener);
        this.mListener = listener;
        mParams.put(CLIENT_ID_PARAM_NAME, ListSting.OAUTH_APP_ID);
        mParams.put(CLIENT_SECRET_PARAM_NAME, ListSting.OAUTH_APP_SECRET);
        mParams.put(REDIRECT_URI_PARAM_NAME, ListSting.OAUTH_REDIRECT_URI);
        mParams.put(EMAIL_PARAM_NAME, email);
        mParams.put(PASSWORD_PARAM_NAME, password);
        mParams.put(GRANT_TYPE_PARAM_NAME, PASSWORD_GRANT_TYPE);
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            JSONObject tokenData = new JSONObject(jsonString);
            return Response.success(Token.fromJSONObject(tokenData), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(Token response) {
        mListener.onResponse(response);
    }

    @Override
    public Map<String, String> getParams() {
        return mParams;
    }
}
