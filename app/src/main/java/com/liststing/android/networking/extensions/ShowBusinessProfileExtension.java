package com.liststing.android.networking.extensions;


import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class ShowBusinessProfileExtension implements ExtensionElement {

    public static final String NAMESPACE = "http://liststing.com/protocol/is_participant_business";
    public static final String ELEMENT_NAME = "show_business_profile";

    private boolean showBusinessProfile;

    public ShowBusinessProfileExtension(boolean showBusinessProfile) {
        this.showBusinessProfile = !showBusinessProfile;
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public String getElementName() {
        return ELEMENT_NAME;
    }

    @Override
    public CharSequence toXML() {

        final StringBuilder buf = new StringBuilder();

        buf.append("<").append(ELEMENT_NAME).append(" xmlns=\"").append(
                NAMESPACE).append("\">");
        buf.append(isShowBusinessProfile());
        buf.append("</").append(ELEMENT_NAME).append('>');

        return buf.toString();
    }

    public boolean isShowBusinessProfile() {
        return showBusinessProfile;
    }

    public static class Provider extends ExtensionElementProvider<ShowBusinessProfileExtension> {
        @Override
        public ShowBusinessProfileExtension parse(XmlPullParser parser, int initialDepth)
                throws XmlPullParserException, IOException {

            parser.next();
            final String is_business = parser.getText();

            // Advance to end of extension.
            while(parser.getEventType() != XmlPullParser.END_TAG) {
                parser.next();
            }

            return new ShowBusinessProfileExtension(Boolean.valueOf(is_business));
        }
    }
}