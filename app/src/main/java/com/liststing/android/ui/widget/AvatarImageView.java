package com.liststing.android.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.util.AttributeSet;

import com.liststing.android.R;
import com.liststing.android.util.BitmapUtils;

/**
 * RoundedImageView is an extension of NetworkImageView, which crops the bitmap
 * into a circular image.
 *
 * TODO: Might be a better idea to create a proxy for both NetworkImageView and RoundedImageView
 *
 * http://stackoverflow.com/a/16208548
 */
public class AvatarImageView extends BitmapNetworkImageView {

    public AvatarImageView(Context context) {
        super(context);
    }

    public AvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AvatarImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }

        int width = getWidth();
        int height = getHeight();
        if (width == 0 || height == 0) {
            return;
        }

        Bitmap bitmap =  ((BitmapDrawable)drawable).getBitmap();
        if(bitmap != null) {
            Bitmap bitmapCopy = bitmap.copy(Bitmap.Config.ARGB_8888, true);
            Bitmap roundBitmap = getCroppedBitmap(bitmapCopy, width);
            canvas.drawBitmap(roundBitmap, 0, 0, null);
        }
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap, int width) {
        Bitmap scaledBitmap;

        // scale bitmap if it does not have equal dimensions
        if (bitmap.getWidth() != width || bitmap.getHeight() != width) {
            scaledBitmap = ThumbnailUtils.extractThumbnail(bitmap, width, width);
        } else {
            scaledBitmap = bitmap;
        }

        // rounding the image
        Bitmap output = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Rect rect = new Rect(0, 0, width, width);
        final Paint paint = new Paint();
        final Paint whitePaint = new Paint();
        final int radius = width / 2;
        final int borderWidth = 1;/*(int) BitmapUtils.convertDpToPixel(2f);*/
        final int dropShadow = 1;

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);

        // remove unneeded bitmap corners
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(radius, radius - dropShadow, radius - borderWidth - dropShadow, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(scaledBitmap, rect, rect, paint);

        whitePaint.setColor(Color.WHITE);
        whitePaint.setAntiAlias(true);
        whitePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
        canvas.drawCircle(radius, radius - dropShadow, radius - dropShadow, whitePaint);

        Paint paintShadow = new Paint();
        paintShadow.setColor(getResources().getColor(R.color.divider_black));
        paintShadow.setAntiAlias(true);

        paintShadow.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
        canvas.drawCircle(radius, radius, radius, paintShadow);
        return output;
    }
}
