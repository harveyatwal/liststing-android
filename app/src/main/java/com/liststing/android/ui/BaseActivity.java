package com.liststing.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.ui.business.BusinessActivity;
import com.liststing.android.ui.business.BusinessListActivity;
import com.liststing.android.ui.business.ListBusinessActivity;
import com.liststing.android.ui.profile.ProfileActivity;
import com.liststing.android.ui.widget.Overlay;
import com.liststing.android.ui.widget.SearchBar;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SearchBar.OnSearchListener {

    private static final int LIST_BUSINESS = 100;

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private SearchBar mSearchBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setUpToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);

        mSearchBar = (SearchBar) findViewById(R.id.searchbar);
        mSearchBar.setOnSearchListener(this);
        mSearchBar.addOverlay((Overlay) findViewById(R.id.overlay));

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(ListSting.getTypeface());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    protected void setUpNavBar() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mNavigationView = (NavigationView) findViewById(R.id.navigation);
        mNavigationView.setNavigationItemSelectedListener(this);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        switch (menuItem.getItemId()) {
            case R.id.action_profile:
                switchToProfileActivity();
                return true;
            case R.id.action_business:
                switchToBusinessActivity();
                return true;
            case R.id.action_list_business:
                switchToListBusinessActivity();
                return true;

            case R.id.action_sign_in:
                switchToSignInActivity();
                return true;

            case R.id.action_why_list:
            case R.id.action_settings:
            case R.id.action_help:
            case R.id.action_about:
                CharSequence text = "This feature has not been implemented for the BETA.";
                Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
                toast.show();
                return true;

            case R.id.action_sign_out:
                ListSting.logout();
                return true;

            default:
                return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LIST_BUSINESS) {
            if (resultCode == RESULT_OK) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }
    }

    private void switchToSignInActivity() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }

    private void switchToProfileActivity() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    private void switchToBusinessActivity() {
        Intent intent = new Intent(this, BusinessActivity.class);
        startActivity(intent);
    }

    private void switchToListBusinessActivity() {
        Intent intent = new Intent(this, ListBusinessActivity.class);
        startActivityForResult(intent, LIST_BUSINESS);
    }

    @Override
    public void onSearch(String query) {
        Intent intent = new Intent(this, BusinessListActivity.class);
        intent.putExtra(BusinessListActivity.ARG_QUERY, query);
        this.startActivity(intent);
    }
}
