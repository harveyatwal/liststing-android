package com.liststing.android.ui.business;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liststing.android.R;
import com.liststing.android.models.Business;

import java.util.ArrayList;

public class BusinessListFragment extends Fragment {

    public static final String ARG_BUSINESSES = "ARG_BUSINESSES";
    private static final String ARG_SHOWING_FAVORITES = "ARG_FAVORITES";
    private RecyclerView mRecyclerView;
    private BusinessListAdapter mBusinessAdapter;
    private ArrayList<Business> mBusinesses;
    private boolean mIsShowingFavorites;
    private View mNoListingsView;
    private View mNoFavoritesView;

    public static BusinessListFragment newInstance(ArrayList<Business> mBusinesses, boolean isShowingFavorites) {
        BusinessListFragment businessListFragment = new BusinessListFragment();

        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_BUSINESSES, mBusinesses);
        args.putBoolean(ARG_SHOWING_FAVORITES, isShowingFavorites);
        businessListFragment.setArguments(args);

        return businessListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_business_list, container, false);

        mBusinesses = getArguments().getParcelableArrayList(ARG_BUSINESSES);
        mIsShowingFavorites = getArguments().getBoolean(ARG_SHOWING_FAVORITES);

        mNoListingsView = rootView.findViewById(R.id.no_listings);
        mNoFavoritesView = rootView.findViewById(R.id.no_favorites);

        if(mBusinesses.size() == 0) {
            showEmptyView();
        }
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.business_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return rootView;
    }

    private void showEmptyView() {
        if(mIsShowingFavorites) {
            mNoFavoritesView.setVisibility(View.VISIBLE);
            return;
        }
        mNoListingsView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mBusinessAdapter = new BusinessListAdapter(getActivity(), mBusinesses);
        mRecyclerView.setAdapter(mBusinessAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mBusinessAdapter != null) {
            mBusinessAdapter.notifyDataSetChanged();
        }
    }

}
