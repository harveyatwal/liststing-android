package com.liststing.android.ui.explore;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.liststing.android.R;
import com.liststing.android.datasets.IndustryTable;
import com.liststing.android.networking.events.IndustryEvents;
import com.liststing.android.networking.services.IndustryService;
import com.liststing.android.util.NetworkUtils;

import de.greenrobot.event.EventBus;

public class ExploreFragment extends Fragment {

    private ListView mListView;
    private ExploreAdapter mExploreAdapter;
    private View mNoNetworkView;
    private View mNoServerView;
    private View mProgressBar;

    public static ExploreFragment newInstance() {
        return new ExploreFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_explore, container, false);

        mListView = (ListView) rootView.findViewById(R.id.business_recycler_view);

        // TODO: Create a retry button for these two views
        mNoNetworkView = rootView.findViewById(R.id.no_network_view);
        mNoServerView = rootView.findViewById(R.id.no_server_view);
        mProgressBar = rootView.findViewById(R.id.business_progress_bar);

        addFavoritesHeader();
        return rootView;
    }

    private void addFavoritesHeader() {
//        if(Account.isSignedIn()) {
//            mListView.addHeaderView(getActivity().getLayoutInflater().inflate(R.layout.view_favorites, null));
//        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mListView.setAdapter(getExploreAdapter());
        getExploreAdapter().reload();
        // prevents calls to the api on orientation change
        if(savedInstanceState == null) {
            loadIndustries();
        }
    }

    private ExploreAdapter getExploreAdapter() {
        if (mExploreAdapter == null) {
            mExploreAdapter = new ExploreAdapter(getActivity(), R.layout.item_explore_industries, mListView);
        }
        return mExploreAdapter;
    }

    private boolean hasExploreAdapter() {
        return (mExploreAdapter != null);
    }

    private void loadIndustries() {
        if (!NetworkUtils.isNetworkAvailable(getActivity())) {
            mNoNetworkView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
            return;
        }
        IndustryService.startService(getActivity());
    }

    private void refreshIndustries() {
        if (hasExploreAdapter()) {
            getExploreAdapter().refresh();
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(IndustryEvents.RetrievingIndustriesStarted event) {
        // only show progress bar if there is no industries in cache
        if(IndustryTable.getIndustries().size() == 0) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }


    @SuppressWarnings("unused")
    public void onEventMainThread(IndustryEvents.RetrievingIndustriesEnded event) {
        mProgressBar.setVisibility(View.GONE);

        if(event.getResult().isNewOrChanged()) {
            refreshIndustries();
        }

        if(event.getResult().hasFailed()) {
            mNoServerView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}
