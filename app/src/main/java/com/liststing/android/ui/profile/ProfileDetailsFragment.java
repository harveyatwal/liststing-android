package com.liststing.android.ui.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.liststing.android.R;
import com.liststing.android.models.Account;

public class ProfileDetailsFragment extends Fragment {

    public static ProfileDetailsFragment newInstance() {
        return new ProfileDetailsFragment();
    }

    private TextView mEditProfileLink;
    private Account mProfile;

    private TextView mFirstName;
    private TextView mLastName;
    private TextView mEmailAddress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_profile_details, container, false);

        mEditProfileLink = (TextView) rootView.findViewById(R.id.profile_edit_link);
        mFirstName = (TextView) rootView.findViewById(R.id.profile_first_name);
        mLastName = (TextView) rootView.findViewById(R.id.profile_last_name);
        mEmailAddress = (TextView) rootView.findViewById(R.id.profile_email_address);

        mEditProfileLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToEditDetailsFragment();
            }
        });

        mProfile = Account.getDefaultAccount();
        setActionBar();
        populateContactInformation();
        return rootView;
    }

    private void setActionBar() {
        if(!(getActivity() instanceof ProfileActivity)) {
            return;
        }

        ActionBar actionBar = ((ProfileActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(getString(R.string.profile));
        actionBar.setHomeAsUpIndicator(0);
    }

    private void moveToEditDetailsFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        ProfileEditDetailsFragment profileEditDetailsFragment = ProfileEditDetailsFragment.newInstance();
        transaction.addToBackStack("");
        transaction.replace(R.id.details_fragment_container, profileEditDetailsFragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    private void populateContactInformation() {
        mFirstName.setText(mProfile.getFirstName());
        mLastName.setText(mProfile.getLastName());
        mEmailAddress.setText(mProfile.getEmail());
    }
}
