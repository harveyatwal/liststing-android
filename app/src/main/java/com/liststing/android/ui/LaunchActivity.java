package com.liststing.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.liststing.android.models.Account;
import com.liststing.android.networking.events.AccountEvents;
import com.liststing.android.ui.explore.ExploreActivity;

import de.greenrobot.event.EventBus;

/**
 * Launches either the SignInActivity or MainaAtivity
 * Specified to have no UI
 */
public class LaunchActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Account.isSignedIn()) {
            Account.getDefaultAccount().fetchAccountDetails();
            return;
        }

        goToExploreActivity();
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(AccountEvents.FetchingAccountEnded event) {
        if(event.getResult().isNewOrChanged()) {
            goToMainActivity();
        }

        if(event.getResult().hasFailed()) {
            goToExploreActivity();
        }
    }

    private void goToExploreActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LaunchActivity.this, ExploreActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void goToMainActivity() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LaunchActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}