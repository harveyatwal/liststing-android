package com.liststing.android.ui.business;

import android.content.Intent;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.android.airmapview.AirMapView;
import com.airbnb.android.airmapview.DefaultAirMapViewBuilder;
import com.airbnb.android.airmapview.listeners.OnMapInitializedListener;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.google.i18n.addressinput.common.AddressData;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.models.Business;
import com.liststing.android.networking.MultipartRequest;
import com.liststing.android.networking.RestClient;
import com.liststing.android.ui.SignInActivity;
import com.liststing.android.ui.chat.ChatActivity;
import com.liststing.android.ui.dialog.PhotoDialog;
import com.liststing.android.ui.widget.AvatarImageView;
import com.liststing.android.ui.widget.DividerItemDecoration;
import com.liststing.android.util.BitmapUtils;
import com.liststing.android.util.FileUtils;
import com.liststing.android.util.LocationUtils;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class BusinessActivity extends AppCompatActivity implements OnMapInitializedListener {

    public static final String ARG_BUSINESS = "arg_business";
    private static final String AVATAR_PARAM_NAME = "avatar";
    private static final String PHOTO_PARAM_NAME = "photo";
    private static final String PHOTO_UPLOAD_BUSINESS_ENDPOINT = "/business/current/add_photo/";
    private static final String UPDATE_BUSINESS_ENDPOINT = "/business/current/update/";

    private static final int SELECT_PHOTO = 100;
    private static final int EDIT_BUSINESS = 200;

    private Business mBusiness;
    private AvatarImageView mAvatar;
    private TextView mBusinessName;
    private TextView mAddressLocation;
    private File mAvatarFile;
    private AirMapView mMapView;
    private DefaultAirMapViewBuilder mMapViewBuilder;
    private View mMapContainer;
    private Button mMessageButton;
    private Button mEditBusinessButton;
    private MenuItem mFavoriteMenuItemOutline;
    private MenuItem mFavoriteMenuItem;
    private View mContainer;
    private Button mOpenGoogleMaps;
    private ViewPager mImagePreviewPager;
    private View mAvatarContainer;
    private BusinessMediaSlideAdapter mPreviewAdapter;
    private RecyclerView mCarouselRecycler;
    private BusinessMediaCarouselAdapter mCarouselAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            mBusiness = extras.getParcelable(ARG_BUSINESS);
        } else {
            mBusiness = Account.getDefaultAccount().getBusiness();
        }
        mImagePreviewPager = (ViewPager) findViewById(R.id.preview_pager);
        mPreviewAdapter = new BusinessMediaSlideAdapter(getSupportFragmentManager(), mBusiness.getPhotos());
        mImagePreviewPager.setAdapter(mPreviewAdapter);

        mCarouselRecycler = (RecyclerView) findViewById(R.id.carousel_list);
        mCarouselAdapter = new BusinessMediaCarouselAdapter(mBusiness.getPhotos());
        mCarouselRecycler.setAdapter(mCarouselAdapter);
        mCarouselRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL_LIST));
        mCarouselRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        mAvatarContainer = findViewById(R.id.avatar_container);
        mAvatar = (AvatarImageView) findViewById(R.id.business_avatar);
        mAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectAvatarPhoto();
            }
        });

        mContainer = findViewById(R.id.container);
        mMapContainer = findViewById(R.id.map_container);
        mMapView = (AirMapView) findViewById(R.id.map_view);
        mOpenGoogleMaps = (Button) findViewById(R.id.open_google_maps);
        mAddressLocation = (TextView) findViewById(R.id.address_location);

        mBusinessName = (TextView) findViewById(R.id.business_name);

        mMessageButton = (Button) findViewById(R.id.message);
        mMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToChatActivity();
            }
        });
        mEditBusinessButton = (Button) findViewById(R.id.edit_business);
        mEditBusinessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BusinessActivity.this, ListBusinessActivity.class);
                intent.putExtra(ListBusinessActivity.ARG_BUSINESS, mBusiness);
                startActivityForResult(intent, EDIT_BUSINESS);
            }
        });

        setupActionBar();
        if(savedInstanceState == null) {
            addFragments();
        }
        removeButtons();
        populateBusinessDetails();
        addLocation();
        showImagePreviewPager();
    }


    private void showImagePreviewPager() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)mAvatarContainer.getLayoutParams();
        if(mBusiness.getPhotos().size() != 0) {
            int marginTop = getResources().getDimensionPixelSize(R.dimen.profile_avatar_location);
            params.setMargins(0, marginTop, 0, 0);
            mAvatarContainer.setLayoutParams(params);
            showPhotoLayout(View.VISIBLE);
        } else {
            params.setMargins(0, 0, 0, 0);
            mAvatarContainer.setLayoutParams(params);
            showPhotoLayout(View.GONE);
        }
    }

    private void showPhotoLayout(int visibility) {
        View photoContainer = findViewById(R.id.photo_container);
        View divider = findViewById(R.id.pager_divider);

        photoContainer.setVisibility(visibility);
        divider.setVisibility(visibility);
        mImagePreviewPager.setVisibility(visibility);

        photoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BusinessActivity.this, BusinessGalleryActivity.class);
                intent.putExtra(BusinessGalleryActivity.ARG_BUSINESS, mBusiness);
                startActivity(intent);
            }
        });
    }

    private void goToChatActivity() {
        if(!Account.isSignedIn()) {
            switchToSignInActivity();
            return;
        }
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.ARG_PARTICIPANT_IS_BUSINESS, true);
        intent.putExtra(ChatActivity.ARG_PARTICIPANT_USER_ID, mBusiness.getAdmin().getId());
        intent.putExtra(ChatActivity.ARG_PARTICIPANT_JABBER_ID, String.format("%s@%s", mBusiness.getAdmin().getJabberId(), ListSting.DOMAIN));
        startActivity(intent);
    }

    private void switchToSignInActivity() {
        Intent intent = new Intent(this, SignInActivity.class);
        intent.putExtra(SignInActivity.FINISH_ACTION, SignInActivity.OPEN_CHAT_ACTIVITY);
        intent.putExtra(ChatActivity.ARG_PARTICIPANT_IS_BUSINESS, true);
        intent.putExtra(ChatActivity.ARG_PARTICIPANT_USER_ID, mBusiness.getAdmin().getId());
        intent.putExtra(ChatActivity.ARG_PARTICIPANT_JABBER_ID, String.format("%s@%s", mBusiness.getAdmin().getJabberId(), ListSting.DOMAIN));
        startActivity(intent);
    }

    private void addLocation() {
        if(mBusiness.canShowLocation()) {
            mMapView.initialize(getSupportFragmentManager());
            mMapView.setOnMapInitializedListener(this);
            mMapViewBuilder = new DefaultAirMapViewBuilder(this);
        } else {
            mMapContainer.setVisibility(View.GONE);
        }
    }

    private void selectAvatarPhoto() {
        if(Account.isShowingPersonalBusiness(mBusiness)) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/jpeg");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        }
    }

    private void updateAndUploadAvatar(int resultCode, Uri data) {
        if(resultCode == RESULT_OK){
            try {
                mAvatarFile = new File(FileUtils.getPath(this, data));
                mAvatar.setLocalImageBitmap(BitmapUtils.decodeUri(this, data,
                        mAvatar.getWidth(),
                        mAvatar.getHeight()));

                uploadAvatar();
            } catch (FileNotFoundException e) {
                RecordedLog.e(RecordedLog.Tag.PROFILE, e);
            }
        }
    }

    private void uploadAvatar() {
        final Map<String, File> files = new HashMap<>();
        files.put(AVATAR_PARAM_NAME, mAvatarFile);

        new Thread() {
            @Override
            public void run() {
                MultipartRequest imageRequest = new MultipartRequest(UPDATE_BUSINESS_ENDPOINT, null, files,
                        new RestClient.Listener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Account.getDefaultAccount().updateFromJsonObject(response);
                            }
                        },
                        new RestClient.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                RecordedLog.e(RecordedLog.Tag.API, "Business > Avatar Upload > " + error);
                                //TODO: Show error message
                            }
                        });

                ListSting.getRequestQueue().add(imageRequest);
            }
        }.start();
    }

    private void addFragments() {
        BusinessDetailsFragment businessDetailsFragment = BusinessDetailsFragment.newInstance(mBusiness);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.details_fragment_container, businessDetailsFragment).commit();

        BusinessServicesFragment businessServicesFragment = BusinessServicesFragment.newInstance(mBusiness);
        FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
        transaction2.add(R.id.services_fragment_container, businessServicesFragment).commit();
    }

    private void removeButtons() {
        if(Account.isShowingPersonalBusiness(mBusiness)) {
            mMessageButton.setVisibility(View.GONE);
        } else {
            mEditBusinessButton.setVisibility(View.GONE);
        }
    }

    private void setupActionBar() {
        setActionBarTitle();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setActionBarTitle() {
        if(Account.isShowingPersonalBusiness(mBusiness)) {
            getSupportActionBar().setTitle(getString(R.string.business));
        } else {
            getSupportActionBar().setTitle(mBusiness.getBusinessName());
        }
    }

    public boolean canShowFavotites() {
        return Account.isCustomerProfileSignedIn() || Account.isShowingOthersBusiness(mBusiness);
    }

    private void populateBusinessDetails() {
        mBusinessName.setText(mBusiness.getBusinessName());
        populateImages();
    }

    private void populateImages() {
        mAvatar.setDefaultImageResId(R.drawable.business_avatar);

        if(!TextUtils.isEmpty(mBusiness.getAvatarUrl())) {
            mAvatar.setImageUrl(ListSting.API_ENDPOINT + mBusiness.getAvatarUrl(), ListSting.getImageLoader());
        }
    }

    private void checkBusinessFavorite() {
        for(Business favorite : Account.getDefaultAccount().getFavorites()) {
            if(favorite.getId() == mBusiness.getId()) {
                mFavoriteMenuItemOutline.setVisible(false);
                mFavoriteMenuItem.setVisible(true);
                return;
            }
        }
    }

    private void showFavoriteBusinessStar(boolean isVisible) {
        mFavoriteMenuItemOutline.setVisible(!isVisible);
        mFavoriteMenuItem.setVisible(isVisible);
        String message = isVisible ? getString(R.string.bussines_added_favorites) : getString(R.string.bussines_removed_favorites);
        Snackbar.make(mContainer, message, Snackbar.LENGTH_SHORT)
                .show();
    }

    private void addFavorite() {
        final String ADD_FAVORITE_BUSINESS_ENDPOINT = String.format("/business/%d/add_favorite/", mBusiness.getId());
        final Map<String, String> params = new HashMap<>();
        params.put("user", String.valueOf(Account.getDefaultAccount().getId()));
        new Thread() {
            @Override
            public void run() {
                MultipartRequest addFavoriteRequest = new MultipartRequest(ADD_FAVORITE_BUSINESS_ENDPOINT, params, null, new RestClient.Listener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Account.getDefaultAccount().addFavorite(mBusiness);
                        showFavoriteBusinessStar(true);
                    }
                }, new RestClient.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        RecordedLog.e(RecordedLog.Tag.API, "Business > Adding Favorite > " + error);
                        //TODO: Create SnackBar error message
                    }
                });
                ListSting.getRequestQueue().add(addFavoriteRequest);
            }
        }.start();
    }

    private void removeFavorite() {
        final String REMOVE_FAVORITE_BUSINESS_ENDPOINT = String.format("/business/%d/remove_favorite/", mBusiness.getId());
        final Map<String, String> params = new HashMap<>();
        params.put("user", String.valueOf(Account.getDefaultAccount().getId()));
        new Thread() {
            @Override
            public void run() {
                MultipartRequest addFavoriteRequest = new MultipartRequest(REMOVE_FAVORITE_BUSINESS_ENDPOINT, params, null, new RestClient.Listener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Account.getDefaultAccount().removeFavorite(mBusiness);
                        showFavoriteBusinessStar(false);
                    }
                }, new RestClient.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        RecordedLog.e(RecordedLog.Tag.API, "Business > Removing Favorite > " + error);
                        //TODO: Create SnackBar error message
                    }
                });
                ListSting.getRequestQueue().add(addFavoriteRequest);
            }
        }.start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(imageReturnedIntent == null) {
                    return;
                }
                updateAndUploadAvatar(resultCode, imageReturnedIntent.getData());
                break;
            case EDIT_BUSINESS:
                refreshActivity();
                break;
            case PhotoDialog.CAMERA:
                break;
            case PhotoDialog.GALLERY:
                if(resultCode == RESULT_OK){
                    if(imageReturnedIntent == null) {
                        return;
                    }
                    Uri selectedImage = imageReturnedIntent.getData();
                    uploadPhoto(new File(FileUtils.getPath(this, selectedImage)));
                }
                break;
        }
    }

    private void refreshActivity() {
        Intent intent = new Intent(this, BusinessActivity.class);
        startActivity(intent);
        finish();
    }

    private void uploadPhoto(File file) {
        final Map<String, File> files = new HashMap<>();
        files.put(PHOTO_PARAM_NAME, file);

        new Thread() {
            @Override
            public void run() {
                MultipartRequest imageRequest = new MultipartRequest(PHOTO_UPLOAD_BUSINESS_ENDPOINT, null, files,
                        new RestClient.Listener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Account.getDefaultAccount().updateFromJsonObject(response);
                                refreshPhotos();
                            }
                        },
                        new RestClient.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                RecordedLog.e(RecordedLog.Tag.API, "Business > Photo Upload > " + error);
                                //TODO: Show error message
                            }
                        });
                ListSting.getRequestQueue().add(imageRequest);
            }
        }.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(Account.isShowingPersonalBusiness(mBusiness)) {
            inflatePersonalBusinessMenu(menu);
            return true;
        }
        if(canShowFavotites()) {
            inflateFavoritesMenu(menu);
            return true;
        }
        return false;
    }

    private void inflatePersonalBusinessMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_business_personal, menu);
    }

    private void inflateFavoritesMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_business_favorite, menu);

        mFavoriteMenuItemOutline = menu.findItem(R.id.action_add_favorite);
        mFavoriteMenuItem = menu.findItem(R.id.action_remove_favorite);
        checkBusinessFavorite();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshPhotos();
    }

    private void refreshPhotos() {
        if(Account.isShowingPersonalBusiness(mBusiness)) {
            mBusiness = Account.getDefaultAccount().getBusiness();
            mPreviewAdapter.setPhotos(mBusiness.getPhotos());
            mCarouselAdapter.setPhotos(mBusiness.getPhotos());
            mPreviewAdapter.notifyDataSetChanged();
            mCarouselAdapter.notifyDataSetChanged();
            mImagePreviewPager.setCurrentItem(0);
            showImagePreviewPager();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_add_favorite:
                addFavorite();
                return true;
            case R.id.action_remove_favorite:
                removeFavorite();
                return true;
            case R.id.action_add_media:
                addPhoto();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addPhoto() {
        new PhotoDialog(this);
    }

    @Override
    public void onMapInitialized() {
        AddressData addressData = LocationUtils.parseAddressData(mBusiness);
        if(addressData != null) {
            final String location = LocationUtils.buildLocationName(addressData);
            final Address address = LocationUtils.getAddress(location, this);
            if(address != null) {
                final LatLng addressLatLng = new LatLng(address.getLatitude(), address.getLongitude());
                final LatLng offsetLatLng = new LatLng(address.getLatitude() - 0.02, address.getLongitude());
                mMapContainer.setVisibility(View.VISIBLE);
                mAddressLocation.setText(location);
                mMapView.setCenterZoom(offsetLatLng, 10);
                mMapView.drawCircle(addressLatLng, 1500, getResources().getColor(R.color.map_border), 5, getResources().getColor(R.color.map_fill));
                mOpenGoogleMaps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openGoogleMaps(addressLatLng, location);
                    }
                });
            }
        }
    }

    private void openGoogleMaps(LatLng latLng, String address) {
        Uri location = Uri.parse(String.format("geo:%s,-%s?q=%s", latLng.latitude, latLng.longitude, address));
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, location);
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }

    public void setBusiness(Business business) {
        mBusiness = business;
    }
}