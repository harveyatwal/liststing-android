package com.liststing.android.ui.business;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.liststing.android.R;
import com.liststing.android.datasets.IndustryTable;
import com.liststing.android.models.Business;
import com.liststing.android.models.Industry;

import java.util.ArrayList;

public class ListBusinessFragment extends Fragment {

    private static final String ARG_BUSINESS = "ARG_BUSINESS";
    private ListView mListView;
    private ArrayAdapter<Industry> mIndustryAdapter;
    private Business mBusiness;
    private boolean mIsEditting = false;

    public static ListBusinessFragment newInstance(Business mBusiness) {
        ListBusinessFragment fragment = new ListBusinessFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_BUSINESS, mBusiness);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list_business, container, false);

        mBusiness = getArguments().getParcelable(ARG_BUSINESS);
        mListView = (ListView) rootView.findViewById(R.id.category_list_view);

        setActionBarTitle();
        addIndustriesToListView();
        return rootView;
    }

    private void setActionBarTitle() {
        if(!(getActivity() instanceof ListBusinessActivity)) {
            return;
        }
        ((ListBusinessActivity) getActivity()).getSupportActionBar()
                .setTitle(getString(R.string.action_list_your_business));
    }

    private void addIndustriesToListView() {
        ArrayList<Industry> industryNames = IndustryTable.getIndustries();
        mIndustryAdapter = new IndustryAdapter(getActivity(), R.layout.item_list_business_industry, industryNames);
        mListView.setAdapter(mIndustryAdapter);
    }

    private class IndustryAdapter extends ArrayAdapter<Industry> implements View.OnClickListener {
        public IndustryAdapter(Context context, int resource, ArrayList<Industry> industryNames) {
            super(context, resource, industryNames);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);
            TextView industryTextView = (TextView) v.findViewById(R.id.industry_name);
            industryTextView.setText(getItem(position).getName());
            industryTextView.setTag(position);
            industryTextView.setOnClickListener(this);
            return v;
        }

        @Override
        public void onClick(View view) {
            moveToBusinessTypeFrag(getItem((int) view.getTag()));
        }

        private void moveToBusinessTypeFrag(Industry item) {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.enter_from_right,  R.anim.exit_to_left);
            transaction.replace(R.id.details_fragment_container, ListBusinessCategoryFragment.newInstance(item, mBusiness));
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
