package com.liststing.android.ui.business;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.liststing.android.R;
import com.liststing.android.datasets.BusinessCollection;
import com.liststing.android.models.Industry;
import com.liststing.android.networking.events.BusinessEvents;
import com.liststing.android.networking.services.BusinessService;
import com.liststing.android.ui.widget.Overlay;
import com.liststing.android.ui.widget.SearchBar;
import com.liststing.android.util.NetworkUtils;

import de.greenrobot.event.EventBus;

public class BusinessListActivity extends AppCompatActivity implements SearchBar.OnSearchListener {

    public static final String ARG_INDUSTRY = "ARG_INDUSTRY";
    public static final String ARG_QUERY = "ARG_QUERY";
    private static final String TAG_FRAGMENT_BUSINESS_LIST = "TAG_FRAGMENT_BUSINESS_LIST";
    private Industry mIndustry;
    private Toolbar mToolbar;
    private BusinessCollection mBusinesses;
    private ProgressBar mProgressBar;
    private BusinessListFragment mBusinessListFragment;
    private View mNoNetworkView;
    private View mNoServerView;
    private SearchBar mSearchBar;
    private String mQuery = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_list);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            mIndustry = extras.getParcelable(ARG_INDUSTRY);
            mQuery = extras.getString(ARG_QUERY, "");
        }

        mSearchBar = (SearchBar) findViewById(R.id.searchbar);
        mSearchBar.setOnSearchListener(this);
        mSearchBar.addOverlay((Overlay) findViewById(R.id.overlay));

        // TODO: Create a retry button for these two views
        mNoNetworkView = findViewById(R.id.no_network_view);
        mNoServerView = findViewById(R.id.no_server_view);
        mProgressBar = (ProgressBar) findViewById(R.id.business_progress_bar);

        setupActionBar();

        if(savedInstanceState == null) {
            loadBusinesses(mQuery); // Load industry businesses with no query
        } else {
            mBusinessListFragment = (BusinessListFragment) getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT_BUSINESS_LIST);
            showEmptyMessageIfExists();
        }
    }

    private void showEmptyMessageIfExists() {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            mNoNetworkView.setVisibility(View.VISIBLE);
        }
    }

    private void clearEmptyViews() {
        mNoNetworkView.setVisibility(View.GONE);
        mNoServerView.setVisibility(View.GONE);
    }

    private void setupActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle(getString(R.string.action_search));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSubtitle();
    }

    private void loadBusinesses(String query) {
        mBusinesses = new BusinessCollection();
        if (!NetworkUtils.isNetworkAvailable(this)) {
            mNoNetworkView.setVisibility(View.VISIBLE);
            removeFragment();
            return;
        }

        if(mIndustry != null) {
            BusinessService.startService(this, mIndustry.getId(), query);
        } else {
            BusinessService.startService(this, -1, query);
        }
    }

    private void removeFragment() {
        if(mBusinessListFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mBusinessListFragment).commit();
        }
    }

    private void addFragment() {
        mBusinessListFragment = BusinessListFragment.newInstance(mBusinesses, false);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, mBusinessListFragment, TAG_FRAGMENT_BUSINESS_LIST)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(BusinessEvents.RetrievingBusinessesStarted event) {
        mProgressBar.setVisibility(View.VISIBLE);
    }


    @SuppressWarnings("unused")
    public void onEventMainThread(BusinessEvents.RetrievingBusinessesEnded event) {
        mProgressBar.setVisibility(View.GONE);

        if(event.getResult().hasFailed()) {
            mNoServerView.setVisibility(View.VISIBLE);
            removeFragment();
        }
    }
    @SuppressWarnings("unused")
    public void onEventMainThread(BusinessEvents.RetrievingBusinessesHasData event) {
        setSubtitle();
        clearEmptyViews();
        mProgressBar.setVisibility(View.GONE);

        BusinessCollection businesses = event.getBusinesses();
        mBusinesses.clear();
        mBusinesses.addAll(businesses);
        removeFragment();
        addFragment();
    }

    private void setSubtitle() {
        ActionBar actionBar = getSupportActionBar();
        if(mIndustry != null) {
            actionBar.setSubtitle(mIndustry.getName());
        } else {
            actionBar.setSubtitle(getString(R.string.all));
        }
        addBreadCrumb();
    }

    private void addBreadCrumb() {
        ActionBar actionBar = getSupportActionBar();
        if(!mQuery.isEmpty()) {
            String newSubTitle = actionBar.getSubtitle() + "  /  " + mQuery.substring(0, 1).toUpperCase() + mQuery.substring(1);;
            actionBar.setSubtitle(newSubTitle);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onSearch(String query) {
        mQuery = query;
        loadBusinesses(query);
    }
}
