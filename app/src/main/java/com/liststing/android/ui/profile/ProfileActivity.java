package com.liststing.android.ui.profile;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.networking.MultipartRequest;
import com.liststing.android.networking.RestClient;
import com.liststing.android.networking.events.AccountEvents;
import com.liststing.android.ui.widget.AvatarImageView;
import com.liststing.android.util.BitmapUtils;
import com.liststing.android.util.FileUtils;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

public class ProfileActivity extends AppCompatActivity {

    private static final String UPDATE_ACCOUNT = "/user/current/update/";
    private static final String AVATAR_PARAM_NAME = "avatar";

    // StartActivityForResult Request codes
    private static final int SELECT_PHOTO = 100;

    private AvatarImageView mAvatar;
    private TextView mUserName;
    private Account mProfile;

    private File mAvatarFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mAvatar = (AvatarImageView) findViewById(R.id.profile_avatar);
        mAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectPhoto();
            }
        });
        mUserName = (TextView) findViewById(R.id.profile_user_name);

        mProfile = Account.getDefaultAccount();

        setupActionBar();
        populateAccountDetails();
        if(savedInstanceState == null) {
            addDetailsFragment();
        }
    }

    private void addDetailsFragment() {
        ProfileDetailsFragment profileDetailsFragment = ProfileDetailsFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.details_fragment_container, profileDetailsFragment).commit();
    }

    private void setupActionBar() {
        getSupportActionBar().setTitle(getString(R.string.profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void populateAccountDetails() {
        populateUsername();
        populateProfileImages();
    }

    private void populateUsername() {
        mUserName.setText(String.format("%s %s", mProfile.getFirstName(), mProfile.getLastName()));
    }

    private void populateProfileImages() {
        mAvatar.setDefaultImageResId(R.drawable.avatar);
        mAvatar.setImageUrl(ListSting.API_ENDPOINT + mProfile.getAvatarUrl(), ListSting.getImageLoader());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(imageReturnedIntent == null) {
                    return;
                }
                updateAndUploadAvatar(resultCode, imageReturnedIntent.getData());
                break;
        }
    }


    private void updateAndUploadAvatar(int resultCode, Uri data) {
        if(resultCode == RESULT_OK){
            try {
                mAvatarFile = new File(FileUtils.getPath(this, data));
                mAvatar.setLocalImageBitmap(BitmapUtils.decodeUri(this, data,
                        mAvatar.getWidth(),
                        mAvatar.getHeight()));

                uploadAvatar();
            } catch (FileNotFoundException e) {
                RecordedLog.e(RecordedLog.Tag.PROFILE, e);
            }
        }
    }

    private void uploadAvatar() {
        final Map<String, File> files = new HashMap<>();
        files.put(AVATAR_PARAM_NAME, mAvatarFile);

        new Thread() {
            @Override
            public void run() {
                MultipartRequest imageRequest = new MultipartRequest(UPDATE_ACCOUNT, null, files,
                        new RestClient.Listener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // update local account database and store bitmap
                                mProfile.updateFromJsonObject(response);
                                EventBus.getDefault().post(new AccountEvents.AccountUpdated());

                            }
                        },
                        new RestClient.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                RecordedLog.e(RecordedLog.Tag.API, "Profile > Avatar Upload > " + error);
                            }
                        });

                ListSting.getRequestQueue().add(imageRequest);
            }
        }.start();
    }

    private void selectPhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/jpeg");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(AccountEvents.AccountUpdated event) {
        populateUsername();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
