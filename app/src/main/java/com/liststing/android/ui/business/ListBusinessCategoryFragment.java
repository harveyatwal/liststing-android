package com.liststing.android.ui.business;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.liststing.android.R;
import com.liststing.android.datasets.CategoryCollection;
import com.liststing.android.datasets.IndustryTable;
import com.liststing.android.datasets.ServiceCollection;
import com.liststing.android.models.Business;
import com.liststing.android.models.Category;
import com.liststing.android.models.Industry;
import com.liststing.android.models.Service;
import com.liststing.android.networking.events.CategoryEvents;
import com.liststing.android.networking.services.CategoryService;
import com.liststing.android.util.NetworkUtils;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;


public class ListBusinessCategoryFragment extends Fragment {

    private static final String SAVED_STATE_CHECKED_SERVICES = "SAVED_STATE_CHECKED_SERVICES";
    private static final String SAVED_STATE_CATEGORIES = "SAVED_STATE_CATEGORIES";
    private static final String ARG_BUSINESS = "ARG_BUSINESS";

    private ArrayList<Category> mCategories;
    private ArrayList<Service> mCheckedServices;

    private static final String ARG_INDUSTRY_ID = "INDUSTRY_ID";
    private CategoryAdapter mCategoryAdapter;
    private ListView mListView;
    private Industry mIndustry;
    private Button mNextButton;
    private View mNoNetworkView;
    private Business mBusiness;
    private boolean mIsEditting = false;

    public static ListBusinessCategoryFragment newInstance(Industry industry, Business mBusiness) {
        ListBusinessCategoryFragment listBusinessCategoryFragment = new ListBusinessCategoryFragment();

        Bundle args = new Bundle();
        args.putLong(ARG_INDUSTRY_ID, industry.getId());
        args.putParcelable(ARG_BUSINESS, mBusiness);
        listBusinessCategoryFragment.setArguments(args);

        return listBusinessCategoryFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIndustry = IndustryTable.getIndustryById(getArguments().getLong(ARG_INDUSTRY_ID, 0));
        mBusiness = getArguments().getParcelable(ARG_BUSINESS);
        if(mBusiness != null) {
            mIsEditting = true;
        }

        if(savedInstanceState == null) {
            mCheckedServices = new ArrayList<>();
            loadTypes();
        } else {
            mCheckedServices = savedInstanceState.getParcelableArrayList(SAVED_STATE_CHECKED_SERVICES);
            mCategories = savedInstanceState.getParcelableArrayList(SAVED_STATE_CATEGORIES);
            addTypesToListView((CategoryCollection) mCategories);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list_business_type, container, false);

        mNoNetworkView = rootView.findViewById(R.id.no_network_view);

        mListView = (ListView) rootView.findViewById(R.id.category_list_view);
        mNextButton = (Button) rootView.findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToBusinessLocationFrag();
            }
        });

        setActionBarTitle();
        mListView.setAdapter(getCategoryAdapter());
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        return rootView;
    }

    private void setActionBarTitle() {
        if(!(getActivity() instanceof ListBusinessActivity)) {
            return;
        }
        ((ListBusinessActivity) getActivity()).getSupportActionBar()
                .setTitle(mIndustry.getName());
    }

    private CategoryAdapter getCategoryAdapter() {
        if (mCategoryAdapter == null) {
            mCategoryAdapter = new CategoryAdapter(getActivity(), R.layout.item_list_business_category, R.id.category_name, new CategoryCollection());
        }
        return mCategoryAdapter;
    }

    private void loadTypes() {
        if (!NetworkUtils.isNetworkAvailable(getActivity())) {
            mNoNetworkView.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.GONE);
            return;
        }
        CategoryService.startService(getActivity(), mIndustry.getId());
    }

    private void moveToBusinessLocationFrag() {
        if(!hasTypeChecked()) {
            return;
        }
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,  R.anim.exit_to_left);
        transaction.replace(R.id.details_fragment_container, ListBusinessLocationFragment.newInstance(mIndustry, mCheckedServices, mBusiness));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void enableNextButton(boolean enabled) {
        mNextButton.setEnabled(enabled);
    }

    private void addTypesToListView(CategoryCollection categories) {
        getCategoryAdapter().addAll(categories);
    }

    private void reenableNextButton() {
        if(mCheckedServices.size() > 0) {
            enableNextButton(true);
        }
    }

    private boolean hasTypeChecked() {
        return mCheckedServices.size() != 0;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(SAVED_STATE_CHECKED_SERVICES, mCheckedServices);
        outState.putParcelableArrayList(SAVED_STATE_CATEGORIES, mCategories);
    }


    @SuppressWarnings("unused")
    public void onEventMainThread(CategoryEvents.RetrievingCategoriesStarted event) {
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(CategoryEvents.RetrievingCategoriesEnded event) {
        if(event.getResult().hasFailed()) {
            mListView.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(CategoryEvents.RetrievingCategoriesHasData event) {
        mCategories = event.getCategories();
        addTypesToListView((CategoryCollection) mCategories);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        reenableNextButton();
    }

    private class CategoryAdapter extends ArrayAdapter<Category> {

        public CategoryAdapter(Context context, int resource, int textview, ArrayList<Category> categories) {
            super(context, resource, textview, categories);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);
            Category category = getItem(position);

            TextView categoryTextView = (TextView) v.findViewById(R.id.category_name);
            final LinearLayout categoryServiceList = (LinearLayout) v.findViewById(R.id.category_services);

            if(categoryServiceList.getChildCount() == 0) {
                addServicesToLayout(categoryServiceList, category.getServices());
            }

            categoryTextView.setText(category.getName());
            categoryTextView.setTag(position);
            categoryTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (categoryServiceList.getVisibility() == View.GONE) {
                        categoryServiceList.setVisibility(View.VISIBLE);
                        return;
                    }
                    categoryServiceList.setVisibility(View.GONE);
                }
            });
            openCategoryServiceList(category, categoryServiceList);
            return v;
        }

        private void openCategoryServiceList(Category category, LinearLayout categoryServiceList) {
            if(mIsEditting) {
                for(Service service : mBusiness.getServices()) {
                    if(service.getCategory().getId() == category.getId()) {
                        categoryServiceList.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        private void addServicesToLayout(LinearLayout categoryList, ServiceCollection services) {
            for(final Service service : services) {
                CheckedTextView serviceCheckBox = (CheckedTextView) getActivity().getLayoutInflater().inflate(R.layout.item_list_business_service_checkbox, null);
                serviceCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addCheckedService(service, (CheckedTextView) view);
                    }
                });

                serviceCheckBox.setText(service.getName());
                categoryList.addView(serviceCheckBox);

                if(mIsEditting) {
                    for(Service businessService : mBusiness.getServices()) {
                        if(businessService.getId() == service.getId()) {
                            addCheckedService(service, serviceCheckBox);
                        }
                    }
                }
                for(Service checkedService : mCheckedServices) {
                    if(service.getId() == checkedService.getId()) {
                        serviceCheckBox.setChecked(true);
                        break;
                    }
                }
            }
        }

        private void addCheckedService(Service service, CheckedTextView view) {
            if (view.isChecked()) {
                setUnchecked(service, view);
                return;
            }
            setChecked(service, view);
        }

        private void setUnchecked(Service service, CheckedTextView view) {
            view.setChecked(false);
            mCheckedServices.remove(service);
            if(mCheckedServices.size() == 0) {
                enableNextButton(false);
            }
        }

        private void setChecked(Service service, CheckedTextView view) {
            view.setChecked(true);
            mCheckedServices.add(service);
            if(mCheckedServices.size() == 1) {
                enableNextButton(true);
            }
        }
    }
}
