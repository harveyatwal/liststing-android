package com.liststing.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.liststing.android.R;

public class Overlay extends View {

    final Animation animationFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
    final Animation animationFadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
    private int mIsVisible;

    public Overlay(Context context) {
        super(context);
    }

    public Overlay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Overlay(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onAnimationStart() {
        super.onAnimationStart();
        this.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onAnimationEnd() {
        super.onAnimationEnd();
        if(mIsVisible == View.VISIBLE) {
            this.setVisibility(View.VISIBLE);
        } else {
            this.setVisibility(View.GONE);
        }
    }

    public void setFadeVisibility(int visibility) {
        mIsVisible = visibility;
        startAnimation();
    }

    private void startAnimation() {
        if(mIsVisible == View.VISIBLE) {
            startAnimation(animationFadeIn);
            return;
        }
        startAnimation(animationFadeOut);
    }
}
