package com.liststing.android.ui.business;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.liststing.android.datasets.BusinessMediaCollection;

public class BusinessMediaSlideAdapter extends FragmentStatePagerAdapter {
    private BusinessMediaCollection photos;

    public BusinessMediaSlideAdapter(FragmentManager fm, BusinessMediaCollection photos) {
        super(fm);
        this.photos = photos;
    }

    public void setPhotos(BusinessMediaCollection photos) {
        this.photos = photos;
    }

    @Override
    public Fragment getItem(int position) {
        return BusinessMediaFragment.newInstance(photos.get(position).getImage());
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}
