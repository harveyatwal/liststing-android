package com.liststing.android.ui.feed;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.liststing.android.networking.ServerInterfaces;
import com.liststing.android.util.RecordedLog;

import de.greenrobot.event.EventBus;

/**
 * retrieves updated posts
 */
public class FeedPostService extends Service{

    private static final String ARG_ACTION  = "action";

    public static enum Action {REQUEST_NEWER, REQUEST_OLDER}

    public static void startService(Context context, Action action) {
        Intent intent = new Intent(context, FeedPostService.class);
        intent.putExtra(ARG_ACTION, action);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        RecordedLog.i(RecordedLog.Tag.FEED, "service > created");
    }

    @Override
    public void onDestroy() {
        RecordedLog.i(RecordedLog.Tag.FEED, "service > destroyed");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_NOT_STICKY;
        }

        Action action = Action.REQUEST_NEWER;
        if (intent.hasExtra(ARG_ACTION)) {
            action = (Action) intent.getSerializableExtra(ARG_ACTION);
        }


        EventBus.getDefault().post(new FeedEvents.UpdatePostsStarted(action));
        updatePosts(action);

        return START_NOT_STICKY;
    }

    private void updatePosts(final Action action) {
        ServerInterfaces.ServerResultListener listener = new ServerInterfaces.ServerResultListener() {

            @Override
            public void onServerResult(ServerInterfaces.ServerResult result) {
                EventBus.getDefault().post(new FeedEvents.UpdatePostsEnded(result, action));
                stopSelf();
            }
        };
        requestPosts(action, listener);
    }

    // construct endpoint
    // determine what action to take
    // send request
    private static void requestPosts(final Action updateAction,
                                     final ServerInterfaces.ServerResultListener resultListener) {



    }
}
