package com.liststing.android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class ActionEditText extends EditText {
    private OnActionListener mActionListener;
    private boolean mCanBackPress;

    public ActionEditText(Context context) {
        super(context);
    }

    public ActionEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Hide cursor when keyboard is dismissed.
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && mActionListener != null && mCanBackPress) {
            mActionListener.onBackPress();
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnActionListener(OnActionListener mActionListener) {
        this.mActionListener = mActionListener;
    }

    public void setCanBackPress(boolean canBackPress) {
        mCanBackPress = canBackPress;
    }

    public interface OnActionListener {
        public abstract void onBackPress();
    }
}