package com.liststing.android.ui.business;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.i18n.addressinput.common.AddressData;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Business;
import com.liststing.android.models.Service;
import com.liststing.android.ui.widget.AvatarImageView;
import com.liststing.android.util.LocationUtils;

import java.util.ArrayList;

/**
 * Hosts the items within the BusinessList RecycleView
 */
public class BusinessListAdapter extends RecyclerView.Adapter<BusinessListAdapter.ViewHolder> {

    private final Context mContext;
    private ArrayList<Business> mBusinesses;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AvatarImageView mBusinessAvatar;
        TextView mBusinessName;
        TextView mBusinessServices;
        TextView mBusinessAddress;
        TextView mBusinessDistance;
        View mContainer;

        public ViewHolder(View itemView) {
            super(itemView);

            mContainer = itemView.findViewById(R.id.container);
            mBusinessAvatar = (AvatarImageView) itemView.findViewById(R.id.business_avatar);
            mBusinessAddress = (TextView) itemView.findViewById(R.id.business_location);
            mBusinessDistance = (TextView) itemView.findViewById(R.id.business_distance);
            mBusinessName = (TextView) itemView.findViewById(R.id.business_name);
            mBusinessServices = (TextView) itemView.findViewById(R.id.business_services);
        }
    }

    public BusinessListAdapter(Context context, ArrayList<Business> businesses) {
        mContext = context;
        mBusinesses = businesses;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_business, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Business business = mBusinesses.get(position);

        AvatarImageView businessAvatar = holder.mBusinessAvatar;
        TextView businessName = holder.mBusinessName;
        TextView businessServices = holder.mBusinessServices;
        TextView businessAddress = holder.mBusinessAddress;
        TextView businessDistance = holder.mBusinessDistance;

        if(business.canShowLocation()) {
            AddressData addressData = LocationUtils.parseAddressData(business);
            if(addressData != null) {
                businessAddress.setVisibility(View.VISIBLE);
                businessDistance.setVisibility(View.VISIBLE);
                businessAddress.setText(getBusinessAddress(addressData));
                businessDistance.setText(LocationUtils.calculateBusinessDistance(addressData, mContext));
            }
        }

        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToBusinessActivity(business);
            }
        });

        setBusinessAvatar(business, businessAvatar);
        setBusinessServices(business, businessServices);
        businessName.setText(business.getBusinessName());
    }

    private String getBusinessAddress(AddressData addressData) {
        return LocationUtils.buildLocationName(addressData, false);
    }


    private void moveToBusinessActivity(Business business) {
        Intent intent = new Intent(mContext, BusinessActivity.class);
        intent.putExtra(BusinessActivity.ARG_BUSINESS, business);
        mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return mBusinesses.size();
    }

    private void setBusinessServices(Business business, TextView businessServices) {
        String servicesText = "";
        int i = 1, limit = 2;
        for(Service service : business.getServices()) {
            if(i++ == limit) {
                servicesText += ", " + service.getName();
                break;
            }
            servicesText += service.getName();
        }
        businessServices.setText(servicesText);
    }

    private void setBusinessAvatar(Business business, AvatarImageView businessAvatar) {
        businessAvatar.setDefaultImageResId(R.drawable.business_avatar);

        if(!TextUtils.isEmpty(business.getAvatarUrl())) {
            businessAvatar.setImageUrl(ListSting.API_ENDPOINT + business.getAvatarUrl(), ListSting.getImageLoader());
        }
    }
}
