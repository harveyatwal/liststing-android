package com.liststing.android.ui.business;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.liststing.android.R;
import com.liststing.android.models.Business;

public class BusinessGalleryActivity extends AppCompatActivity {

    public static final String ARG_BUSINESS = "arg_business";
    private static final String TAG_GALLERY_FRAGMENT = "tag_gallery_fragment";
    private Business mBusiness;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_gallery);

        Bundle extras = getIntent().getExtras();
        if(extras != null && savedInstanceState == null) {
            mBusiness = extras.getParcelable(ARG_BUSINESS);
            addGalleryFragment();
        }
        setupActionBar();
    }

    private void addGalleryFragment() {
        BusinessGalleryFragment galleryFragment = BusinessGalleryFragment.newInstance(mBusiness);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.gallery_fragment, galleryFragment, TAG_GALLERY_FRAGMENT).commit();
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
