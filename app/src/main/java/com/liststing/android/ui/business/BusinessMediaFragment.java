package com.liststing.android.ui.business;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.ui.widget.BitmapNetworkImageView;


public class BusinessMediaFragment extends Fragment{

    private static final String ARG_URL = "ARG_IMAGE_URL";
    String mImageURL;
    private BitmapNetworkImageView mImage;
    private ProgressBar mPreviewProgres;

    public static BusinessMediaFragment newInstance(String imageURL) {
        BusinessMediaFragment businessMediaFragment = new BusinessMediaFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BusinessMediaFragment.ARG_URL, imageURL);
        businessMediaFragment.setArguments(bundle);

        return businessMediaFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.item_list_photo_slide, container, false);

        mPreviewProgres = (ProgressBar) rootView.findViewById(R.id.preview_progress);
        mImage = (BitmapNetworkImageView) rootView.findViewById(R.id.image);
        mImageURL = getArguments().getString(ARG_URL);

        mImage.setResponseObserver(new BitmapNetworkImageView.ResponseObserver() {
            @Override
            public void onError() {
                mPreviewProgres.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess() {
                mPreviewProgres.setVisibility(View.GONE);
            }
        });
        mImage.setImageUrl(ListSting.API_ENDPOINT + mImageURL, ListSting.getImageLoader());
        return rootView;
    }
}
