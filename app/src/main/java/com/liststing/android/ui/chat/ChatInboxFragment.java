package com.liststing.android.ui.chat;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liststing.android.R;

public class ChatInboxFragment extends Fragment {

    private View mNoMessagesView;

    public static ChatInboxFragment newInstance() {
        return new ChatInboxFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_chat, container, false);

        mNoMessagesView = rootView.findViewById(R.id.no_messages);
        mNoMessagesView.setVisibility(View.VISIBLE);

        return rootView;
    }

}
