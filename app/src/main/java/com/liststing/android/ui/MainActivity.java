package com.liststing.android.ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.models.Business;
import com.liststing.android.networking.services.ChatService;
import com.liststing.android.ui.widget.AvatarImageView;
import com.liststing.android.ui.widget.SlidingTabLayout;

/**
 * Launched activity
 */
public class MainActivity extends BaseActivity implements ServiceConnection {

    public static final String ARG_IS_BUSINESS = "ARG_IS_BUSINESS";

    private MainTabAdapter mTabAdapter;
    private SlidingTabLayout mTabbar;
    private ViewPager mViewPager;
    private AvatarImageView mAvatar;
    private AvatarImageView mBusinessAvatar;
    private Account mProfile;
    private TextView mProfileName;
    private TextView mBusinessName;
    private NavigationView mNavigationView;
    private ChatService.ChatController mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationView = (NavigationView) findViewById(R.id.navigation);
        mViewPager = (ViewPager) findViewById(R.id.main_viewpager);
        mTabAdapter = new MainTabAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mTabAdapter);

        mTabbar = (SlidingTabLayout) findViewById(R.id.tabbar);
        mTabbar.setSelectedIndicatorColors(getResources().getColor(R.color.tab_indicator));
        mTabbar.setCustomTabView(R.layout.tab_text, android.R.id.text1);
        mTabbar.setDistributeEvenly(true);
        mTabbar.setViewPager(mViewPager);

        setUpChatService();
        setUpToolbar();
        setUpNavBar();
    }

    private void setUpChatService() {
        Intent intent = new Intent(this, ChatService.class);
        startService(intent);
        bindService(intent, this, BIND_AUTO_CREATE);
    }

    private void changeMenuItemsIfBusiness() {
        if(mProfile.hasBusiness()) {
            Menu menu = mNavigationView.getMenu();
            menu.removeItem(R.id.action_list_business);
            menu.removeItem(R.id.action_why_list);
            menu.findItem(R.id.action_business).setVisible(true);
        }
    }

    private void populateNavBarProfileDetails() {
        mProfile = Account.getDefaultAccount();

        populateProfileDetails();
        populateBusinessDetails();
        changeMenuItemsIfBusiness();
    }

    private void populateBusinessDetails() {
        if(mProfile.hasBusiness()) {
            Business business = mProfile.getBusiness();
            mBusinessAvatar = (AvatarImageView) findViewById(R.id.business_avatar);
            mBusinessName = (TextView) findViewById(R.id.business_name);

            mBusinessAvatar.setDefaultImageResId(R.drawable.business_avatar);
            mBusinessAvatar.setImageUrl(ListSting.API_ENDPOINT + business.getAvatarUrl(), ListSting.getImageLoader());
            mBusinessName.setText(String.format("%s", business.getBusinessName()));
        } else {
            View view = findViewById(R.id.business_details);
            view.setVisibility(View.GONE);
        }
    }

    private void populateProfileDetails() {
        mAvatar = (AvatarImageView) findViewById(R.id.profile_avatar);
        mProfileName = (TextView) findViewById(R.id.profile_name);

        mAvatar.setDefaultImageResId(R.drawable.avatar);
        mAvatar.setImageUrl(ListSting.API_ENDPOINT + mProfile.getAvatarUrl(), ListSting.getImageLoader());
        mProfileName.setText(String.format("%s %s", mProfile.getFirstName(), mProfile.getLastName()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        populateNavBarProfileDetails();
        if(mController != null) {
            mController.clearNotification();
        }
    }

    @Override
    protected void onDestroy() {
        unbindService(this);
        super.onDestroy();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        mController = ((ChatService.ChatController) iBinder);
        mController.login(Account.getDefaultAccount().getJabberId(), Account.getDefaultAccount().getAccessToken());
        mController.clearNotification();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }
}
