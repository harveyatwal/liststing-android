package com.liststing.android.ui;

import android.os.Parcelable;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.ui.chat.ChatInboxFragment;
import com.liststing.android.ui.business.BusinessListFragment;
import com.liststing.android.ui.explore.ExploreFragment;

import java.lang.ref.WeakReference;

/**
 * view pager fragment adapter used within MainActivity
 */
public class MainTabAdapter extends FragmentStatePagerAdapter {

    public static final int CLIENT_TAB_SIZE = 3;

    private final SparseArray<WeakReference<Fragment>> mFragments = new SparseArray<>(CLIENT_TAB_SIZE);

    public MainTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        // work around "Fragement no longer exists for key" Android bug
        // by catching the IllegalStateException
        // https://code.google.com/p/android/issues/detail?id=42601
        try {
            super.restoreState(state, loader);
        } catch (IllegalStateException e) {
            // nop
        }
    }

    @Override
    public Fragment getItem(int position) {
        Tab tab = Tab.getByPosition(position);
        return Tab.getFragment(tab);
    }

    @Override
    public int getCount() {
        return CLIENT_TAB_SIZE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Tab tab = Tab.getByPosition(position);
        return tab.getName();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object item = super.instantiateItem(container, position);
        if (item instanceof Fragment) {
            mFragments.put(position, new WeakReference<>((Fragment) item));
        }
        return item;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        mFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    enum Tab {
        EXPLORE(R.string.explore,  0),
        MESSAGE(R.string.messages, 1),
        FAVORITES(R.string.favorites, 2);

        private final int position;
        private final int nameId;

        Tab(@StringRes int nameId, int position) {
            this.nameId = nameId;
            this.position = position;
        }

        public static Tab getByPosition(int position) {
            for(Tab tab : values()) {
                if(tab.position == position)
                    return tab;
            }
            return null;
        }

        public static Fragment getFragment(Tab tab) {
            switch(tab) {
                case EXPLORE:
                    return ExploreFragment.newInstance();
                case MESSAGE:
                    return ChatInboxFragment.newInstance();
                case FAVORITES:
                    return BusinessListFragment.newInstance(Account.getDefaultAccount().getFavorites(), true);
                default:
                    return null;
            }
        }

        public CharSequence getName() {
            return ListSting.getContext().getResources().getString(nameId);
        }
    }
}