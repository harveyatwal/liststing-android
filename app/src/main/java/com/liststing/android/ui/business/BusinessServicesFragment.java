package com.liststing.android.ui.business;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liststing.android.R;
import com.liststing.android.datasets.CategoryCollection;
import com.liststing.android.datasets.ServiceCollection;
import com.liststing.android.models.Business;
import com.liststing.android.models.Category;
import com.liststing.android.models.Service;

public class BusinessServicesFragment extends Fragment {

    private Business mBusiness;
    private LinearLayout mCategoryListLayout;

    public static BusinessServicesFragment newInstance(Business business) {
        BusinessServicesFragment businessServicesFragment = new BusinessServicesFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(BusinessActivity.ARG_BUSINESS, business);
        businessServicesFragment.setArguments(bundle);

        return businessServicesFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_business_services, container, false);

        mBusiness = getArguments().getParcelable(BusinessActivity.ARG_BUSINESS);
        mCategoryListLayout = (LinearLayout) rootView.findViewById(R.id.category_list_view);

        addBusinessServices();
        return rootView;
    }

    private void addBusinessServices() {
        CategoryCollection selectedCategories = new CategoryCollection();
        for(Service service : mBusiness.getServices()) {
            Category category;
            int categoryIndex = selectedCategories.indexOf(service.getCategory());
            if(categoryIndex == -1) {
                category = new Category(service.getCategory().getId(), service.getCategory().getName());
                selectedCategories.add(category);
            } else {
                category = selectedCategories.get(categoryIndex);
            }
            category.getServices().add(service);
        }

        addCategoriesToLayout(selectedCategories);
    }

    private void addCategoriesToLayout(CategoryCollection categories) {
        for(int i = 0; i < categories.size(); i ++) {
            Category category = categories.get(i);
            RelativeLayout categoryLayout = (RelativeLayout) getActivity().getLayoutInflater().inflate(R.layout.item_business_category, null);
            TextView categoryTextView = (TextView) categoryLayout.findViewById(R.id.category_name);
            final ImageView categoryExpandIcon = (ImageView) categoryLayout.findViewById(R.id.category_expand_icon);
            View categoryContainer = categoryLayout.findViewById(R.id.category_container);
            final LinearLayout categoryList = (LinearLayout) categoryLayout.findViewById(R.id.category_services);
            addServicesToLayout(categoryList, category.getServices());

            categoryTextView.setText(category.getName());
            categoryContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (categoryList.getVisibility() == View.GONE) {
                        categoryList.setVisibility(View.VISIBLE);
                        categoryExpandIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_collapse));
                        return;
                    }
                    categoryExpandIcon.setImageDrawable(getResources().getDrawable(R.drawable.icon_expand));
                    categoryList.setVisibility(View.GONE);
                }
            });
            mCategoryListLayout.addView(categoryLayout);

            if(i == 0) {
                categoryLayout.findViewById(R.id.divider).setVisibility(View.GONE);
            }
        }
    }

    private void addServicesToLayout(LinearLayout categoryList, ServiceCollection services) {
        for(final Service service : services) {
            TextView serviceTextView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.item_business_service_textview, null);
            serviceTextView.setText(service.getName());
            categoryList.addView(serviceTextView);
        }
    }
}