package com.liststing.android.ui.business;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.models.Business;
import com.liststing.android.networking.MultipartRequest;
import com.liststing.android.networking.RestClient;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BusinessEditDetailsFragment extends Fragment {

    private static final String UPDATE_BUSINESS_ENDPOINT = "/business/current/update/";

    public static BusinessEditDetailsFragment newInstance() {
        return new BusinessEditDetailsFragment();
    }

    private Business mBusiness;

    private TextView mDescription;
    private TextView mPhoneNumber;
    private TextView mEmail;
    private TextView mWebsite;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_business_edit_details, container, false);
        setHasOptionsMenu(true);

        mBusiness = Account.getDefaultAccount().getBusiness();

        mDescription = (EditText) rootView.findViewById(R.id.business_description);
        mPhoneNumber = (EditText) rootView.findViewById(R.id.business_phone_number);
        mEmail = (EditText) rootView.findViewById(R.id.business_email);
        mWebsite = (EditText) rootView.findViewById(R.id.business_website);

        setActionBar();
        prefillDetails();
        return rootView;
    }

    private void setActionBar() {
        if(!(getActivity() instanceof BusinessActivity)) {
            return;
        }
        ActionBar actionBar = ((BusinessActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_clear_white_24dp);
        actionBar.setTitle(getString(R.string.edit_business));
    }

    private void prefillDetails() {
        mDescription.setText(mBusiness.getDescription());
        mPhoneNumber.setText(mBusiness.getPhoneNumber());
        mEmail.setText(mBusiness.getEmail());
        mWebsite.setText(mBusiness.getWebsite());
    }

    private void finishEditting() {
        final Map<String, String> params = new HashMap<>();
        params.put("description", mDescription.getText().toString());
        params.put("phone_number", mPhoneNumber.getText().toString());
        params.put("email", mEmail.getText().toString());
        params.put("website", mWebsite.getText().toString());
        enableInputs(false);

        new Thread() {
            @Override
            public void run() {
                MultipartRequest updateRequest = new MultipartRequest(UPDATE_BUSINESS_ENDPOINT, params, null, new RestClient.Listener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Account.getDefaultAccount().updateFromJsonObject(response);
                        enableInputs(true);
                        finishBusinessUpdate();
                    }
                }, new RestClient.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        enableInputs(true);
                        RecordedLog.e(RecordedLog.Tag.API, "Updating Business > " + error);

                        //TODO: Create SnackBar error message
                    }
                });
                ListSting.getRequestQueue().add(updateRequest);
            }
        }.start();
    }

    private void finishBusinessUpdate() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    private void enableInputs(boolean isEnabled) {
        mDescription.setEnabled(isEnabled);
        mPhoneNumber.setEnabled(isEnabled);
        mEmail.setEnabled(isEnabled);
        mWebsite.setEnabled(isEnabled);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_finish:
                finishEditting();
                return true;
            case android.R.id.home:
                getActivity().getSupportFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
