package com.liststing.android.ui.widget;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.liststing.android.R;
import com.liststing.android.datasets.SearchSuggestionTable;

import java.util.ArrayList;

import static android.view.ViewTreeObserver.OnGlobalLayoutListener;

public class SearchBar extends RelativeLayout implements TextView.OnEditorActionListener, ActionEditText.OnActionListener, View.OnTouchListener {

    private OnSearchListener mOnSearchListener;
    private ActionEditText mEditText;
    private Overlay mOverlay;
    private ListView mSuggestionContainer;

    private SuggestionAdapter mSuggestionAdapter;
    private boolean mIsFirstTimeDrawing = true;
    private int mSuggestionHeight;
    private int mContainerHeight;
    private boolean mTextBoxHasFocus = false;

    public SearchBar(Context context) {
        super(context);
        init();
    }

    public SearchBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.searchbar, this);
        mSuggestionContainer = (ListView) findViewById(R.id.suggestion_container);
        mEditText = (ActionEditText) findViewById(R.id.search_edit_text);

        mSuggestionAdapter = new SuggestionAdapter(getContext(), R.layout.item_search_suggestion);
        mSuggestionContainer.setAdapter(mSuggestionAdapter);
        mSuggestionContainer.setDivider(null);

        populateSearchSuggestions("");
        initEditText();
    }

    private void initEditText() {
        mEditText.setOnTouchListener(this);
        mEditText.setOnEditorActionListener(this);
        mEditText.setOnActionListener(this);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                populateSearchSuggestions(charSequence.toString());
            }
        });
    }

    private void performSearch() {
        performSearch(mEditText.getText().toString());
    }

    private void performSearch(String query) {
        SearchSuggestionTable.insert(query);
        if (mOnSearchListener != null) {
            mOnSearchListener.onSearch(query);
        }
    }

    public void setOnSearchListener(OnSearchListener listener) {
        mOnSearchListener = listener;
    }

    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            hideKeyboard();
            focusSearch(false);
            performSearch();
            return true;
        }
        return false;
    }

    private void hideKeyboard() {
        if(mTextBoxHasFocus) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getWindowToken(), 0);
        }
    }

    private void populateSearchSuggestions(String query) {
        ArrayList<String> suggestions = SearchSuggestionTable.getSuggestions(query);
        mSuggestionAdapter.clear();
        mSuggestionAdapter.addAll(suggestions);
        mSuggestionAdapter.notifyDataSetChanged();
        updateContainerHeight();
    }

    private void updateContainerHeight() {
        mSuggestionContainer.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (mIsFirstTimeDrawing) {
                    mIsFirstTimeDrawing = false;
                    mContainerHeight = mSuggestionContainer.getHeight();
                    if(mSuggestionContainer.getChildCount() > 0) {
                        mSuggestionHeight = mSuggestionContainer.getChildAt(0).getHeight();
                    }
                    mSuggestionContainer.setVisibility(View.GONE);
                } else {
                    animateContainerHeight();
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    mSuggestionContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    mSuggestionContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    // todo: Limit to 7 rows
    private void animateContainerHeight() {
        int newHeight = mSuggestionAdapter.getCount() * mSuggestionHeight;
        if(mContainerHeight != newHeight) {
            expand(mSuggestionContainer, mContainerHeight, newHeight);
            mContainerHeight = newHeight;
        }
    }

    private void focusSearch(boolean hasFocus) {
        if (hasFocus) {
            mTextBoxHasFocus = true;
            mEditText.setCursorVisible(true);
            mEditText.setCanBackPress(true);
            showOverlay(true);
            showSuggestions(true);
            return;
        }
        mTextBoxHasFocus = false;
        mEditText.setCanBackPress(false);
        mEditText.setCursorVisible(false);
        showOverlay(false);
        showSuggestions(false);
    }

    private void showOverlay(boolean visible) {
        if(mOverlay == null) {
            return;
        }
        if(visible) {
            mOverlay.setFadeVisibility(VISIBLE);
            return;
        }
        mOverlay.setFadeVisibility(GONE);
    }

    private void showSuggestions(boolean visible) {
        mSuggestionContainer.setVisibility(VISIBLE);
        if (visible) {
            expand(mSuggestionContainer, 0, mContainerHeight);
            return;
        }
        expand(mSuggestionContainer, mContainerHeight, 0);
    }

    private void expand(final View v, int oldHeight, int height) {
        ValueAnimator va = ValueAnimator.ofInt(oldHeight, height);
        va.setDuration(100);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                v.getLayoutParams().height = value.intValue();
                v.requestLayout();
            }
        });
        va.start();
    }

    public void addOverlay(Overlay overlay) {
        mOverlay = overlay;
        mOverlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                focusSearch(false);
            }
        });
    }

    @Override
    public void onBackPress() {
        focusSearch(false);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP && !mTextBoxHasFocus) {
            focusSearch(true);
        }
        return false;
    }

    public interface OnSearchListener {
        public abstract void onSearch(String query);
    }

    public class SuggestionAdapter extends ArrayAdapter<String> {

        public SuggestionAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;

            if (view == null) {
                LayoutInflater layoutInflater;
                layoutInflater = LayoutInflater.from(getContext());
                view = layoutInflater.inflate(R.layout.item_search_suggestion, null);
            }

            String suggestion = getItem(position);
            TextView suggestionTextView = (TextView) view.findViewById(R.id.suggestion_text);
            suggestionTextView.setText(suggestion);
            suggestionTextView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    TextView textView = (TextView) view;
                    hideKeyboard();
                    focusSearch(false);
                    performSearch(textView.getText().toString());
                }
            });
            return view;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }
}