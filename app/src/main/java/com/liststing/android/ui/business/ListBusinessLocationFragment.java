package com.liststing.android.ui.business;

import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.airbnb.android.airmapview.AirMapMarker;
import com.airbnb.android.airmapview.AirMapView;
import com.airbnb.android.airmapview.listeners.OnMapInitializedListener;
import com.android.i18n.addressinput.AddressWidget;
import com.google.i18n.addressinput.common.AddressProblemType;
import com.google.i18n.addressinput.common.AddressProblems;
import com.liststing.android.R;
import com.liststing.android.models.Business;
import com.liststing.android.models.Industry;
import com.liststing.android.models.QCAddress;
import com.liststing.android.models.Service;
import com.liststing.android.util.ClientAddressWidgetProvider;
import com.liststing.android.util.LocationUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.i18n.addressinput.common.AddressData;
import com.google.i18n.addressinput.common.AddressField;
import com.google.i18n.addressinput.common.FormOptions;
import com.google.i18n.addressinput.common.SimpleClientCacheManager;

import java.util.ArrayList;
import java.util.Map;

import static com.google.i18n.addressinput.common.AddressField.*;

public class ListBusinessLocationFragment extends Fragment implements AdapterView.OnItemSelectedListener, OnMapInitializedListener, AddressWidget.AddressFieldEditTextChanged {

    private static final String SAVED_STATE_ADDRESS = "SAVED_STATE_ADDRESS";
    private static final String ARG_INDUSTRY = "ARG_INDUSTRY";
    private static final String ARG_SAVED_SERVICES = "ARG_SERVICES";
    private static final String ARG_BUSINESS = "ARG_BUSINESS";

    private LinearLayout addressWidgetContainer;
    private Location mLocation;
    private AirMapView mapView;
    private AddressWidget mAddressWidget;
    private Button mNextButton;
    private Industry mIndustry;
    private ArrayList<Service> mServices;
    private Bundle mBundle;
    private Business mBusiness;
    private boolean mIsEdittingBusiness = false;

    public static ListBusinessLocationFragment newInstance(Industry industry, ArrayList<Service> checkedServices, Business mBusiness) {
        ListBusinessLocationFragment listBusinessLocationFragment = new ListBusinessLocationFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_INDUSTRY, industry);
        args.putParcelableArrayList(ARG_SAVED_SERVICES, checkedServices);
        args.putParcelable(ARG_BUSINESS, mBusiness);
        listBusinessLocationFragment.setArguments(args);

        return listBusinessLocationFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBundle = savedInstanceState;
        mIndustry = getArguments().getParcelable(ARG_INDUSTRY);
        mServices = getArguments().getParcelableArrayList(ARG_SAVED_SERVICES);
        mBusiness = getArguments().getParcelable(ARG_BUSINESS);
        if(mBusiness != null) {
            mIsEdittingBusiness = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list_business_location, container, false);
        addressWidgetContainer = (LinearLayout) rootView.findViewById(R.id.container);
        mapView = (AirMapView) rootView.findViewById(R.id.map_view);

        mNextButton = (Button) rootView.findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fieldsValid()) {
                    moveToFinalStepsFrag(mLocation, mAddressWidget.getAddressData().toString());
                }
            }
        });

        setActionBarTitle();
        getLocation();
        initMap();
        return rootView;
    }

    private void initMap() {
        mapView.initialize(getChildFragmentManager());
        mapView.setOnMapInitializedListener(this);
    }

    private void setActionBarTitle() {
        if(!(getActivity() instanceof ListBusinessActivity)) {
            return;
        }
        ((ListBusinessActivity) getActivity()).getSupportActionBar()
                .setTitle(R.string.location);
    }

    public void getLocation() {
        LocationUtils locationUtils = new LocationUtils(getActivity());
        mLocation = locationUtils.getLastLocation();
        mAddressWidget = buildAddressWidget(buildFormOptions(), getSavedAddress(mBundle));
    }

    private AddressData getSavedAddress(Bundle savedInstanceState) {
        // hit back on final steps fragment
        if(mAddressWidget != null) {
            return mAddressWidget.getAddressData();
        }
        // Editing business
        if(mIsEdittingBusiness) {
            return LocationUtils.parseAddressData(mBusiness);
        }

        // first time
        if(savedInstanceState == null && mLocation != null) {
            return null;
        }

        // orientation change
        if(savedInstanceState != null) {
            QCAddress savedAddress = savedInstanceState.getParcelable(SAVED_STATE_ADDRESS);
            return AddressData.builder()
                    .setCountry(savedAddress.getPostalCountry())
                    .setAdminArea(savedAddress.getAdministrativeArea())
                    .setAddressLine1(savedAddress.getAddressLine1())
                    .setAddressLine2(savedAddress.getAddressLine2())
                    .setDependentLocality(savedAddress.getDependentLocality())
                    .setLocality(savedAddress.getLocality())
                    .setSortingCode(savedAddress.getSortingCode())
                    .setPostalCode(savedAddress.getPostalCode())
                    .build();
        }
        return null;
    }

    private FormOptions buildFormOptions() {
        FormOptions formOptions = new FormOptions();
        formOptions.setHidden(RECIPIENT);
        formOptions.setHidden(ORGANIZATION);
        return formOptions;
    }

    private boolean fieldsValid() {
        AddressProblems problems = mAddressWidget.getAddressProblems();
        for(Map.Entry<AddressField, AddressProblemType> entry : problems.getProblems().entrySet()) {
            mAddressWidget.displayErrorMessageForField(mAddressWidget.getAddressData(), entry.getKey(), entry.getValue());
            return false;
        }
        return true;
    }

    private AddressWidget buildAddressWidget(FormOptions formOptions, AddressData savedAddress) {
        if (savedAddress != null) {
            return new AddressWidget(getActivity(), addressWidgetContainer, formOptions,
                    new SimpleClientCacheManager(),savedAddress,
                    new ClientAddressWidgetProvider(getActivity()), this, this);
        }

        return new AddressWidget(getActivity(), addressWidgetContainer, formOptions,
                new SimpleClientCacheManager(),
                new ClientAddressWidgetProvider(getActivity()), this, this);
    }

    private int getMapZoomBasedOnField(AddressField addressField) {
        switch (addressField) {
            case COUNTRY:
            default:
                return 3;
            case ADMIN_AREA:
                return 5;
            case LOCALITY:
                return 8;
            case DEPENDENT_LOCALITY:
                return 12;
            case ADDRESS_LINE_1:
            case ADDRESS_LINE_2:
            case STREET_ADDRESS:
            case POSTAL_CODE:
                return 14;
        }
    }

    private void animateToAddress(AddressField field) {
        AddressData addressData = mAddressWidget.getAddressData();
        Address address = LocationUtils.getAddress(LocationUtils.buildLocationName(addressData), getActivity());

        if (mapView.isInitialized() && address != null) {
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mapView.animateCenterZoom(latLng, getMapZoomBasedOnField(field));
        }
    }

    private void addMapMarker() {
        AddressData addressData = mAddressWidget.getAddressData();
        Address address = LocationUtils.getAddress(LocationUtils.buildLocationName(addressData), getActivity());

        if (mapView.isInitialized() && address != null) {
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mapView.clearMarkers();
            mapView.addMarker(new AirMapMarker(latLng, 0));
        }
    }

    private void moveToFinalStepsFrag(Location location, String addressData) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right,  R.anim.exit_to_left);
        transaction.replace(R.id.details_fragment_container, ListBusinessFinalStepsFragment.newInstance(mIndustry, mServices, location, addressData, mBusiness));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list_business_location, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_skip:
                moveToFinalStepsFrag(mLocation, null);
                return true;
        }

        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_STATE_ADDRESS, new QCAddress(mAddressWidget.getAddressData()));
    }

    @Override
    public void onMapInitialized() {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        AddressWidget.AddressSpinnerInfo spinnerInfo = mAddressWidget.findSpinnerByView(parent);
        if (spinnerInfo == null) {
            return;
        }
        mAddressWidget.updateChildNodes(parent, position);
        animateToAddress(spinnerInfo.getAddressField());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onEditTextChanged(AddressField field, String value) {
        animateToAddress(field);
        addMapMarker();
    }
}
