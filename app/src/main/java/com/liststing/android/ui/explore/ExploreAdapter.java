package com.liststing.android.ui.explore;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.datasets.IndustryCollection;
import com.liststing.android.datasets.IndustryTable;
import com.liststing.android.models.Industry;
import com.liststing.android.ui.business.BusinessListActivity;
import com.liststing.android.ui.widget.BitmapNetworkImageView;
import com.liststing.android.util.RecordedLog;

/**
 * Hosts the items within the Explore RecyclerView
 */
public class ExploreAdapter extends ArrayAdapter<Industry> {

    private final ListView mListView;

    public ExploreAdapter(Context context, @LayoutRes int id, ListView listView) {
        super(context, id);
        mListView = listView;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater;
            layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.item_explore_industries, null);
        }

        final Industry industry = getItem(position);
        if (industry != null) {
            TextView industryName = (TextView) view.findViewById(R.id.industry_name);
            BitmapNetworkImageView industryImage = (BitmapNetworkImageView) view.findViewById(R.id.industry_image);

            industryName.setText(industry.getName());
            industryImage.setImageUrl(ListSting.API_ENDPOINT + industry.getImage(), ListSting.getImageLoader());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    moveToListBusinessActivity(industry);
                }
            });
        }

        return view;
    }

    private void moveToListBusinessActivity(Industry industry) {
        Intent intent = new Intent(getContext(), BusinessListActivity.class);
        intent.putExtra(BusinessListActivity.ARG_INDUSTRY, industry);
        getContext().startActivity(intent);
    }

    private void loadIndustries() {
        if (mIsTaskRunning) {
            RecordedLog.w(RecordedLog.Tag.EXPLORE, "adapter > industries are already in the process of being loaded");
            return;
        }
        new LoadIndustriesTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void clearIndustries() {
        if (!isEmpty()) {
            clear();
            notifyDataSetChanged();
        }
    }

    public void reload() {
        clearIndustries();
        loadIndustries();
    }

    public void refresh() {
        loadIndustries();
    }

    /**
     * background thread to load posts
     */
    private boolean mIsTaskRunning = false;

    private class LoadIndustriesTask extends AsyncTask<Void, Void, Boolean> {
        IndustryCollection posts;

        @Override
        protected void onPreExecute() {
            mIsTaskRunning = true;
        }

        @Override
        protected void onCancelled() {
            mIsTaskRunning = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            posts = IndustryTable.getIndustries();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                clear();
                addAll(posts);
            }

            mIsTaskRunning = false;
        }
    }
}
