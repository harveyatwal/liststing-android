package com.liststing.android.ui.feed;

import com.liststing.android.networking.ServerInterfaces;

/**
 * Feed-related events passed into EventBus
 */
public class FeedEvents {

    public static class UpdatePostsStarted {
        private final FeedPostService.Action mAction;
        public UpdatePostsStarted(FeedPostService.Action action) {
            mAction = action;
        }
        public FeedPostService.Action getAction() {
            return mAction;
        }
    }

    public static class UpdatePostsEnded {
        private final ServerInterfaces.ServerResult mResult;
        private final FeedPostService.Action mAction;
        public UpdatePostsEnded(ServerInterfaces.ServerResult result,
                                FeedPostService.Action action) {
            mResult = result;
            mAction = action;
        }
        public ServerInterfaces.ServerResult getResult() {
            return mResult;
        }
        public FeedPostService.Action getAction() {
            return mAction;
        }
    }
}
