package com.liststing.android.ui.business;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.models.Business;

public class BusinessDetailsFragment extends Fragment {
    private Business mBusiness;
    private boolean mIsDefaultBusiness = false;

    private TextView mDescription;
    private TextView mPhoneNumber;
    private TextView mEmail;
    private TextView mWebsite;

    private View mDescriptionContainer;
    private View mPhoneContainer;
    private View mEmailContainer;
    private View mWebsiteContainer;

    private ImageView mEditBusinessLink;
    private Button mAddDescriptionButton;
    private Button mAddPhoneButton;
    private Button mAddWebsiteButton;
    private Button mAddEmailButton;

    public static BusinessDetailsFragment newInstance(Business business) {
        BusinessDetailsFragment businessDetailsFragment = new BusinessDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(BusinessActivity.ARG_BUSINESS, business);
        businessDetailsFragment.setArguments(bundle);
        return businessDetailsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_business_details, container, false);

        mBusiness = getArguments().getParcelable(BusinessActivity.ARG_BUSINESS);
        if(Account.isBusinessProfileSignedIn() && Account.getDefaultAccount().getBusiness().getId() == mBusiness.getId()) {
            mIsDefaultBusiness = true;
            // if returning from BusinessEditDetailsFragment we want to update the business data
            mBusiness = Account.getDefaultAccount().getBusiness();
        }

        mDescription = (TextView) rootView.findViewById(R.id.business_description);
        mPhoneNumber = (TextView) rootView.findViewById(R.id.business_phone_number);
        mEmail = (TextView) rootView.findViewById(R.id.business_email);
        mWebsite = (TextView) rootView.findViewById(R.id.business_website);

        mDescriptionContainer =rootView.findViewById(R.id.description_container);
        mPhoneContainer = rootView.findViewById(R.id.phone_container);
        mEmailContainer = rootView.findViewById(R.id.email_container);
        mWebsiteContainer = rootView.findViewById(R.id.website_container);

        mEditBusinessLink = (ImageView) rootView.findViewById(R.id.business_edit_link);
        mAddDescriptionButton = (Button) rootView.findViewById(R.id.add_description);
        mAddWebsiteButton = (Button) rootView.findViewById(R.id.add_website);
        mAddPhoneButton = (Button) rootView.findViewById(R.id.add_phone_number);
        mAddEmailButton = (Button) rootView.findViewById(R.id.add_email);

        mEditBusinessLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToEditDetailsFragment();
            }
        });
        mAddDescriptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToEditDetailsFragment();
            }
        });
        mAddWebsiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToEditDetailsFragment();
            }
        });
        mAddPhoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToEditDetailsFragment();
            }
        });
        mAddEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToEditDetailsFragment();
            }
        });

        setActionBarTitle();
        populateDetails();
        hideEditDetails();
        setIntents();
        return rootView;
    }

    private void hideEditDetails() {
        if(!mIsDefaultBusiness) {
            mEditBusinessLink.setVisibility(View.GONE);
        }
    }

    private void setActionBarTitle() {
        if(!(getActivity() instanceof BusinessActivity)) {
            return;
        }
        BusinessActivity activity = ((BusinessActivity) getActivity());
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setHomeAsUpIndicator(0);
        activity.setActionBarTitle();
    }

    private void moveToEditDetailsFragment() {
        if(mIsDefaultBusiness) {
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            BusinessEditDetailsFragment businessEditDetailsFragment = BusinessEditDetailsFragment.newInstance();
            transaction.addToBackStack("");
            transaction.replace(R.id.details_fragment_container, businessEditDetailsFragment);
            transaction.addToBackStack(null);

            transaction.commit();
        }
    }

    private void populateDetails() {
        populateField(mBusiness.getDescription(), mDescription, mDescriptionContainer, mAddDescriptionButton);
        populateField(mBusiness.getEmail(), mEmail, mEmailContainer, mAddEmailButton);
        populateField(mBusiness.getWebsite(), mWebsite, mWebsiteContainer, mAddWebsiteButton);
        populateField(formatPhoneNumber(), mPhoneNumber, mPhoneContainer, mAddPhoneButton);

        if(mEmailContainer.getVisibility() == View.GONE &&
                mPhoneContainer.getVisibility() == View.GONE &&
                mWebsiteContainer.getVisibility() == View.GONE &&
                mDescriptionContainer.getVisibility() == View.GONE) {
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        }
    }
    private void populateField(String data, TextView fieldView, View container, Button addOptionView) {
        boolean isEmpty = data.isEmpty();
        if(isEmpty && mIsDefaultBusiness) {
            fieldView.setVisibility(View.GONE);
            addOptionView.setVisibility(View.VISIBLE);
            return;
        }
        if(isEmpty && container != null) {
            container.setVisibility(View.GONE);
        }
        fieldView.setText(data);
    }

    private String formatPhoneNumber() {
        String phoneNumber = mBusiness.getPhoneNumber();
        return PhoneNumberUtils.formatNumber(phoneNumber);
    }

    private void setIntents() {
        mPhoneContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                Uri phone = Uri.parse(String.format("tel:%s", mBusiness.getPhoneNumber()));
                intent.setData(phone);
                startActivity(intent);
            }
        });

        mEmailContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", mBusiness.getEmail(), null));
                startActivity(intent);
            }
        });

        mWebsiteContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri website = Uri.parse(mBusiness.getWebsite());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(website);
                startActivity(intent);
            }
        });
    }
}
