package com.liststing.android.ui.chat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.models.ChatMessage;
import com.liststing.android.networking.RestClient;
import com.liststing.android.networking.extensions.ImageExtension;
import com.liststing.android.networking.extensions.ShowBusinessProfileExtension;
import com.liststing.android.networking.extensions.UserExtension;
import com.liststing.android.networking.services.ChatService;
import com.liststing.android.ui.widget.AvatarImageView;
import com.liststing.android.util.RecordedLog;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.nick.packet.Nick;
import org.json.JSONObject;

public class ChatActivity extends AppCompatActivity implements ChatMessageListener, ServiceConnection, ChatService.ChatCreatedListener {

    public static final String ACCOUNT_ENDPOINT = "user/";

    public static final String ARG_PARTICIPANT_IS_BUSINESS = "arg_participant_is_business";
    public static final String ARG_PARTICIPANT_USER_ID = "arg_participant_user_id";
    public static final String ARG_PARTICIPANT_JABBER_ID = "arg_jabber_id";

    private ChatService.ChatController mController;

    private ListView mMessageListView;
    private ChatArrayAdapter mChatArrayAdapter;
    private ImageButton mSendButton;
    private EditText mMessageEditText;

    private Chat mChat;
    private String mParticipantJabberID;
    private long mParticipantUserID;
    private Account mParticipant;
    private boolean mIsParticipantBusiness;
    private String mParticipantDisplayName;
    private AvatarImageView mParticipantAvatar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            mIsParticipantBusiness = extras.getBoolean(ARG_PARTICIPANT_IS_BUSINESS);
            mParticipantJabberID = extras.getString(ARG_PARTICIPANT_JABBER_ID);
            mParticipantUserID = extras.getLong(ARG_PARTICIPANT_USER_ID);
        }

        setUpChatService();
        setupSendButton();
        setupChatContainer();
        setupActionBar();
        getParticipant();
    }

    private void getParticipant() {
        String endpoint = String.format("%s%s/", ACCOUNT_ENDPOINT, String.valueOf(mParticipantUserID));
        RecordedLog.d(RecordedLog.Tag.CHAT, "Retreving chat participant");

        RestClient.Listener listener = new RestClient.Listener() {
            @Override
            public void onResponse(JSONObject response) {
                mParticipant = new Account();
                mParticipant.updateFromJsonObject(response);
                mSendButton.setEnabled(true);
                mMessageEditText.setEnabled(true);
                updateActionBar();
            }
        };

        RestClient.ErrorListener errorListener = new RestClient.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RecordedLog.e(RecordedLog.Tag.CHAT, "Volley error \n + " + error);
            }
        };
        ListSting.getRestClient().get(endpoint, null, null, listener, errorListener);
    }

    private void updateActionBar() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View avatarActionBar = inflater.inflate(R.layout.actionbar_avatar, null);
        mParticipantAvatar = (AvatarImageView) avatarActionBar.findViewById(R.id.participant_avatar);
        TextView particpantName = (TextView) avatarActionBar.findViewById(R.id.participant_name);

        updateParticipantView();
        getSupportActionBar().setCustomView(avatarActionBar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        particpantName.setText(mParticipantDisplayName);
    }

    private void updateParticipantView() {
        if(mIsParticipantBusiness) {
            actionBarUpdate(mParticipant.getBusiness().getBusinessName(), mParticipant.getBusiness().getAvatarUrl());
            return;
        }
        actionBarUpdate(mParticipant.getUsername(), mParticipant.getAvatarUrl());
    }

    private void actionBarUpdate(String businessName, String imageURL) {
        mParticipantDisplayName = businessName;
        if(imageURL.equals("null")) {
            mParticipantAvatar.setVisibility(View.GONE);
            return;
        }
        mParticipantAvatar.setImageUrl(ListSting.API_ENDPOINT + imageURL, ListSting.getImageLoader());
    }

    private void setUpChatService() {
        Intent intent = new Intent(this, ChatService.class);
        startService(intent);
        bindService(intent, this, BIND_AUTO_CREATE);
    }

    private void setupSendButton() {
        mMessageEditText = (EditText) findViewById(R.id.message);
        mSendButton = (ImageButton) findViewById(R.id.send_message);
        mMessageEditText.setEnabled(true);
        mSendButton.setEnabled(true);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
    }

    private void sendMessage() {
        String body = mMessageEditText.getText().toString();
        try {
            Account account = Account.getDefaultAccount();
            Message msg = new Message();
            msg.setType(Message.Type.chat);
            msg.setBody(body);

            msg.addExtension(new Nick(getNickName()));
            msg.addExtension(new UserExtension(account.getId()));
            msg.addExtension(new ShowBusinessProfileExtension(shouldParticipantUseBusinessProfile()));
            msg.addExtension(new ImageExtension(getAvatarImageURL()));

            mChat.sendMessage(msg);

        } catch (SmackException.NotConnectedException e) {
            RecordedLog.e(RecordedLog.Tag.CHAT, e);
        }

        mChatArrayAdapter.add(new ChatMessage(false, body, ""));
        mMessageEditText.setText("");
    }

    /**
     * Participant will continue to use a business profile for future notifications, if the
     * current user is not messaging as a business
     * @return
     */
    private boolean shouldParticipantUseBusinessProfile() {
        if(mIsParticipantBusiness) {
            return false;
        }
        return isBusinessMessaging();
    }

    private String getNickName() {
        if(isBusinessMessaging()) {
            return Account.getDefaultAccount().getBusiness().getBusinessName();
        }
        return Account.getDefaultAccount().getUsername();
    }

    private String getAvatarImageURL() {
        if(isBusinessMessaging()) {
            return Account.getDefaultAccount().getBusiness().getAvatarUrl();
        }
        return Account.getDefaultAccount().getAvatarUrl();
    }


    private boolean isBusinessMessaging() {
        if(Account.isBusinessProfileSignedIn()) {
            // return false if messaging a user
            return !mIsParticipantBusiness;
        }
        return false;
    }

    private void setupChatContainer() {
        mChatArrayAdapter = new ChatArrayAdapter(this, R.layout.item_chat_right_message);
        mMessageListView = (ListView) findViewById(R.id.message_container);
        mMessageListView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        mMessageListView.setAdapter(mChatArrayAdapter);

        // Scrolls the list view to bottom on data change
        mChatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                mMessageListView.setSelection(mChatArrayAdapter.getCount() - 1);
            }
        });
    }


    private void setupActionBar() {
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_business_favorite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processMessage(Chat chat, final Message message) {
        final String msgBody = message.getBody();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mChatArrayAdapter.add(new ChatMessage(true, msgBody, mParticipantDisplayName));
            }
        });
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        mController = ((ChatService.ChatController) iBinder);
        mController.clearNotification();
        mController.openChat(mParticipantJabberID, this);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }

    @Override
    protected void onResume() {
        if(mController != null) {
            mController.stopNotifications(mParticipantJabberID);
            mController.clearNotification();
        }
        super.onResume();
    }
    @Override
    protected void onPause() {
        if(mController != null) {
            mController.registerNotifications();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if(!isChangingConfigurations() && mController != null) {
            mController.closeChat(mChat);
        }
        unbindService(this);
        super.onDestroy();
    }

    @Override
    public void chatInitialized(Chat chat) {
        mChat = chat;
        mChat.addMessageListener(this);
    }

    class ChatArrayAdapter extends ArrayAdapter<ChatMessage> {
        public ChatArrayAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            ChatMessage msg = getItem(position);
            View row = convertView;

            if(msg.isIncoming()) {
                row = inflater.inflate(R.layout.item_chat_left_message, parent, false);
                TextView authorText = (TextView) row.findViewById(R.id.author);
                authorText.setText(msg.getAuthor());
            } else {
                row = inflater.inflate(R.layout.item_chat_right_message, parent, false);
            }
            TextView chatText = (TextView) row.findViewById(R.id.message);
            chatText.setText(msg.getMessage());
            TextView timestampText = (TextView) row.findViewById(R.id.timestamp);
            timestampText.setText(msg.getTimestamp());
            return row;
        }
    }
}