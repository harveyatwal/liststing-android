package com.liststing.android.ui.feed.views;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.liststing.android.ListSting;

public class FeedRecyclerView extends RecyclerView {

    public FeedRecyclerView(Context context) {
        super(context);
    }

    public FeedRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FeedRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    // RecyclerView prematurely returns false
    // Need to ensure that recyclerview is scrolled to top
    // http://stackoverflow.com/a/25227797/1673548
    public boolean canScrollUp() {
        return super.canScrollVertically(-1) || (getChildAt(0) != null && getChildAt(0).getTop() < 0);
    }

    /**
     * Custom item decoration needed to set padding within recyclerview items
     */
    public static class FeedItemDecoration extends RecyclerView.ItemDecoration {
        private final int mHorizontalPadding;
        private final int mVerticalPadding;

        public FeedItemDecoration(@DimenRes int horizontalPadding, @DimenRes int verticalPadding) {
            super();
            mHorizontalPadding = ListSting.getContext(). getResources().getDimensionPixelSize(horizontalPadding);
            mVerticalPadding = ListSting.getContext(). getResources().getDimensionPixelSize(verticalPadding);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            outRect.set(mHorizontalPadding, // left
                    0,                      // top
                    mHorizontalPadding,     // right
                    mVerticalPadding);      // bottom
        }
    }
}
