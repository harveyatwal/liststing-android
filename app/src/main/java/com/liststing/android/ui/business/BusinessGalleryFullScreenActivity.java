package com.liststing.android.ui.business;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

import com.android.volley.toolbox.NetworkImageView;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.datasets.BusinessMediaCollection;
import com.liststing.android.models.Business;

public class BusinessGalleryFullScreenActivity extends AppCompatActivity {

    public static final String ARG_BUSINESS = "arg_business";
    public static final String ARG_POSITION = "arg_posiion";
    private Business mBusiness;
    private int mPosition;
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_gallery_fullscreen);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            mBusiness = extras.getParcelable(ARG_BUSINESS);
            mPosition = extras.getInt(ARG_POSITION, 0);
        }

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(new FullScreenImageAdapter(this, mBusiness.getPhotos()));
        mViewPager.setCurrentItem(mPosition);
        setupActionBar();
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class FullScreenImageAdapter extends PagerAdapter {
        private Context mContext;
        private final BusinessMediaCollection mPhotos;

        public FullScreenImageAdapter(Context context, BusinessMediaCollection photos) {
            this.mContext = context;
            this.mPhotos = photos;
        }

        @Override
        public int getCount() {
            return mPhotos.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.item_list_fullscreen_image, container, false);

            NetworkImageView image = (NetworkImageView) itemView.findViewById(R.id.image);
            String imageUrl = mPhotos.get(position).getImage();
            image.setImageUrl(ListSting.API_ENDPOINT + imageUrl, ListSting.getImageLoader());

            container.addView(itemView);

            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}
