package com.liststing.android.ui.business;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.NetworkImageView;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.datasets.BusinessMediaCollection;
import com.liststing.android.models.BusinessMedia;

public class BusinessMediaCarouselAdapter extends
        RecyclerView.Adapter<BusinessMediaCarouselAdapter.ViewHolder> {

    private BusinessMediaCollection mPhotos;

    public BusinessMediaCarouselAdapter(BusinessMediaCollection photos) {
        mPhotos = photos;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public NetworkImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (NetworkImageView) itemView.findViewById(R.id.image);
        }
    }

    @Override
    public BusinessMediaCarouselAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_list_photo_carousel, parent, false);
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BusinessMediaCarouselAdapter.ViewHolder viewHolder, int position) {
        BusinessMedia photo =  mPhotos.get(position);
        viewHolder.image.setImageUrl(ListSting.API_ENDPOINT + photo.getImage(), ListSting.getImageLoader());
    }

    public void setPhotos(BusinessMediaCollection photos) {
        this.mPhotos = photos;
    }

    @Override
    public int getItemCount() {
        return mPhotos.size();
    }
}