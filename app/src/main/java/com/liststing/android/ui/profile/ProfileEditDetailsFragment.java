package com.liststing.android.ui.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.networking.events.AccountEvents;
import com.liststing.android.networking.MultipartRequest;
import com.liststing.android.networking.RestClient;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.greenrobot.event.EventBus;

public class ProfileEditDetailsFragment extends Fragment {

    private static final String UPDATE_ACCOUNT_ENDPOINT = "/user/current/update/";

    public static ProfileEditDetailsFragment newInstance() {
        return new ProfileEditDetailsFragment();
    }

    private TextView mFinishLink;
    private Account mProfile;

    private EditText mFirstName;
    private EditText mLastName;
    private EditText mEmailAddress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_profile_edit_details, container, false);
        setHasOptionsMenu(true);

        mFinishLink = (TextView) rootView.findViewById(R.id.profile_edit_link);
        mFirstName = (EditText) rootView.findViewById(R.id.profile_first_name);
        mLastName = (EditText) rootView.findViewById(R.id.profile_last_name);
        mEmailAddress = (EditText) rootView.findViewById(R.id.profile_email_address);

        mFinishLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishEditting();
            }
        });
        setActionBar();
        prefillInputs();
        return rootView;
    }

    private void finishEditting() {
        final Map<String, String> params = new HashMap<>();
        params.put("first_name", mFirstName.getText().toString());
        params.put("last_name", mLastName.getText().toString());
        params.put("email", mEmailAddress.getText().toString());
        enableInputs(false);

        new Thread() {
            @Override
            public void run() {
                MultipartRequest updateRequest = new MultipartRequest(UPDATE_ACCOUNT_ENDPOINT, params, null, new RestClient.Listener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Account.getDefaultAccount().updateFromJsonObject(response);
                        enableInputs(true);
                        finishAccountUpdate();
                    }
                }, new RestClient.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        enableInputs(true);
                        RecordedLog.e(RecordedLog.Tag.API, "Updating Account > " + error);

                        //TODO: Create SnackBar error message
                    }
                });
                ListSting.getRequestQueue().add(updateRequest);
            }
        }.start();
    }

    private void finishAccountUpdate() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getActivity().getSupportFragmentManager().popBackStack();
                EventBus.getDefault().post(new AccountEvents.AccountUpdated());
            }
        });
    }

    private void enableInputs(boolean isEnabled) {
        mFirstName.setEnabled(isEnabled);
        mLastName.setEnabled(isEnabled);
        mEmailAddress.setEnabled(isEnabled);
    }

    private void prefillInputs() {
        mProfile = Account.getDefaultAccount();
        mFirstName.setText(mProfile.getFirstName());
        mLastName.setText(mProfile.getLastName());
        mEmailAddress.setText(mProfile.getEmail());
    }

    private void setActionBar() {
        if(!(getActivity() instanceof ProfileActivity)) {
            return;
        }

        ActionBar actionBar = ((ProfileActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_clear_white_24dp);
        actionBar.setTitle(getString(R.string.edit_profile));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_finish:
                finishEditting();
                return true;
            case android.R.id.home:
                getActivity().getSupportFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
