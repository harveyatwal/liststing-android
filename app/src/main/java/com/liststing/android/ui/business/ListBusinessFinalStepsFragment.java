package com.liststing.android.ui.business;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.models.Business;
import com.liststing.android.models.Industry;
import com.liststing.android.models.Service;
import com.liststing.android.networking.RestClient;
import com.liststing.android.ui.SignInActivity;
import com.liststing.android.util.NetworkUtils;
import com.liststing.android.util.RecordedLog;
import com.liststing.android.util.TextUtils;
import com.liststing.android.util.ValidationUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListBusinessFinalStepsFragment extends Fragment {
    private static final String NEW_BUSINESS_ENDPOINT = "/businesses/new/";
    private static final String UPDATE_BUSINESS_ENDPOINT = "/business/current/update/";

    private static final String ARG_INDUSTRY = "ARG_INDUSTRY";
    private static final String ARG_SAVED_SERVICES = "ARG_SERVICES";
    private static final String ARG_LOCATION = "ARG_LOCATION";
    private static final String ARG_ADDRESS = "ARG_ADDRESS_DATA";
    private static final String ARG_BUSINESS = "ARG_BUSINESS";

    private String mAddressData;
    private Location mLocation;
    private ArrayList<Service> mServices;
    private Industry mIndustry;
    private Button mListButton;
    private EditText mBusinessName;
    private EditText mEmail;
    private EditText mCountryCode;
    private EditText mPhoneNumber;
    private EditText mWebsite;
    private EditText mDescription;
    private boolean mSkippedLocation = false;
    private Business mBusiness;
    private boolean mIsEdittingBusiness = false;

    public static ListBusinessFinalStepsFragment newInstance(Industry industry, ArrayList<Service> checkedServices,
                                                             Location location, String addressData, Business business) {
        ListBusinessFinalStepsFragment listBusinessFinalStepsFragment = new ListBusinessFinalStepsFragment();

        Bundle args = new Bundle();
        args.putParcelable(ARG_INDUSTRY, industry);
        args.putParcelableArrayList(ARG_SAVED_SERVICES, checkedServices);
        args.putParcelable(ARG_LOCATION, location);
        args.putString(ARG_ADDRESS, addressData);
        args.putParcelable(ARG_BUSINESS, business);
        listBusinessFinalStepsFragment.setArguments(args);

        return listBusinessFinalStepsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIndustry = getArguments().getParcelable(ARG_INDUSTRY);
        mServices = getArguments().getParcelableArrayList(ARG_SAVED_SERVICES);
        mLocation = getArguments().getParcelable(ARG_LOCATION);
        mBusiness = getArguments().getParcelable(ARG_BUSINESS);
        mAddressData = getArguments().getString(ARG_ADDRESS);
        if(mAddressData == null) {
            mSkippedLocation = true;
        }
        if(mBusiness != null) {
            mIsEdittingBusiness = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list_business_final_steps, container, false);

        mBusinessName = (EditText) rootView.findViewById(R.id.business_name);
        mEmail = (EditText) rootView.findViewById(R.id.email);
        mCountryCode = (EditText) rootView.findViewById(R.id.country_code);
        mPhoneNumber = (EditText) rootView.findViewById(R.id.phone_number);
        mWebsite = (EditText) rootView.findViewById(R.id.website);
        mDescription = (EditText) rootView.findViewById(R.id.description);
        mListButton = (Button) rootView.findViewById(R.id.list_button);
        mListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listBusiness();
            }
        });

        autoFillData();
        return rootView;
    }

    private void autoFillData() {
        if(mIsEdittingBusiness) {
            mBusinessName.setText(mBusiness.getBusinessName());
            mEmail.setText(mBusiness.getEmail());
            mWebsite.setText(mBusiness.getWebsite());
            mDescription.setText(mBusiness.getDescription());
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            try {
                Phonenumber.PhoneNumber number = phoneUtil.parse("+" + mBusiness.getPhoneNumber(), "");
                mCountryCode.setText(String.valueOf(number.getCountryCode()));
                mPhoneNumber.setText(String.valueOf(number.getNationalNumber()));
            } catch (NumberParseException e) {
                RecordedLog.e(RecordedLog.Tag.LIST_BUSINESS, "Country Code > " + e.toString());
            }
        }
    }

    private void listBusiness() {
        if(!isFieldsValid()) {
           return;
        }
        if(!hasNetworkConnectivity()) {
            return;
        }
        HashMap<String, Object> params = createBusinessParams();
        if(!Account.isSignedIn()) {
            moveToSignInPage(params);
            return;
        }
        enableInputs(false);
        registerBusiness(params);
    }

    private HashMap<String, Object> createBusinessParams() {
        final String industryId = String.valueOf(mIndustry.getId());
        final String businessName = TextUtils.getText(mBusinessName).trim();
        final String countryCode = TextUtils.getText(mCountryCode).trim();
        final String phoneNumber = TextUtils.getText(mPhoneNumber).trim();
        final String email = TextUtils.getText(mEmail).trim();
        final String website = TextUtils.getText(mWebsite).trim();
        final String description = TextUtils.getText(mDescription).trim();
        final String fullPhoneNumber = countryCode + phoneNumber;

        final HashMap<String, Object> params = new HashMap<>();
        params.put("admin", String.valueOf(Account.getDefaultAccount().getId()));
        params.put("business_name", businessName);
        params.put("industry", industryId);
        params.put("description", description);
        params.put("phone_number", phoneNumber.length() == 0 ? "" : fullPhoneNumber);
        params.put("email", email);
        params.put("website", website);
        if(mSkippedLocation) {
            params.put("show_location", "false");
        } else {
            params.put("address_data", mAddressData);
        }
        ArrayList<String> serviceIds = new ArrayList<>();
        for(Service service : mServices) {
            serviceIds.add(String.valueOf(service.getId()));
        }
        params.put("services", serviceIds);
        if(mLocation != null) {
            params.put("longitude", String.valueOf(mLocation.getLongitude()));
            params.put("latitude", String.valueOf(mLocation.getLatitude()));
        }
        return params;
    }

    private void moveToSignInPage(HashMap<String, Object> params) {
        Intent intent = new Intent(getActivity(), SignInActivity.class);
        intent.putExtra(SignInActivity.FINISH_ACTION, SignInActivity.REGISTER_BUSINESS_ACTIVITY);
        intent.putExtra(SignInActivity.REGISTER_BUSINESS, params);
        startActivity(intent);
        getActivity().finish();
    }

    private void registerBusiness(HashMap<String, Object> params) {
        String path = NEW_BUSINESS_ENDPOINT;
        if(mIsEdittingBusiness) {
            path = UPDATE_BUSINESS_ENDPOINT;
        }
        ListSting.getRestClient().post(path, params, null, new RestClient.Listener() {
            @Override
            public void onResponse(JSONObject response) {
                Account.getDefaultAccount().updateFromJsonObject(response);
                enableInputs(true);
                finishRegistration();
            }
        }, new RestClient.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                enableInputs(true);
                RecordedLog.e(RecordedLog.Tag.API, "Create Business > " + error);
            }
        });
    }

    private void enableInputs(boolean enabled) {
        mBusinessName.setEnabled(enabled);
        mCountryCode.setEnabled(enabled);
        mPhoneNumber.setEnabled(enabled);
        mEmail.setEnabled(enabled);
        mWebsite.setEnabled(enabled);
        mDescription.setEnabled(enabled);
    }

    private void finishRegistration() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent returnIntent = new Intent();
                getActivity().setResult(getActivity().RESULT_OK, returnIntent);
                getActivity().finish();
            }
        });
    }

    private boolean isFieldsValid() {
        return ValidationUtils.requiredField(getActivity(), mBusinessName) && isOptionalFieldsValid();
    }

    private boolean isOptionalFieldsValid() {
        if(!ValidationUtils.isValidEmailNoInputRequired(mEmail)) {
            mEmail.setError(getString(R.string.invalid_email));
            mEmail.requestFocus();
            return false;
        }

        if(!ValidationUtils.isDigitOnly(mCountryCode) && mCountryCode.length() > 0) {
            mCountryCode.setError(getString(R.string.invalid_phone_number_digit_only));
            mCountryCode.requestFocus();
            return false;
        }

        if(!ValidationUtils.isDigitOnly(mPhoneNumber) && mPhoneNumber.length() > 0) {
            mPhoneNumber.setError(getString(R.string.invalid_phone_number_digit_only));
            mPhoneNumber.requestFocus();
            return false;
        }

        if(!ValidationUtils.isValidPhoneNumber(mCountryCode, mPhoneNumber) && mPhoneNumber.length() > 0) {
            mPhoneNumber.setError(getString(R.string.invalid_phone_number));
            mPhoneNumber.requestFocus();
            return false;
        }

        return true;
    }

    private boolean hasNetworkConnectivity() {
        if (!NetworkUtils.isNetworkAvailable(getActivity())) {
            // Snackbar Error Message
            return false;
        }
        return true;
    }
}
