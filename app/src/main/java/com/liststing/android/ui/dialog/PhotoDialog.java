package com.liststing.android.ui.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.MediaStore;

import com.liststing.android.R;

public class PhotoDialog extends Dialog {

    public static final int GALLERY = 0;
    public static final int CAMERA = 1;
    private final Context mContext;

    private AlertDialog.Builder mBuilder;

    public PhotoDialog(Context context) {
        super(context);
        mContext = context;
        init();
    }

    private void init() {
        mBuilder = new AlertDialog.Builder(getContext());
        build();
    }

    private void build() {
        String[] addItems = getItems();
        mBuilder.setTitle(R.string.add_photo);
        mBuilder.setItems(addItems, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case CAMERA:
                        takePhoto();
                        break;
                    case GALLERY:
                    default:
                        pickPhoto();
                        break;
                }
            }
        });
        mBuilder.create();
        mBuilder.show();
    }

    private void takePhoto() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ((Activity) mContext).startActivityForResult(takePicture, CAMERA);
    }

    private void pickPhoto() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        ((Activity) mContext).startActivityForResult(pickPhoto, GALLERY);
    }

    private String[] getItems() {
        String[] mItems = new String[2];
        //todo: get list of camera applications installed
        mItems[GALLERY] = (getContext().getString(R.string.gallery));
        mItems[CAMERA] = (getContext().getString(R.string.camera));
        return mItems;
    }
}
