package com.liststing.android.ui.business;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.datasets.BusinessMediaCollection;
import com.liststing.android.models.Account;
import com.liststing.android.models.Business;
import com.liststing.android.networking.MultipartRequest;
import com.liststing.android.networking.RestClient;
import com.liststing.android.ui.widget.BitmapNetworkImageView;
import com.liststing.android.util.RecordedLog;

import org.json.JSONObject;


public class BusinessGalleryFragment extends Fragment {
    private static final String PHOTO_DELETE_BUSINESS_ENDPOINT = "/business/current/delete_photo/";

    private static final String ARG_BUSINESS = "arg_business";
    private GridView mGridView;
    private boolean mCanSelectItem = false;
    private BusinessMediaCollection mPhotos;
    private GalleryAdapter mGalleryAdapter;
    private Business mBusiness;

    public static BusinessGalleryFragment newInstance(Business business) {
        BusinessGalleryFragment businessGalleryFragment = new BusinessGalleryFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_BUSINESS, business);
        businessGalleryFragment.setArguments(bundle);
        return businessGalleryFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_business_gallery, container, false);

        mBusiness = getArguments().getParcelable(ARG_BUSINESS);
        mGridView = (GridView) rootView.findViewById(R.id.photo_gallery);
        mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        mGridView.setMultiChoiceModeListener(new MultiChoiceModeListener());
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if(!Account.isSignedIn() || Account.isCustomerProfileSignedIn() || Account.isShowingOthersBusiness(mBusiness)) {
                    viewGalleryFullScreen(position);
                }
                if (mCanSelectItem) {
                    togleSelectedItem(position);
                }
            }
        });
        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (Account.isShowingPersonalBusiness(mBusiness)) {
                    togleSelectedItem(position);
                    if (!mCanSelectItem) {
                        enableSelectItem(true);
                    }
                }
                return true;
            }
        });

        if(mBusiness.getPhotos() != null) {
            mPhotos = mBusiness.getPhotos();
            mGalleryAdapter = new GalleryAdapter(getActivity(), mPhotos);
            mGridView.setAdapter(mGalleryAdapter);
        }
        return rootView;
    }

    private void viewGalleryFullScreen(int position) {
        Intent intent = new Intent(getActivity(), BusinessGalleryFullScreenActivity.class);
        intent.putExtra(BusinessGalleryFullScreenActivity.ARG_BUSINESS, mBusiness);
        intent.putExtra(BusinessGalleryFullScreenActivity.ARG_POSITION, position);
        startActivity(intent);
    }

    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(Account.isShowingPersonalBusiness(mBusiness)) {
            inflater.inflate(R.menu.menu_business_gallery, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_select_item:
                enableSelectItem(true);
                return true;
            case R.id.action_delete:
                deleteSelectItems();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteSelectItems() {
        SparseBooleanArray checkedItems = mGridView.getCheckedItemPositions();
        int checkedCount = 0;
        for(int i = 0; i < checkedItems.size(); i++) {
            int originalPosition = checkedItems.keyAt(i);
            int newPosition = originalPosition - checkedCount;
            if(checkedItems.valueAt(i)) {
                // The position of the checked item changes as the loop iterates
                // To ensure the removal of the correct image, the variable checkedCount
                // is used along with the position
                mGridView.setItemChecked(originalPosition, false);
                deleteSelectedItem(newPosition);
                checkedCount ++;
            }
        }
    }

    private void deleteSelectedItem(final int position) {
        final String path = String.format("%s%d/",PHOTO_DELETE_BUSINESS_ENDPOINT, mPhotos.get(position).getId());
        mPhotos.remove(position);
        new Thread() {
            @Override
            public void run() {
                MultipartRequest imageRequest = new MultipartRequest(path, null, null,
                        new RestClient.Listener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Account.getDefaultAccount().updateFromJsonObject(response);
                                mGalleryAdapter.notifyDataSetChanged();
                            }
                        },
                        new RestClient.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                RecordedLog.e(RecordedLog.Tag.API, "Business > Photo Delete > " + error);
                                //TODO: Show error message
                            }
                        });
                ListSting.getRequestQueue().add(imageRequest);
            }
        }.start();
    }

    private void enableSelectItem(boolean isEnabled) {
        if(Account.isShowingPersonalBusiness(mBusiness)) {
            mCanSelectItem = isEnabled;
        }
    }

    private void highlightItem(int position, boolean checked) {
        View view = mGridView.getChildAt(position);
        if(view != null) {
            if (checked) {
                view.findViewById(R.id.highlight).setVisibility(View.VISIBLE);
                return;
            }
            view.findViewById(R.id.highlight).setVisibility(View.GONE);
        }
    }

    private void togleSelectedItem(int position) {
        if(!mGridView.isItemChecked(position)) {
            mGridView.setItemChecked(position, true);
        } else {
            mGridView.setItemChecked(position, false);
        }
    }

    ///////////////////////////////////////////////
    //
    // Populates each grid item with a image
    ///////////////////////////////////////////////
    class GalleryAdapter extends BaseAdapter {
        private Context mContext;
        private final BusinessMediaCollection mPhotos;

        public GalleryAdapter(Context context, BusinessMediaCollection photos) {
            this.mContext = context;
            this.mPhotos = photos;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = convertView;
            if (convertView == null) {
                view = inflater.inflate(R.layout.item_grid_gallery_photo, null);
            }

            BitmapNetworkImageView image = (BitmapNetworkImageView) view.findViewById(R.id.image);
            String imageUrl = mPhotos.get(position).getImage();
            image.setImageUrl(ListSting.API_ENDPOINT + imageUrl, ListSting.getImageLoader());

            if(mGridView.isItemChecked(position)) {
                view.findViewById(R.id.highlight).setVisibility(View.VISIBLE);
            }
            return view;
        }

        @Override
        public int getCount() {
            return mPhotos.size();
        }

        @Override
        public Object getItem(int position) {
            return mPhotos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    ///////////////////////////////////////////////
    //
    // MultiChoiceModeListener creates a ContextualActionBar when
    // an image is selected
    ///////////////////////////////////////////////
    public class MultiChoiceModeListener implements GridView.MultiChoiceModeListener {
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_gallery_select_photo, menu);

            mode.setTitle(getResources().getString(R.string.select_items));
            if(mGridView.getCheckedItemCount() == 0) {
                mode.setSubtitle(getResources().getString(R.string.one_item_selected));
            } else {
                updateSelectItemText(mode);
                enableSelectItem(true);
            }
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    deleteSelectItems();
                    reset();
                    return true;
            }
            return true;
        }

        public void onDestroyActionMode(ActionMode mode) {
            reset();
        }

        private void reset() {
            clearPhotos();
            enableSelectItem(false);
        }

        private void clearPhotos() {
            SparseBooleanArray checked = mGridView.getCheckedItemPositions();
            for(int i = 0; i < checked.size(); i++) {
                int key = checked.keyAt(i);
                if(checked.valueAt(i)) {
                    highlightItem(key, false);
                    mGridView.setItemChecked(key, false);
                }
            }
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long l, boolean checked) {
            updateSelectItemText(mode);
            highlightItem(position, checked);
        }

        private void updateSelectItemText(ActionMode mode) {
            int selectCount = mGridView.getCheckedItemCount();
            switch (selectCount) {
                case 1:
                    mode.setSubtitle(R.string.one_item_selected);
                    break;
                default:
                    mode.setSubtitle("" + selectCount + ' ' + getResources().getString(R.string.items_selected));
                    break;
            }
        }
    }
}