package com.liststing.android.ui.feed;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.liststing.android.R;
import com.liststing.android.datasets.FeedPostTable;
import com.liststing.android.datasets.PostCollection;
import com.liststing.android.models.Post;
import com.liststing.android.networking.ServerInterfaces;
import com.liststing.android.util.DateTimeUtils;
import com.liststing.android.util.RecordedLog;
import com.liststing.android.util.RecordedLog.Tag;

/**
 * Hosts the items within the FeedRecyclerView
 */
public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final PostCollection mFeedPosts = new PostCollection();
    private ServerInterfaces.DataLoadedListener mDataLoadedListener;
    private final Context mContext;

    public FeedAdapter(Context context) {
        super();
        mContext = context;
        setHasStableIds(true);
    }

    public void setOnDataLoadedListener(ServerInterfaces.DataLoadedListener listener) {
        mDataLoadedListener = listener;
    }

    private void loadPosts() {
        if (mIsTaskRunning) {
            RecordedLog.w(Tag.FEED, "adapter > posts are already in the process of being loaded");
            return;
        }
        new LoadPostsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void clear() {
        if (!isEmpty()) {
            //mPosts.clear();
            notifyDataSetChanged();
        }
    }

    public void refresh() {
        loadPosts();
    }

    private void reload() {
        clear();
        loadPosts();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootPostView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_feed_post, parent, false);
        return new FeedPostViewHolder(rootPostView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final FeedPostViewHolder postHolder = (FeedPostViewHolder) holder;
        final Post post = getItem(position);

        postHolder.txtVendorName.setText(post.getVendorName());
        postHolder.txtDate.setText(DateTimeUtils.dateToTimeSpan(post.getDatePublished()));

        if (post.hasMessage()) {
            postHolder.txtMessage.setVisibility(View.VISIBLE);
            postHolder.txtMessage.setText(post.getVendorMessage());
        } else {
            postHolder.txtMessage.setVisibility(View.GONE);
        }

        postHolder.txtEndorseCount.setText(post.getNumEndorsed());
    }

    private Post getItem(int position) {
        return mFeedPosts.get(position);
    }

    @Override
    public int getItemCount() {
        return mFeedPosts.size();
    }

    public boolean isEmpty() {
        return (mFeedPosts == null || mFeedPosts.size() == 0);
    }

    private class FeedPostViewHolder extends RecyclerView.ViewHolder {
        private final CardView cardView;

        private final TextView txtVendorName;
        private final TextView txtEndorseCount;
        private final TextView txtMessage;
        private final TextView txtDate;

        private final ImageView imgFeatured;
        private final ImageView imgAvatar;

        public FeedPostViewHolder(View rootPostView) {
            super(rootPostView);

            cardView = (CardView) rootPostView.findViewById(R.id.post_container);

            txtEndorseCount = (TextView) rootPostView.findViewById(R.id.post_endorse_count);
            txtVendorName = (TextView) rootPostView.findViewById(R.id.post_vendor_name);
            txtMessage = (TextView) rootPostView.findViewById(R.id.post_message);
            txtDate = (TextView) rootPostView.findViewById(R.id.post_date);

            imgAvatar = (ImageView) rootPostView.findViewById(R.id.post_vendor_avatar);
            imgFeatured = (ImageView) rootPostView.findViewById(R.id.post_image);
        }
    }

    /**
     * background thread to load posts
     */
    private boolean mIsTaskRunning = false;

    private class LoadPostsTask extends AsyncTask<Void, Void, Boolean> {
        PostCollection posts;

        @Override
        protected void onPreExecute() {
            mIsTaskRunning = true;
        }

        @Override
        protected void onCancelled() {
            mIsTaskRunning = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            posts = FeedPostTable.getPosts(FeedConstants.FEED_MAX_POSTS_TO_DISPLAY);
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                mFeedPosts.clear();
                mFeedPosts.addAll(posts);
                notifyDataSetChanged();
            }

            if (mDataLoadedListener != null) {
                mDataLoadedListener.onDataLoaded(isEmpty());
            }

            mIsTaskRunning = false;
        }
    }

}
