package com.liststing.android.ui.feed;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liststing.android.R;
import com.liststing.android.networking.ServerInterfaces;
import com.liststing.android.ui.feed.views.FeedRecyclerView;
import com.liststing.android.util.RecordedLog;
import com.liststing.android.util.RecordedLog.Tag;

/**
 * Contains posts from vendors
 */
public class FeedFragment extends Fragment {

    private static final String KEY_WAS_PAUSED = "was_paused";

    private RecyclerView mRecyclerView;
    private FeedAdapter mFeedAdapter;
    private View mEmptyView;

    private boolean mWasPaused;

    private final ServerInterfaces.DataLoadedListener mDataLoadedListener = new ServerInterfaces.DataLoadedListener() {
        @Override
        public void onDataLoaded(boolean isEmpty) {
        if (!isFragmentStillAttached()) {
            return;
        }
        if (isEmpty) {
            setEmptyMessage();
            return;
        }

        mEmptyView.setVisibility(View.GONE);
        }
    };

    public static FeedFragment newInstance() {
        return new FeedFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_feed, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.addItemDecoration(new FeedRecyclerView.FeedItemDecoration(
                R.dimen.content_margin, R.dimen.feed_post_item_vertical_gutter));

        // Empty Activity View
        mEmptyView = rootView.findViewById(R.id.empty_view);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        boolean hasFeedAdapter = hasFeedAdapter();
        mRecyclerView.setAdapter(getFeedAdapter());

        // check if the feed adapter did not exist - to prevent
        // posts from loading every time the user moves between
        // fragments
        if(!hasFeedAdapter) {
            refreshPosts();
        }
    }

    private FeedAdapter getFeedAdapter() {
        if (mFeedAdapter == null) {
            mFeedAdapter = new FeedAdapter(getActivity());
            mFeedAdapter.setOnDataLoadedListener(mDataLoadedListener);
        }
        return mFeedAdapter;
    }

    private boolean hasFeedAdapter() {
        return (mFeedAdapter != null);
    }

    private boolean isFeedAdapterEmpty() {
        return (mFeedAdapter == null || mFeedAdapter.isEmpty());
    }

    private boolean isFragmentStillAttached() {
        return isAdded();
    }

    private void setEmptyMessage() {
        if (!isFragmentStillAttached()) {
            return;
        }

        int titleResId = 0;
        int descriptionResId = 0;

        mEmptyView.setVisibility(View.VISIBLE);
    }

    private void refreshPosts() {
        if (hasFeedAdapter()) {
            getFeedAdapter().refresh();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        RecordedLog.d(Tag.FEED, "saving instance state");
        outState.putBoolean(KEY_WAS_PAUSED, mWasPaused);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            RecordedLog.d(Tag.FEED, "restoring instance");

            mWasPaused = savedInstanceState.getBoolean(KEY_WAS_PAUSED);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
//        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
//        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mWasPaused = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mWasPaused) {
            RecordedLog.d(Tag.FEED, "resumed from paused state");
            mWasPaused = false;

            // refresh the posts in case the user returned from an activity that
            // changed one (or more) of the posts
            refreshPosts();
        }
    }
}