package com.liststing.android.ui.feed;

public class FeedConstants {

    public static final int  FEED_MAX_POSTS_TO_DISPLAY       = 200;
    public static final int  FEED_MAX_POSTS_TO_REQUEST       = 20;
}
