package com.liststing.android.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.liststing.android.R;
import com.liststing.android.util.BitmapUtils;

/**
 * RoundedImageView crops the bitmap into a circular image.
 *
 */
public class RoundedImageView extends ImageView {
    private Paint paintBorder;
    private Paint paintSelectorBorder;
    private int borderWidth;
    private boolean hasBorder;


    public RoundedImageView(Context context)
    {
        this(context, null);
    }

    public RoundedImageView(Context context, AttributeSet attrs)
    {
        this(context, attrs, R.attr.roundImageViewStyle);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    /**
     * Initializes paint objects and sets desired attributes.
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    private void init(Context context, AttributeSet attrs, int defStyle)
    {
        paintBorder = new Paint();
        paintBorder.setAntiAlias(true);
        paintSelectorBorder = new Paint();
        paintSelectorBorder.setAntiAlias(true);

        // load the styled attributes and set their properties
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.RoundImageView, defStyle, 0);

        // Check if border and/or border is enabled
        hasBorder = attributes.getBoolean(R.styleable.RoundImageView_border, false);

        // Set border properties if enabled
        if(hasBorder) {
            int defaultBorderSize = (int) BitmapUtils.convertDpToPixel(2f);
            setBorderWidth(attributes.getDimensionPixelOffset(R.styleable.RoundImageView_border_width, defaultBorderSize));
            setBorderColor(attributes.getColor(R.styleable.RoundImageView_border_color, Color.WHITE));
        }

        // We no longer need our attributes TypedArray, give it back to cache
        attributes.recycle();
    }

    public void setHasBorder(boolean hasBorder) {
        this.hasBorder = hasBorder;
    }

    /**
     * Sets the CircularImageView's border width in dp.
     *
     * @param borderWidth
     */
    public void setBorderWidth(float borderWidth)
    {
        setBorderWidth((int) BitmapUtils.convertDpToPixel(borderWidth));
    }

    /**
     * Sets the CircularImageView's border width in pixels.
     *
     * @param borderWidth
     */
    public void setBorderWidth(int borderWidth)
    {
        this.borderWidth = borderWidth;
        this.requestLayout();
        this.invalidate();
    }

    /**
     * Sets the CircularImageView's basic border color.
     *
     * @param borderColor
     */
    public void setBorderColor(int borderColor)
    {
        if (paintBorder != null)
            paintBorder.setColor(borderColor);
        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }

        int width = getWidth();
        int height = getHeight();
        if (width == 0 || height == 0) {
            return;
        }

        Bitmap bitmap =  ((BitmapDrawable)drawable).getBitmap();
        Bitmap bitmapCopy = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Bitmap roundBitmap =  getCroppedBitmap(bitmapCopy, width);
        canvas.drawBitmap(roundBitmap, 0, 0, null);
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap, int width) {
        Bitmap scaledBitmap;

        // scale bitmap if it does not have equal dimensions
        if (bitmap.getWidth() != width || bitmap.getHeight() != width) {
            scaledBitmap = ThumbnailUtils.extractThumbnail(bitmap, width, width);
        } else {
            scaledBitmap = bitmap;
        }

        // rounding the image
        Bitmap output = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Rect rect = new Rect(0, 0, width, width);
        final Paint paint = new Paint();
        final int radius = width / 2;
        final int dropShadow = 3;

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);

        // remove unneeded bitmap corners
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(radius, radius - dropShadow, hasBorder ? radius - this.borderWidth - dropShadow : radius, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(scaledBitmap, rect, rect, paint);

        if(hasBorder) {
            paintBorder.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
            canvas.drawCircle(radius, radius - dropShadow, radius - dropShadow, paintBorder);

            Paint paintShadow = new Paint();
            paintShadow.setColor(getResources().getColor(R.color.divider_black));
            paintShadow.setAntiAlias(true);

            paintShadow.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OVER));
            canvas.drawCircle(radius, radius, radius, paintShadow);
        }
        return output;
    }
}