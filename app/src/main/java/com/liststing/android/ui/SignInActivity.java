package com.liststing.android.ui;

import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.liststing.android.ListSting;
import com.liststing.android.R;
import com.liststing.android.models.Account;
import com.liststing.android.networking.events.AccountEvents;
import com.liststing.android.networking.RestClient;
import com.liststing.android.networking.SignInRequest;
import com.liststing.android.networking.SignUpRequest;
import com.liststing.android.networking.Token;
import com.liststing.android.networking.services.ChatService;
import com.liststing.android.ui.chat.ChatActivity;
import com.liststing.android.util.NetworkUtils;
import com.liststing.android.util.RecordedLog;
import com.liststing.android.util.TextUtils;
import com.liststing.android.util.ValidationUtils;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

public class SignInActivity extends AppCompatActivity {

    public static final String FINISH_ACTION = "EXTRA_FINISH_ACTION";
    public static final String REGISTER_BUSINESS = "EXTRA_REGISTER_BUSINESS";
    private static final String NEW_BUSINESS_ENDPOINT = "/businesses/new/";

    public static final int REGISTER_BUSINESS_ACTIVITY = 100;
    public static final int OPEN_CHAT_ACTIVITY = 200;

    private RelativeLayout mCreateAccountLinkContainer;
    private LinearLayout mSignUpContainer;
    private TextView mCreateAccountLink;
    private RelativeLayout mSignInLinkContainer;
    private TextView mSignInLink;
    private TextView mStatusMessage;
    private TextView mErrorMessage;
    private EditText mPassword;
    private EditText mEmail;
    private ProgressBar mSignInProgressBar;
    private Button mSignInButton;
    private EditText mFirstName;
    private EditText mLastName;
    private Button mSignUpButton;
    private boolean mPasswordHasFocus = false;

    private HashMap<String, Object> mParams;
    private boolean mIsListingBusiness = false;
    private TextView mListStingText;
    private boolean mIsMessagingBusiness = false;
    private boolean mIsParticipantBusiness;
    private String mParticipantJabberID;
    private Long mParticipantUserID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        Intent intent = getIntent();
        getFinishAction(intent);

        mListStingText = (TextView) findViewById(R.id.liststing);
        ListSting.setTypeface(mListStingText);

        mSignInLinkContainer = (RelativeLayout) findViewById(R.id.sign_in_link_container);
        mSignUpContainer = (LinearLayout) findViewById(R.id.sign_up_container);
        mSignInLink = (TextView) findViewById(R.id.sign_in_back_link);

        mCreateAccountLinkContainer = (RelativeLayout) findViewById(R.id.create_account_link_container);
        mCreateAccountLink = (TextView) findViewById(R.id.create_account_link);

        mPassword = (EditText) findViewById(R.id.password);
        mEmail = (EditText) findViewById(R.id.email);
        mFirstName = (EditText) findViewById(R.id.first_name);
        mLastName = (EditText) findViewById(R.id.last_name);

        mStatusMessage = (TextView) findViewById(R.id.status_message);
        mErrorMessage = (TextView) findViewById(R.id.error_message);

        mSignInProgressBar = (ProgressBar) findViewById(R.id.sign_in_progress_bar);
        mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignUpButton = (Button) findViewById(R.id.sign_up_button);

        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
        mSignInLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSignUpView(false);
            }
        });
        mCreateAccountLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSignUpView(true);
            }
        });

        prefillEmailAddress();
        setUpToolbar();
    }

    private void getFinishAction(Intent intent) {
        int finishAction = intent.getIntExtra(FINISH_ACTION, 0);
        switch (finishAction) {
            case REGISTER_BUSINESS_ACTIVITY:
                mParams = (HashMap<String, Object>) intent.getSerializableExtra(REGISTER_BUSINESS);
                mIsListingBusiness = true;
                break;
            case OPEN_CHAT_ACTIVITY:
                mIsParticipantBusiness = intent.getBooleanExtra(ChatActivity.ARG_PARTICIPANT_IS_BUSINESS, false);
                mParticipantJabberID = intent.getStringExtra(ChatActivity.ARG_PARTICIPANT_JABBER_ID);
                mParticipantUserID = intent.getLongExtra(ChatActivity.ARG_PARTICIPANT_USER_ID, -1);
                mIsMessagingBusiness = true;
                break;
        }
    }

    private void signUp() {
        mStatusMessage.setVisibility(View.GONE);
        mErrorMessage.setVisibility(View.GONE);
        if(!isSignUpCredentialsPresent()) {
            return;
        }
        if(!hasNetworkConnectivity()) {
            return;
        }
        if(!isCredentialValid()) {
            return;
        }

        // Prevent double tapping of the "done" btn in keyboard for those clients that don't dismiss the keyboard.
        // Samsung S4 for example
        if (mSignInProgressBar.getVisibility() == View.VISIBLE) {
            return;
        }

        startSignUpProgress();
        createCredentials();
    }

    private void signIn() {
        mStatusMessage.setVisibility(View.GONE);
        mErrorMessage.setVisibility(View.GONE);
        if(!isSignInCredentialsPresent()) {
            return;
        }

        if(!hasNetworkConnectivity()) {
            return;
        }

        // Prevent double tapping of the "done" btn in keyboard for those clients that don't dismiss the keyboard.
        // Samsung S4 for example
        if (mSignInProgressBar.getVisibility() == View.VISIBLE) {
            return;
        }

        startSignInProgress();
        fetchCredentials();
    }

    private void prefillEmailAddress() {
        android.accounts.Account[] accounts = AccountManager.get(this).getAccounts();;
        if(accounts.length == 0) {
            return;
        }
        android.accounts.Account primaryAccount = accounts[0];
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        if (primaryAccount != null && emailPattern.matcher(primaryAccount.name).matches()) {
            String primaryEmail = primaryAccount.name;
            mEmail.setText(primaryEmail);
            mPasswordHasFocus = true;
        }
    }

    private boolean isSignInCredentialsPresent() {
        return ValidationUtils.requiredField(this, mEmail) && ValidationUtils.requiredField(this, mPassword);
    }
    private boolean isSignUpCredentialsPresent() {
        return isSignInCredentialsPresent() && ValidationUtils.requiredField(this, mFirstName) && ValidationUtils.requiredField(this, mLastName);
    }
    private boolean isCredentialValid() {
        boolean isValid = true;

        if(!ValidationUtils.isValidEmailInputRequired(mEmail)) {
            mEmail.setError(getString(R.string.invalid_email));
            mEmail.requestFocus();
            return false;
        }
        return isValid;
    }

    private void focusEmailAddress() {
        if(mPasswordHasFocus) {
            mPassword.requestFocus();
            return;
        }
        mEmail.requestFocus();
    }

    private boolean hasNetworkConnectivity() {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            mErrorMessage.setText(getString(R.string.no_network_connectivity));
            mErrorMessage.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }

    private void startSignUpProgress() {
        mEmail.setEnabled(false);
        mPassword.setEnabled(false);
        mFirstName.setEnabled(false);
        mLastName.setEnabled(false);

        mSignUpButton.setVisibility(View.GONE);
        mSignInProgressBar.setVisibility(View.VISIBLE);

        mStatusMessage.setText(getString(R.string.signing_up));
        mStatusMessage.setVisibility(View.VISIBLE);
    }
    private void stopSignUpProgress() {
        mEmail.setEnabled(true);
        mPassword.setEnabled(true);
        mFirstName.setEnabled(true);
        mLastName.setEnabled(true);

        mSignUpButton.setVisibility(View.VISIBLE);
        mSignInProgressBar.setVisibility(View.GONE);
        mStatusMessage.setVisibility(View.GONE);
    }

    private void startSignInProgress() {
        mEmail.setEnabled(false);
        mPassword.setEnabled(false);

        mSignInButton.setVisibility(View.GONE);
        mSignInProgressBar.setVisibility(View.VISIBLE);

        mStatusMessage.setText(getString(R.string.signing_in));
        mStatusMessage.setVisibility(View.VISIBLE);
    }
    private void stopSignInProgress() {
        mEmail.setEnabled(true);
        mPassword.setEnabled(true);

        mSignInButton.setVisibility(View.VISIBLE);
        mSignInProgressBar.setVisibility(View.GONE);
        mStatusMessage.setVisibility(View.GONE);
    }


    private void fetchCredentials() {
        final String email = TextUtils.getText(mEmail).trim();
        final String password = TextUtils.getText(mPassword).trim();

        new Thread() {
            @Override
            public void run() {
                SignInRequest signInRequest = new SignInRequest(email, password, new Response.Listener<Token>() {
                    @Override
                    public void onResponse(Token token) {
                        Account account = Account.getDefaultAccount();
                        account.setAccessToken(token.toString());
                        account.setEmail(email);
                        account.save();
                        account.fetchAccountDetails();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        JSONObject jsonError = NetworkUtils.volleyErrorToJSON(error);
                        int errorMessageId = parseErrorId(jsonError);
                        stopSignInProgress();
                        showErrorMessage(errorMessageId);
                    }
                });

                ListSting.getRequestQueue().add(signInRequest);
            }
        }.start();
    }

    private void createCredentials() {
        final String email = TextUtils.getText(mEmail).trim();
        final String password = TextUtils.getText(mPassword).trim();
        final String firstName = TextUtils.getText(mFirstName).trim();
        final String lastName = TextUtils.getText(mLastName).trim();

        new Thread() {
            @Override
            public void run() {
                SignUpRequest signUpRequest = new SignUpRequest(email, password, firstName,
                        lastName,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                fetchCredentials();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                JSONObject jsonError = NetworkUtils.volleyErrorToJSON(error);
                                int errorMessageId = parseErrorId(jsonError);
                                stopSignUpProgress();
                                showErrorMessage(errorMessageId);
                            }
                        }
                );

                ListSting.getRequestQueue().add(signUpRequest);
            }
        }.start();

    }

    private int parseErrorId(JSONObject jsonError) {
        // fallback
        int errorMsgId = R.string.unable_to_sign_in;

        if (jsonError != null) {
            String error = jsonError.optString("error", "");
            String errorDescription = jsonError.optString("error_description", "");

            if (error.equals("invalid_grant") &&  errorDescription.contains("Invalid credentials given.")) {
                return R.string.incorrect_username_or_password;
            }

            if(isSignUpError(jsonError)) {
                return signUpError(jsonError);
            }
        }
        return errorMsgId;
    }

    private boolean isSignUpError(JSONObject jsonError) {
        String emailError = jsonError.optString("email", "");

        return !emailError.isEmpty();
    }
    private int signUpError(JSONObject jsonError) {
        String emailError = jsonError.optString("email", "");
        // email address already exists
        if(emailError.contains("unique")) {
            return R.string.email_unique;
        }
        return 0;
    }


    private void finishActivityAndSignIn() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    private void showErrorMessage(int errorMessageId) {
        mErrorMessage.setVisibility(View.VISIBLE);
        mErrorMessage.setText(getString(errorMessageId));
    }

    private void showSignUpView(boolean isVisibile) {
        if(isVisibile) {
            mCreateAccountLinkContainer.setVisibility(View.GONE);
            mSignUpContainer.setVisibility(View.VISIBLE);
            mSignInLinkContainer.setVisibility(View.VISIBLE);
            mSignInButton.setVisibility(View.GONE);
            mSignUpButton.setVisibility(View.VISIBLE);
            focusEmailAddress();
            return;
        }
        mCreateAccountLinkContainer.setVisibility(View.VISIBLE);
        mSignUpContainer.setVisibility(View.GONE);
        mSignInLinkContainer.setVisibility(View.GONE);
        mSignInButton.setVisibility(View.VISIBLE);
        mSignUpButton.setVisibility(View.GONE);
        focusEmailAddress();
    }

    private void setUpToolbar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.action_sign_in_or_sign_up));
        getSupportActionBar().setElevation(0);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(AccountEvents.FetchingAccountEnded event) {

        if(event.getResult().isNewOrChanged()) {
            if(mIsListingBusiness) { // Listing business but hasn't signed in
                listBusiness();
                return;
            }
            if(mIsMessagingBusiness) {
                messageBusiness();
                return;
            }

            finishActivityAndSignIn();
        }

        if(event.getResult().hasFailed()) {
            // TODO: ERROR FETCHING ACCOUNT DETAILS
        }
    }

    private void messageBusiness() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                if(Account.getDefaultAccount().getId() != mParticipantUserID) {
                    Intent chatIntent = new Intent(SignInActivity.this, ChatActivity.class);
                    chatIntent.putExtra(ChatActivity.ARG_PARTICIPANT_IS_BUSINESS, mIsParticipantBusiness);
                    chatIntent.putExtra(ChatActivity.ARG_PARTICIPANT_USER_ID, mParticipantUserID);
                    chatIntent.putExtra(ChatActivity.ARG_PARTICIPANT_JABBER_ID, mParticipantJabberID);
                    startActivity(chatIntent);
                }
                finish();
            }
        });
    }

    private void listBusiness() {
        mParams.put("admin", String.valueOf(Account.getDefaultAccount().getId()));
        ListSting.getRestClient().post(NEW_BUSINESS_ENDPOINT, mParams, null, new RestClient.Listener() {
            @Override
            public void onResponse(JSONObject response) {
                Account.getDefaultAccount().updateFromJsonObject(response);
                finishActivityAndSignIn();
            }
        }, new RestClient.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                RecordedLog.e(RecordedLog.Tag.API, "Create Business > " + error);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sign_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
