package com.liststing.android.ui.business;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.liststing.android.R;
import com.liststing.android.models.Business;

public class ListBusinessActivity extends AppCompatActivity {

    public static final String ARG_BUSINESS = "ARG_BUSINESS";
    private Business mBusiness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBusiness = null;
        if(getIntent().hasExtra(ARG_BUSINESS)) {
            mBusiness = getIntent().getExtras().getParcelable(ARG_BUSINESS);
        }

        // Prevent overlapping fragments
        if (savedInstanceState != null) {
            return;
        }

        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.details_fragment_container, ListBusinessFragment.newInstance(mBusiness)).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
