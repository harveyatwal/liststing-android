package com.liststing.android;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.liststing.android.datasets.AccountTable;
import com.liststing.android.datasets.FeedPostTable;
import com.liststing.android.datasets.IndustryTable;
import com.liststing.android.datasets.BusinessTable;
import com.liststing.android.datasets.SearchSuggestionTable;
import com.liststing.android.util.RecordedLog;

public class ListStingDatabase extends SQLiteOpenHelper {
    private static final String DB_NAME = ListSting.NAME + ".db";
    private static final int DB_VERSION = 1;

    private ListStingDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    /*
	 *  singleton
	 */
    private static ListStingDatabase mReaderDb;
    private final static Object mDbLock = new Object();
    public static ListStingDatabase getDatabase() {
        if (mReaderDb == null) {
            synchronized(mDbLock) {
                if (mReaderDb == null) {
                    mReaderDb = new ListStingDatabase(ListSting.getContext());
                    // this ensures that onOpen() is called with a writable database (open will fail if app calls getReadableDb() first)
                    mReaderDb.getWritableDatabase();
                }
            }
        }
        return mReaderDb;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: currently the database is being reset. However, in future versions it would
        // TODO: be viable to restructure the database
        RecordedLog.i(RecordedLog.Tag.APPLICATION, "database > upgrade from version " + oldVersion + " to version " + newVersion);
        reset(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        RecordedLog.i(RecordedLog.Tag.APPLICATION, "database > downgrade from version " + oldVersion + " to version " + newVersion);
        reset(db);
    }

    private void createTables(SQLiteDatabase db) {
        AccountTable.createTable(db);
        FeedPostTable.createTable(db);
        IndustryTable.createTable(db);
        BusinessTable.createTable(db);
        SearchSuggestionTable.createTable(db);
    }
    private void dropTables(SQLiteDatabase db) {
        AccountTable.dropTables(db);
        FeedPostTable.dropTables(db);
        IndustryTable.dropTables(db);
        BusinessTable.dropTables(db);
        SearchSuggestionTable.dropTables(db);
    }

    /*
     * clear the database of all data
     */
    public void reset(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            dropTables(db);
            createTables(db);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    /*
     * resets (clears) the reader database
     */
    public static void reset() {
        // note that we must call getWritableDb() before getDatabase() in case the database
        // object hasn't been created yet
        SQLiteDatabase db = getWritableDb();
        getDatabase().reset(db);
    }

    public static SQLiteDatabase getReadableDb() {
        return getDatabase().getReadableDatabase();
    }
    public static SQLiteDatabase getWritableDb() {
        return getDatabase().getWritableDatabase();
    }
}
